/**
 * @format
 */
import * as React from 'react';
import { AppRegistry } from 'react-native';
import { name as appName } from './app.json';
import App from './app/componnts/Servicos/Teste';
import Menu from './app/componnts/Menu'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducers from './app/store/reducers'

// const store = createStore(reducers)

// const MenuProvider = () => {
//     return (
//         <Provider store={store}>
//             <Menu />
//         </Provider>
//     )
// }

AppRegistry.registerComponent(appName, () => Menu);
