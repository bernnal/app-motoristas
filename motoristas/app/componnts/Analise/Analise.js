import React, { Component } from 'react'
import { View, Text, Modal, ScrollView, FlatList, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import styles from './styles'
import { Col, Row, Grid } from "react-native-easy-grid"
import moment from 'moment'
import axios from 'axios'
import { withNavigation } from 'react-navigation'
import { API_MOTORISTAS_VENCIMENTOS } from '../../config'

class Analise extends Component {

    constructor(props) {
        super(props)
        this.state = {
            //api_token: this.props.navigation.state.params.api_token,
            //firstDay: null,
            //dataSourceR: [],
            //totalSomaR: 0,
        }
    }

    // componentDidMount() {
    //     axios.get(`${API_MOTORISTAS_VENCIMENTOS}`, {
    //         params: {
    //             'param1': this.state.api_token,
    //             'param2': this.state.firstDay,
    //         }
    //     }, {
    //             "headers": {
    //                 'Content-Type': 'application/json',
    //             }
    //         }).then((dataSource) => {
    //             let filterR = dataSource.data['data'];

    //             let filterDataR = filterR.filter(function (el) {
    //                 return el.tipo === 'R';
    //             })
    //             this.setState({
    //                 motoristasFinancas: true,
    //                 dataSourceR: filterDataR,
    //             })
    //         })
    //         .catch(error => console.log(error));
    // }

    render() {
        // let date = new Date();
        // let firstDayMonth = new Date(date.getFullYear(), date.getMonth(), 1)
        // let dayFormat = moment(firstDayMonth).format('DD-MM-YYYY')
        // this.state.firstDay = dayFormat

        // if (this.state.motoristasFinancas) {
        //     let sum = a => a.reduce((x, y) => x + y);
        //     let totalAmountR = sum(this.state.dataSourceR.map(x => Number(x.valor_servico)))
        //     let totalSomaR = totalAmountR.toFixed(2)
        //     this.state.totalSomaR = totalSomaR
        // }

        return (
            <ScrollView style={{ marginBottom: 30 }}>
                <View style={styles.container}>
                    <View style={styles.dashboardHeader}>
                        <Text style={styles.header}> CONSULTA DE VENCIMENTOS </Text>
                    </View>
                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                            Serviços
                        </Text>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                        <Grid>
                            <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >ID</Text></Col>
                            <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Data</Text></Col>
                            <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Recolha</Text></Col>
                            <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Entrega</Text></Col>
                            <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                        </Grid>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5 }}>
                        <FlatList
                            //data={this.state.dataSourceR}
                            renderItem={({ item, index }) =>
                                < Grid >
                                    <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.codigo_servico}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.recolha}</Text></Col>
                                    <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.entrega}</Text></Col>
                                    <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor_servico}</Text></Col>
                                </Grid>
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {/* <Text style={{ fontWeight: 'bold', fontSize: 14 }}>Total({this.state.dataSourceR.length}): {this.state.totalSomaR}€</Text> */}
                    </View>
                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                            Prémio Produção Própria
                        </Text>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                        <Grid>
                            <Col style={{ alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >ID</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Data</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Cliente</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Recolha</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Entrega</Text></Col>
                            <Col style={{ alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                        </Grid>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5 }}>
                        <FlatList
                            //data={}
                            renderItem={({ item, index }) =>
                                <Grid>
                                    <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.codigo_servico}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.descricao}</Text></Col>
                                    <Col style={{ width: '9%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.recolha}</Text></Col>
                                    <Col style={{ width: '9%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.entrega}</Text></Col>
                                    <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor_servico}</Text></Col>
                                </Grid>
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 14 }}>0,00€</Text>
                    </View>
                    <View>
                        <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                            Prémio Noturno
                        </Text>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                        <Grid>
                            <Col style={{ alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >ID</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Data</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Cliente</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Recolha</Text></Col>
                            <Col style={{}}><Text style={{ margintop: 10 }} >Entrega</Text></Col>
                            <Col style={{ alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                        </Grid>
                    </View>
                    <View
                        style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                    />
                    <View style={{ paddingHorizontal: 5 }}>
                        <FlatList
                            //data={}
                            renderItem={({ item, index }) =>
                                <Grid>
                                    <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.codigo_servico}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                    <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.descricao}</Text></Col>
                                    <Col style={{ width: '9%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.recolha}</Text></Col>
                                    <Col style={{ width: '9%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.entrega}</Text></Col>
                                    <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor_servico}</Text></Col>
                                </Grid>
                            }
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <Text style={{ fontWeight: 'bold', fontSize: 14 }}>0,00€</Text>
                    </View>
                    <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9' }}>
                        <ScrollView>
                            <Grid>
                                <Col style={{ alignItems: 'center' }}>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Vencimento</Text></Row>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>500.00€</Text></Row>
                                </Col>
                                <Col style={{ alignItems: 'center' }}>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Objetivo</Text></Row>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>0.00€</Text></Row>
                                </Col>
                                <Col style={{ alignItems: 'center' }}>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Comissões</Text></Row>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>9,60€</Text></Row>
                                </Col>
                                <Col style={{ alignItems: 'center' }}>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Total</Text></Row>
                                    <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>509,60€</Text></Row>
                                </Col>
                            </Grid>
                        </ScrollView>
                    </View>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                            <Text style={styles.text}> SAIR </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

export default withNavigation(Analise)