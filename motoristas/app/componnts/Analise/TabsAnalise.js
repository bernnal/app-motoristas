import React from 'react';
import { Text, View } from 'react-native';
import {
  createMaterialTopTabNavigator,
  createAppContainer,
} from 'react-navigation';
import Analise from './Analise'
import LastMonthAnalise from './LastMonthAnalise'

const TabNavigator = createMaterialTopTabNavigator(
  {
    'MES ANTERIOR': { screen: LastMonthAnalise },
    'MÊS ATUAL': { screen: Analise },
  },
  {
    initialRouteName: 'MÊS ATUAL',
    style: {
        backgroundColor: 'rgba(0,0,0,0.6)',
        paddingTop: 50,
    },
    tabBarOptions:  {
      style: {
        backgroundColor: '#C15F16'
      },
      indicatorStyle: {
        borderBottomColor: '#fff',
        borderBottomWidth: 2,
      },
    },
  }
);

export default TabNavigator;
