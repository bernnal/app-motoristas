import React, { Component } from 'react'
import { View, Text, Modal, ScrollView, FlatList, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import styles from './styles'
import axios from 'axios'
import { Col, Row, Grid } from "react-native-easy-grid"
import { API_MOTORISTAS_FINANCAS } from '../../config'
import moment from 'moment'
import { withNavigation } from 'react-navigation'

class Contas extends Component {

    constructor(props) {
        super(props)
        this.state = {
            api_token: this.props.navigation.state.params.api_token,
            motoristasFinancas: false,
            dataSourceR: [],
            dataSourceP: [],
            dataSourceD: [],
            totalSomaR: 0,
            totalSomaP: 0,
            totalSomaD: 0,
            receita: 0,
            totalTotal: 0,
        }
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_FINANCAS}`, {
            params: {
                'param1': this.state.api_token,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSource) => {
                let filterR = dataSource.data['data'];
                let filterP = dataSource.data['data'];
                let filterD = dataSource.data['data'];

                let filterDataR = filterR.filter(function (el) {
                    return el.tipo === 'R';
                })
                let filterDataP = filterP.filter(function (el) {
                    return el.tipo === 'P';
                })
                let filterDataD = filterD.filter(function (el) {
                    return el.tipo === 'D';
                })

                this.setState({
                    motoristasFinancas: true,
                    dataSourceR: filterDataR,
                    dataSourceP: filterDataP,
                    dataSourceD: filterDataD,
                })
            })
            .catch(error => console.log(error));
    }

    render() {
        if(this.state.motoristasFinancas){
            let sum = a => a.reduce((x, y) => x + y);
            let totalAmountR = sum(this.state.dataSourceR.map(x => Number(x.valor_servico)))
            let totalSomaR = totalAmountR.toFixed(2)
            this.state.totalSomaR = totalSomaR

            let totalAmountP = sum(this.state.dataSourceP.map(x => Number(x.valor_servico)))
            let totalSomaP = totalAmountP.toFixed(2)
            this.state.totalSomaP = totalSomaP

            let totalAmountD = sum(this.state.dataSourceD.map(x => Number(x.valor)))
            let totalSomaD = totalAmountD.toFixed(2)
            this.state.totalSomaD = totalSomaD

            let rec = Number(this.state.totalSomaR) + Number(this.state.totalSomaP)
            let recFixed = rec.toFixed(2)
            this.state.receita = recFixed

            let totTotal = Number(this.state.receita) - Number(this.state.totalSomaD)
            let totFixed = totTotal.toFixed(2)
            this.state.totalTotal = totFixed
        }
        return (
            <View style={{}}>
                <Modal
                    onRequestClose={this.props.onCancel}
                    visible={this.props.isVisible}
                    animationType='slide'
                    transparent={true}>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.6)' }}>
                        <TouchableWithoutFeedback onPress={this.props.onCancel}>
                            <View style={styles.offset}></View>
                        </TouchableWithoutFeedback>
                        <ScrollView style={{ marginTop: 30, marginBottom: 30 }}>
                            <View style={styles.container}>
                                <View style={styles.dashboardHeader}>
                                    <Text style={styles.header}> RELAÇÃO DE COBRANÇAS & DESPESAS </Text>
                                </View>
                                <View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                                        Serviços
                                </Text>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                                    <Grid>
                                        <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >ID</Text></Col>
                                        <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Data</Text></Col>
                                        <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Recolha</Text></Col>
                                        <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Entrega</Text></Col>
                                        <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                                    </Grid>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{}}>
                                    <FlatList
                                        data={this.state.dataSourceR}
                                        renderItem={({ item, index }) =>
                                            <Grid style={{ backgroundColor: index % 2 === 0 ? 'white' : '#d9d9d9' }}>
                                                <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.codigo_servico}</Text></Col>
                                                <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                                <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.recolha}</Text></Col>
                                                <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.entrega}</Text></Col>
                                                <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor_servico}</Text></Col>
                                            </Grid>
                                        }
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                                <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>Total({this.state.dataSourceR.length}): {this.state.totalSomaR}€</Text>
                                </View>
                                <View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                                        Produção Própria
                                </Text>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                                    <Grid>
                                        <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >ID</Text></Col>
                                        <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Data</Text></Col>
                                        <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Cliente</Text></Col>
                                        <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Lugar</Text></Col>
                                        <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                                    </Grid>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{}}>
                                    <FlatList
                                        data={this.state.dataSourceP}
                                        renderItem={({ item, index }) =>
                                            <Grid style={{ backgroundColor: index % 2 === 0 ? 'white' : '#d9d9d9' }}>
                                                <Col style={{ width: '20%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.codigo_servico}</Text></Col>
                                                <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                                <Col style={{ width: '25%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.descricao}</Text></Col>
                                                <Col style={{ width: '18%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{item.lugares}</Text></Col>
                                                <Col style={{ width: '12%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor_servico}</Text></Col>
                                            </Grid>
                                        }
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                                <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.totalSomaP}€</Text>
                                </View>
                                <View>
                                    <Text style={{ fontWeight: 'bold', fontSize: 16, marginVertical: 10 }}>
                                        Despesa
                                </Text>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                                    <Grid>
                                        <Col style={{ width: '33%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >Data</Text></Col>
                                        <Col style={{ width: '33%', alignItems: 'center' }}><Text style={{ margintop: 10 }} >Despesa</Text></Col>
                                        <Col style={{ width: '33%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >Valor</Text></Col>
                                    </Grid>
                                </View>
                                <View
                                    style={{ height: 1, width: '100%', backgroundColor: '#222' }}
                                />
                                <View style={{}}>
                                    <FlatList
                                        data={this.state.dataSourceD}
                                        renderItem={({ item, index }) =>
                                            <Grid style={{ backgroundColor: index % 2 === 0 ? 'white' : '#d9d9d9' }}>
                                                <Col style={{ width: '33%', alignItems: 'flex-start' }}><Text style={{ margintop: 10 }} >{moment(item.data_hora).format('DD-MM-YYYY HH:MM')}</Text></Col>
                                                <Col style={{ width: '33%', alignItems: 'center' }}><Text style={{ margintop: 10 }} >{item.descricao}</Text></Col>
                                                <Col style={{ width: '33%', alignItems: 'flex-end' }}><Text style={{ margintop: 10 }} >{item.valor}</Text></Col>
                                            </Grid>
                                        }
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                                <View style={{ paddingHorizontal: 5, marginVertical: 20, flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.totalSomaD}€</Text>
                                </View>
                                <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9' }}>
                                    <ScrollView>
                                        <Grid>
                                            <Col style={{ alignItems: 'center' }}>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Receita</Text></Row>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.receita}€</Text></Row>
                                            </Col>
                                            <Col style={{ alignItems: 'center' }}>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Despesa</Text></Row>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.totalSomaD}€</Text></Row>
                                            </Col>
                                            <Col style={{ alignItems: 'center' }}>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>Total</Text></Row>
                                                <Row><Text style={{ fontWeight: 'bold', fontSize: 14 }}>{this.state.totalTotal}€</Text></Row>
                                            </Col>
                                        </Grid>
                                    </ScrollView>
                                </View>
                                <View style={styles.buttonsLogin}>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                                        <Text style={styles.text}> SAIR </Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                        <TouchableWithoutFeedback onPress={this.props.onCancel}>
                            <View style={styles.offset}></View>
                        </TouchableWithoutFeedback>
                    </View>
                </Modal>
            </View>
        )
    }
}

export default withNavigation(Contas)