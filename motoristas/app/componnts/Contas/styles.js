import React, { Component } from 'react'
import { StyleSheet, Dimensions } from 'react-native'

const { width: WIDTH } = Dimensions.get('window')

const styles = StyleSheet.create({
    offset: {
        //flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#F2F2F2',
        justifyContent: 'space-between',
        width: '100%',
        //marginTop: 80,
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#4A606F',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    btnEntrar: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: '#fff',
        fontSize: 14,
        textAlign: 'center',
    },
})

export default styles