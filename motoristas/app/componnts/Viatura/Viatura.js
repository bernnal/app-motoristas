import React, { Component } from 'react'
import {
	Text,
	View,
	ImageBackground,
	Image,
	TextInput,
	Dimensions,
	TouchableOpacity,
	Alert,
	Button,
	Picker,
	Animated,
	Platform,
	TouchableWithoutFeedback,
	StyleSheet
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../resources/fonts/selection.json'
//import { Container, Content, Picker, Form, } from "native-base";
import styles from './styles'
import bgImage from '../../images/fundo-vertical-cor.jpg'
import logo from '../../images/logo-tg-v-01.png'
import Login from '../Login/Login'
import axios from 'axios'
import { API_GET_LOGO, API_MOTORISTAS_VIATURAS } from '../../config'

const { width: WIDTH } = Dimensions.get('window')
const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class Viatura extends Component {

	constructor(props) {
		super(props)
		this.state = {
			api_token: this.props.navigation.state.params.api_token,
			app_footer : this.props.navigation.state.params.app_footer,
			logo: this.props.navigation.state.params.logo,
			marca: '',
			modelo: '',
			matricula: '',
			km_actuais: '',

			descViatura: '',
			descViaturaIOS: 'Selecione a Viatura',

			viaturas: false,
			dataSource: [],

			choosenindex: '',

			modalIsVisible: false,
			modalAnimatedValue: new Animated.Value(0),
		}
	}

	static navigationOptions = {
		drawerIcon: ({ tintColor }) => (
			<MyIcon name="fonte-tg-81" style={{ fontSize: 24, color: tintColor }} />
		)
	}

	componentDidMount() {
		axios.get(`${API_MOTORISTAS_VIATURAS}`, {
			'param1': this.state.api_token
		}, {
				"headers": {
					'Content-Type': 'application/json',
				}
			}).then((dataSource) => {
				this.setState({
					viaturas: true,
					dataSource: dataSource.data['data'],
				})
			})
			.catch(error => console.log(error));
	}

	onValueChange(value, index) {
		console.log('index', index)
		console.log('value', value)
		let kms = this.state.dataSource[index].km_actuais
		let marcaSelecionada = this.state.dataSource[index].marca
		let modeloSelecionado = this.state.dataSource[index].modelo
		let matriculaSelecionada = this.state.dataSource[index].matricula

		let viaturaSelecionada = marcaSelecionada + ' ' + modeloSelecionado + ' ' + matriculaSelecionada

		this.setState({
			descViatura: value,
			km_actuais: kms,
			descViaturaIOS: viaturaSelecionada
		});

	}

	loginUser = () => {
		try {
			this.setState({ error: '', loading: true })

			const api_token = this.state.api_token
			const app_footer = this.state.app_footer
			this.props.navigation.navigate('Dashboard', {
				api_token: api_token,
				app_footer: app_footer,
			})
		} catch (err) {
			Alert.alert((err))
		}
	}

	_handlePressOpen = () => {
		if (this.state.modalIsVisible) {
			return;
		}

		this.setState({ modalIsVisible: true }, () => {
			Animated.timing(this.state.modalAnimatedValue, {
				toValue: 1,
				duration: 200,
				useNativeDriver: true,
			}).start();
		});
	};

	_handlePressDone = () => {
		Animated.timing(this.state.modalAnimatedValue, {
			toValue: 0,
			duration: 150,
			useNativeDriver: true,
		}).start(() => {
			this.setState({ modalIsVisible: false });
		});
	};

	render() {

		console.log('lista de viaturas: ', this.state.dataSource)

		let list_viaturas = this.state.dataSource;
		if (typeof (list_viaturas) !== undefined) {
			list_viaturas = [list_viaturas][0];
		}

		let km_selected = typeof (this.state.km_actuais) !== 'undefined' ? this.state.km_actuais : 0

		let modalPicker = null
		if (Platform.OS === 'ios') {
			modalPicker = (
				<View style={{
					height: 45,
					marginTop: 70,
					flexDirection: 'row',
					width: WIDTH - 85,
				}}>
					<View style={styles.iconContainer}>
						<MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
					</View>
					<TouchableWithoutFeedback onPress={this._handlePressOpen}>
						<View
							style={{
								justifyContent: 'center',
								backgroundColor: '#fff',
								width: WIDTH - 130,
								paddingRight: 30,
							}}>
							<Text style={{ paddingLeft: 15, fontSize: 14 }}>{this.state.descViaturaIOS}</Text>
						</View>
					</TouchableWithoutFeedback>
				</View>
			)
		} else {
			modalPicker = (
				<View style={{
					height: 45,
					marginTop: 70,
					flexDirection: 'row',
					width: WIDTH - 85,
				}}>
					<View style={styles.iconContainer}>
						<MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
					</View>
					<View style={{
						justifyContent: 'center',
						backgroundColor: '#fff',
						width: WIDTH - 130,
						paddingRight: 30,
					}}>
						<Picker
							placeholderText='Selecione a Viatura'
							mode='dropdown'
							placeholderStyle={{ color: '#000' }}
							selectedValue={this.state.descViatura}
							itemStyle={{
								fontSize: 14,
								color: '#000',
							}}
							onValueChange={this.onValueChange.bind(this)}>
							{list_viaturas.map((item, key) => {
								return (
									<Picker.Item
										label={item.marca + ' ' + ' ' + item.modelo + ' ' + item.matricula}
										value={item.id}
										key={key}
									/>
								);
							})}
						</Picker>
					</View>
				</View>
			)
		}

		_maybeRenderModal = () => {
			if (!this.state.modalIsVisible) {
				return null;
			}

			const { modalAnimatedValue } = this.state;
			const opacity = modalAnimatedValue;
			const translateY = modalAnimatedValue.interpolate({
				inputRange: [0, 1],
				outputRange: [300, 0],
			});

			return (
				<View
					style={StyleSheet.absoluteFill}
					pointerEvents={this.state.modalIsVisible ? 'auto' : 'none'}>
					<TouchableWithoutFeedback onPress={this.props.onCancel}>
						<Animated.View style={[styles.overlay, { opacity }]} />
					</TouchableWithoutFeedback>
					<Animated.View
						style={{
							width: '100%',
							position: 'absolute',
							bottom: 0,
							left: 0,
							transform: [{ translateY }],

						}}>
						<View style={styles.toolbar}>
							<View style={styles.toolbarRight}>
								<Button title="OK" onPress={this._handlePressDone} />
							</View>
						</View>
						<Picker
							style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
							selectedValue={this.state.descViatura}
							itemStyle={{
								fontSize: 18,
								color: '#000',
							}}
							onValueChange={this.onValueChange.bind(this)}>
							{list_viaturas.map((item, key) => {
								return (
									<Picker.Item
										label={item.marca + ' ' + ' ' + item.modelo + ' ' + item.matricula}
										value={item.id}
										key={key}
									/>
								);
							})}
						</Picker>
					</Animated.View>
				</View>
			)
		}

		return (
			<ImageBackground
				source={bgImage}
				style={styles.backgroundContainer} >
				<View>
					{this.state.viaturas ?
						<View
							style={{
								height: (Dimensions.get('window').height / 10) * 10,
								alignItems: 'center',
								justifyContent: 'center',
							}}>
							<View style={styles.logoContainer}>
								<Image source={{ uri: `${API_GET_LOGO}${this.state.logo}` }} style={styles.logo} />
							</View>
							{modalPicker}
							<View style={styles.inputText2}>
								<View style={styles.iconContainer}>
									<MyIcon style={styles.icon} name="fonte-tg-46" size={25} />
								</View>
								<TextInput
									style={styles.input}
									value={km_selected}
									keyboardType={'numeric'}
									placeholder={"0"}
									placeholderTextColor={'#000'}
									underlineColorAndroid='transparent'
								/>
							</View>
							<View style={styles.buttonsLogin}>
								<TouchableOpacity onPress={() => this.props.navigation.navigate('Trocar de Motorista')} style={styles.btnSair}>
									<Text style={styles.text}> Sair </Text>
								</TouchableOpacity>
								<TouchableOpacity onPress={this.loginUser} style={styles.btnEntrar}>
									<Text style={styles.text}> Entrar </Text>
								</TouchableOpacity>
							</View>
						</View>
						: null}
				</View>
				{_maybeRenderModal()}
			</ImageBackground>
		)
	}
}