import * as React from 'react';
import { Text, View, StyleSheet, Picker, ImageBackground, Dimensions } from 'react-native';
import bgImage from '../../images/fundo-vertical-cor.jpg'
import axios from 'axios'

const { width: WIDTH } = Dimensions.get('window')

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            marca: '',
            modelo: '',
            matricula: '',
            km_actuais: '',

            descViatura: '',

            viaturas: false,
            dataSource: [],

            choosenindex: '',
            carroSelecionado: '',
        };
    }

    componentDidMount() {
        axios
            .get('http://www.mocky.io/v2/5d0bf3c73500005a00b89676', {
                headers: {
                    'Content-Type': 'application/json',
                },
            })
            .then(dataSource => {
                this.setState({
                    viaturas: true,
                    dataSource: dataSource.data['data'],
                });
            })
            .catch(error => console.log(error));
    }

    onValueChange(value: string, index: number) {
        let kms = this.state.dataSource[index].km_actuais

        this.setState({
            descViatura: value,
            choosenindex: index,
            km_actuais: kms,
        });
    }

    render() {
        let list_viaturas = this.state.dataSource;
        if (typeof list_viaturas !== undefined) {
            list_viaturas = [list_viaturas][0];
        }

        console.log('carroSelecionado', this.state.carroSelecionado)

        return (
            <ImageBackground
                source={bgImage}
                style={styles.backgroundContainer} >
                <View style={styles.container}>
                    <View style={{ marginTop: 30 }}>
                        <Text>Id of the Car: {this.state.descViatura}</Text>
                    </View>
                    <View>
                        <Text>Index of the Car: {this.state.choosenindex}</Text>
                    </View>
                    <View>
                        <Text>Kms: {this.state.km_actuais}</Text>
                    </View>

                    <Picker
                        placeholderText='Select the car'
                        mode='dropdown'
                        placeholderStyle={{ color: '#000' }}
                        selectedValue={this.state.descViatura}
                        onValueChange={this.onValueChange.bind(this)}>
                        {list_viaturas.map((item, key) => {
                            return (
                                <Picker.Item
                                    label={item.marca + ' ' + ' ' + item.modelo + ' ' + item.matricula}
                                    value={item.id}
                                    key={key}
                                />
                            );
                        })}
                    </Picker>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingTop: 30,
        backgroundColor: '#ecf0f1',
        padding: 8,
        width: WIDTH - 85
    },
    backgroundContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
});
