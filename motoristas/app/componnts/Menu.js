import React, { Component } from 'react'
import {
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Dimensions,
    View,
    Image,
    Text
} from 'react-native'
import { createDrawerNavigator, DrawerItems, withNavigation, } from 'react-navigation'
import Dashboard from './Dashboard/Dashboard'
import Viatura from './Viatura/Viatura'
import NovoServico from './NovoServico/NovoServico'
import NovaDespesa from './NovaDespesa/NovaDespesa'
import Header from './Header'
import Login from './Login/Login'
import ServicesNavigator from './Servicos/ServicesNavigator'
import TodayService from './Servicos/TodayServices'
import YesterdayService from './Servicos/YesterdayServices'
import TomorrowService from './Servicos/TomorrowServices'
import Agenda from './Agenda/Agenda'
import RenderServices from './Servicos/RenderServices'
import TestePicker from './Viatura/TestePicker'
import Contas from './Contas/Contas'
import Analise from './Analise/Analise'
import LastMonthAnalise from './Analise/LastMonthAnalise'
import TabNavigator from './Analise/TabsAnalise'
import Cliente from './Servicos/Modal/Cliente'
import Teste from './Servicos/Teste'
import FormadePagamento from './Servicos/Modal/FormadePagamento'
import FechoServico from './Servicos/Modal/FechoServico'
import FechoServicoRetorno from './Servicos/Modal/FechoServicoRetorno'


export default class App extends Component {
    render() {
        return (
            <AppDrawerNavigator />
        )
    }
}

const CustomDrawerComponent = (props) => (
    <SafeAreaView style={{ flex: 1 }}>
        <ScrollView>
            <DrawerItems {...props} />
        </ScrollView>
    </SafeAreaView>
)

const AppDrawerNavigator = createDrawerNavigator({
    // Teste : () => <Teste />,
    'Trocar de Motorista': Login,
    FormadePagamento,
    'Teste': Teste,
    FechoServicoRetorno,
    'Servicos': TodayService,
    Cliente,
    'Services Navigator': ServicesNavigator,
    'Teste Picker': TestePicker,
    'Trocar de Viatura': Viatura,
    'Analise': Analise,
    'Last Month Analise': LastMonthAnalise,
    Contas,
    RenderServices,
    TabNavigator,
    Agenda: Agenda,
    Dashboard: Dashboard,
    'Novo Servico': NovoServico,
    'Tomorrow': TomorrowService,
    'Yesterday': YesterdayService,
    Despesas: NovaDespesa,
}, {
        contentComponent: CustomDrawerComponent,
        drawerWidth: 300,
        contentOptions: {
            activeTintColor: 'orange'
        }
    })