import { StyleSheet, Dimensions, Platform} from 'react-native'

const { width: WIDTH } = Dimensions.get('window')

const styles = StyleSheet.create({
    backgroundContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    dashboard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '5%',
        backgroundColor: '#E6AC4A',
        flexWrap: 'wrap',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    textDashboard: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center'
    },
    data: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 45,
        height: 45,
        backgroundColor: '#535557',
    },
    icon: {
        color: '#fff',
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 10,
    },
    textareaContainer: {
        height: 100,
        padding: 5,
        backgroundColor: '#fff',
    },
    textarea: {
        textAlignVertical: 'top',  // hack android
        height: 90,
        fontSize: 14,
        color: '#535557',
    },
    buttonsLogin: {
        width: WIDTH - 35,
        alignItems: 'center',
        justifyContent: 'space-between', //testar space around e depois paddingHorizontal
        flexDirection: 'row',        
        flexWrap: 'wrap', //quebrar linha
        marginTop: 40,
        marginBottom: 40,
        //borderWidth: 1,
        //borderColor: 'red',
    },
    btnSair: {
        width: WIDTH - 260,
        height: 45,
        borderRadius: 10,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',  
        alignItems:'center',      
    },
    btnEntrar: {
        width: WIDTH - 260,
        height: 45,
        borderRadius: 10,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems:'center',
    },
    text: {
        color:'rgba(255, 255, 255, 255)',
        fontSize: 22,
        textAlign: 'center',
    },
    toolbar: {
        width: '100%',
        backgroundColor: '#f1f1f1',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    toolbarRight: {
        alignSelf: 'flex-end',
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.65)',
    },
})

export default styles;