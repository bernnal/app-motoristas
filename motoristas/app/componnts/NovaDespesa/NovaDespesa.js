import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Dimensions,
    TextInput,
    Platform,
    ScrollView,
    TouchableOpacity,
    Animated,
    TouchableWithoutFeedback,
    StyleSheet,
    Button,
    Picker,
    Alert
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Icon from 'react-native-vector-icons/FontAwesome';
import Textarea from 'react-native-textarea'
import icoMoonConfig from '../../resources/fonts/selection.json'
import bgImage from '../../images/background_v.jpg'
import styles from './styles'
//import Header from '../Header'
import Footer from '../Footer'
import axios from 'axios'
import { API_MOTORISTAS_VIATURAS, API_TIPOS_DESPESAS, API_INCLUI_DESPESA } from '../../config'

const { width: WIDTH } = Dimensions.get('window')
const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class NovoServico extends Component {
    constructor(props) {
        super(props)
        this.state = {
            api_token: this.props.navigation.state.params.api_token,
            marca: '',
            modelo: '',
            matricula: '',
            km_actuais: '',

            descViatura: '',
            descDespesa: '',
            descViaturaIOS: 'Selecione a Viatura',
            descDespesaIOS: 'Selecione a Despesa',

            viaturas: false,
            despesas: false,

            dataSource: [],
            dataSourceDespesas: [],
            choosenindex: '',

            preco: '',
            entidade: '',
            n_fatura: '',
            codigo_servico: '',
            observacoes: '',

            modalIsVisible: false,
            modalIsVisibleNovaDespesa: false,
            modalAnimatedValue: new Animated.Value(0),

        }
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-44" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_VIATURAS}`, {
            'param1': this.state.api_token
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSource) => {
                this.setState({
                    viaturas: true,
                    dataSource: dataSource.data['data'],
                })
                console.log('lista de viaturas dentro do axios', this.state.dataSource)
            })
            .catch(error => console.log(error));

        axios.get(`${API_TIPOS_DESPESAS}`, {
            params: {
                'param1': this.state.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceDespesas) => {
                this.setState({
                    despesas: true,
                    dataSourceDespesas: dataSourceDespesas.data['data'],
                })
                console.log('lista de despesas dentro do axios', this.state.dataSourceDespesas)
            })
            .catch(error => console.log(error));
    }

    onValueChange(value: string, index: string) {
        let kms = this.state.dataSource[index].km_actuais
        let marcaSelecionada = this.state.dataSource[index].marca
        let modeloSelecionado = this.state.dataSource[index].modelo
        let matriculaSelecionada = this.state.dataSource[index].matricula

        let viaturaSelecionada = marcaSelecionada + ' ' + modeloSelecionado + ' ' + matriculaSelecionada

        this.setState({
            descViatura: value,
            km_actuais: kms,
            descViaturaIOS: viaturaSelecionada
        });
    }

    onValueChangeDespesas(value: string, index: string) {
        let despesaSelecionada = this.state.dataSourceDespesas[index].nome

        this.setState({
            descDespesa: value,
            descDespesaIOS: despesaSelecionada
        });
    }

    _handlePressOpen = () => {
        if (this.state.modalIsVisible) {
            return;
        }

        this.setState({ modalIsVisible: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenNovaDespesa = () => {
        if (this.state.modalIsVisibleNovaDespesa) {
            return;
        }

        this.setState({ modalIsVisibleNovaDespesa: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressDone = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisible: false });
        });
    };

    _handlePressDoneNovaDespesa = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleNovaDespesa: false });
        });
    };

    criarDespesa = () => {
        try {
            this.setState({ error: '', loading: true })

            axios.post(`${API_INCLUI_DESPESA}`, {
                    'param1': this.state.api_token,
                    'param2': this.state.descViatura,
                    'param3': this.state.km_actuais,
                    'param4': this.state.descDespesa,
                    'param5': this.state.preco,
                    'param6': this.state.entidade,
                    'param7': this.state.n_fatura,
                    'param8': this.state.observacoes,
                    'param8': this.state.codigo_servico,
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((jsonResponse) => {
                    if (jsonResponse.data.error) {
                        Alert.alert((jsonResponse.data.error));
                    } else {
                        Alert.alert('Guardo com Sucesso!')
                    }
                })
                .catch((error) => {
                    console.log("axios error:", error);
                });
        } catch (err) {
            Alert.alert((err))
        }
    }

    render() {  

        let list_viaturas = this.state.dataSource;
        if (typeof (list_viaturas) !== undefined) {
            list_viaturas = [list_viaturas][0];
        }

        let list_despesas = this.state.dataSourceDespesas;
        if (typeof (list_despesas) !== undefined) {
            list_despesas = [list_despesas][0];
        }

        let km_selected = typeof (this.state.km_actuais) !== 'undefined' ? this.state.km_actuais : 0

        let modalPicker = null
        let modalPickerDespesas = null
        if (Platform.OS === 'ios') {
            modalPicker = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
                    </View>
                    <TouchableWithoutFeedback onPress={this._handlePressOpen}>
                        <View
                            style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column',
                            }}>
                            <Text style={{
                                paddingTop: 13,
                                height: 45,
                                fontSize: 16,
                                paddingLeft: 15,
                                borderWidth: 1,
                                borderColor: '#CCC',
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: '#000'
                            }}>{this.state.descViaturaIOS}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
            modalPickerDespesas = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-20" size={25} />
                    </View>
                    <TouchableWithoutFeedback onPress={this._handlePressOpenNovaDespesa}>
                        <View
                            style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column',
                            }}>
                            <Text style={{
                                paddingTop: 13,
                                height: 45,
                                fontSize: 16,
                                paddingLeft: 15,
                                borderWidth: 1,
                                borderColor: '#CCC',
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: '#000'
                            }}>{this.state.descDespesaIOS}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
        } else {
            modalPicker = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            placeholder='Selecione a Viatura'
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descViatura}
                            onValueChange={this.onValueChange.bind(this)}
                        >
                            {
                                list_viaturas.map((item, key) => {
                                    return <Picker.Item
                                        label={[item.marca, ' ', item.modelo, ' ', item.matricula]}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerDespesas = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-20" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            placeholder='Selecione a Despesa'
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descDespesa}
                            onValueChange={this.onValueChangeDespesas.bind(this)}
                        >
                            {
                                list_despesas.map((item, key) => {
                                    return <Picker.Item
                                        label={[item.nome]}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
        }

        _maybeRenderModal = () => {
            if (!this.state.modalIsVisible) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisible ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDone} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descViatura}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChange.bind(this)}>
                            {list_viaturas.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.marca + ' ' + ' ' + item.modelo + ' ' + item.matricula}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalNovaDespesa = () => {
            if (!this.state.modalIsVisibleNovaDespesa) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleNovaDespesa ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneNovaDespesa} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descDespesa}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeDespesas.bind(this)}>
                            {list_despesas.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={item}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        return (
            <ImageBackground
                source={bgImage}
                style={styles.backgroundContainer}>
                <View style={styles.dashboard}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Icon style={{ marginLeft: 10 }} name="bars" size={30} color="#535557" />
                    </TouchableOpacity>
                    <Text style={styles.textDashboard}></Text>
                    <MyIcon style={{ color: '#535557', marginRight: 10 }} name="fonte-tg-27" size={30} />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            height: (Dimensions.get('window').height / 10) * 10,
                            width: '95%',
                            marginTop: 10,

                        }}>

                        {modalPicker}

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: 10,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-46" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    value={km_selected}
                                    editable={true}
                                    onChangeText={(text) => { this.setState({ km_selected: text }) }}
                                    keyboardType='numeric'
                                    placeholder={"0"}
                                    placeholderTextColor={'#000'}
                                >
                                </TextInput>
                            </View>
                        </View>

                        {modalPickerDespesas}

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: 10,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-47" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    keyboardType='numeric'
                                    placeholder={"0 €"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.preco}
                                    onChangeText={preco => this.setState({ preco })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: 10,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-58" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    placeholder={"Entidade"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.entidade}
                                    onChangeText={entidade => this.setState({ entidade })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: 10,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-50" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    placeholder={"Nº Fatura"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.n_fatura}
                                    onChangeText={n_fatura => this.setState({ n_fatura })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'flex-start',
                            alignItems: 'center',
                            marginTop: 10,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-71" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    placeholder={"ID Serviço"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.codigo_servico}
                                    onChangeText={codigo_servico => this.setState({ codigo_servico })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={styles.container}>
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={styles.textarea}
                                // onChangeText={this.onChange}
                                // defaultValue={this.state.text}
                                maxLength={120}
                                placeholder={'Observações...'}
                                placeholderTextColor={'#000'}
                                underlineColorAndroid={'transparent'}
                                borderRadius={10}
                                value={this.state.observacoes}
                                onChangeText={observacoes => this.setState({ observacoes })}
                            />
                        </View>

                        <View style={styles.buttonsLogin}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                                <Text style={styles.text}> Sair </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.criarDespesa} style={styles.btnEntrar}>
                                <Text style={styles.text}> Enviar </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                <Footer />
                {_maybeRenderModal()}
                {_maybeRenderModalNovaDespesa()}
            </ImageBackground>
        )
    }
}