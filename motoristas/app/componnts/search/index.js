import React from 'react';
import { StyleSheet, View } from 'react-native';
import SearchBar from './SearchBar'
import SearchResults from './SearchResults';
 
class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <SearchBar/>
        <SearchResults/>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default App