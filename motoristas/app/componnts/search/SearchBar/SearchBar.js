import React, { Component } from 'react';
import { Text, TouchableOpacity, TextInput, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import styles from './SearchBarStyles'

class SearchBar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ''
    }
  }

  searchOnMedium = () => {
	// https://www.googleapis.com/customsearch/v1?key=AIzaSyD5cdDfwONnBTXg4c0MGQWil-4KqrLI3MM&cx=017576662512468239146:omuauf_lfve&q=lectures
	// let apiURL = 'https://www.googleapis.com/customsearch/v1'
	// let API_KEY = 'AIzaSyD5cdDfwONnBTXg4c0MGQWil-4KqrLI3MM'
	// let cx = '017576662512468239146:omuauf_lfve'
	// let q = 'lectures'
    const URL = 'https://www.googleapis.com/customsearch/v1?key=AIzaSyD5cdDfwONnBTXg4c0MGQWil-4KqrLI3MM&cx=017576662512468239146:omuauf_lfve&q=lectures'
    fetch(URL, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => {
      response.json().then((data) => {
        this.props.searchResults(data.items);
	  })
	  console.log(response)
    }).catch((error) => console.log(error));
  }

  render() {
    return (
      <View style={styles.searchBarContainer}>
        <TextInput
          placeholder='Enter your search terms'
          style={styles.textInputSearch}
          underlineColorAndroid={'transparent'}
          onChangeText={(searchTerm) => this.setState({ searchTerm })}
          value={this.state.searchTerm}
        />
        <TouchableOpacity
          style={styles.textSearchButton}
          onPress={() => this.searchOnMedium('react native')}
        >
          <Icon name="search" size={16} color="#000" />
        </TouchableOpacity>
      </View>
    )
  }
}

export default SearchBar