import React, { Component } from 'react'
import { View, Text, Image, Platform, StyleSheet, TouchableOpacity } from 'react-native'
import { withNavigation } from 'react-navigation'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import Icon from 'react-native-vector-icons/FontAwesome';
import icoMoonConfig from '../resources/fonts/selection.json'
import logoFooter from '../images/logo-tg-h-01.png'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Footer extends Component {

    render() {
        return (
            <View style={styles.logoContainerFooter}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')}>
                    <MyIcon style={{ color: '#fff', marginLeft: 10 }} name="fonte-tg-68" size={30} />
                </TouchableOpacity>
                <Image source={logoFooter} style={styles.logoFooter} />
                <MyIcon style={{ color: '#fff', marginRight: 10 }} name="fonte-tg-60" size={30} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    logoContainerFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '5%',
        backgroundColor: '#5A5B60',
        flexWrap: 'wrap',
    },
    logoFooter: {
        width: '40%',
        height: '40%',
        resizeMode: 'contain',
    }
})

export default withNavigation(Footer)