import React, { Component } from 'react'
import { Text, View, ImageBackground, Dimensions, Image, TextInput, TouchableOpacity, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import axios from 'axios'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { API_MOTORISTAS_LOGIN } from '../../config'
import icoMoonConfig from '../../resources/fonts/selection.json'
import styles from './styles'
import bgImage from '../../images/fundo-vertical-preto.jpg'
import logo from '../../images/logo-tg-v-01.png'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            nif: '111111111',
            user: 'emanuel',
            password: '123456',
            loading: false

        }
        this.loginUser = this.loginUser.bind(this)
    }

    loginUser = () => {
        try {
            const { nif, user, password } = this.state
            this.setState({ error: '', loading: true })

            axios.post(`${API_MOTORISTAS_LOGIN}`,
                {
                    'param3': this.state.nif,
                    'param1': this.state.user,
                    'param2': this.state.password
                }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((response) => {
                    if (response.data.error) {
                        Alert.alert((response.data.error));
                    } else {
                        const api_token = response.data.api_token
                        const app_footer = response.data.app_footer
                        const logo = response.data.logo
                        this.props.navigation.navigate('Trocar de Viatura', {
                            api_token: api_token,
                            app_footer: app_footer,
                            logo: logo
                        })
                    } console.log("reactNativeDemo", "response get details:" + response);
                })
                .catch((error) => {
                    console.log("axios error:", error);
                });
        } catch (err) {
            Alert.alert((err))
        }
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-33" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    render() {
        return (
            <ImageBackground
                source={bgImage}
                style={styles.backgroundContainer}>
                <View
                    style={{
                        height: (Dimensions.get('window').height / 10) * 10,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                    <View style={styles.logoContainer}>
                        <Image source={logo} style={styles.logo} />
                    </View>
                    <View style={styles.inputText}>
                        <View style={styles.iconContainer}>
                            <MyIcon style={styles.icon} name="fonte-tg-43" size={25} />
                        </View>
                        <TextInput
                            style={styles.input}
                            placeholder={'NIF'}
                            placeholderTextColor={'rgba(0, 0, 0, 0.6)'}
                            underlineColorAndroid='transparent'
                            value={this.state.nif}
                            onChangeText={nif => this.setState({ nif })}
                        />
                    </View>
                    <View style={styles.inputText}>
                        <View style={styles.iconContainer}>
                            <MyIcon style={styles.icon} name="fonte-tg-39" size={25} />
                        </View>
                        <TextInput
                            // autoCapitalize="none" -> transforma tudo em minusculo,
                            style={styles.input}
                            placeholder={'Utilizador'}
                            placeholderTextColor={'rgba(0, 0, 0, 0.6)'}
                            underlineColorAndroid='transparent'
                            value={this.state.user}
                            onChangeText={user => this.setState({ user })}
                        />
                    </View>
                    <View style={styles.inputText}>
                        <View style={styles.iconContainer}>
                            <MyIcon style={styles.icon} name="fonte-tg-73" size={25} />
                        </View>
                        <TextInput
                            style={styles.input}
                            placeholder={'Senha'}
                            secureTextEntry={true}
                            placeholderTextColor={'rgba(0, 0, 0, 0.6)'}
                            underlineColorAndroid='transparent'
                            value={this.state.password}
                            onChangeText={password => this.setState({ password })}
                        />
                    </View>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity style={styles.btnSair}>
                            <Text style={styles.text}> Sair </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.loginUser} style={styles.btnEntrar}>
                            <Text style={styles.text}> Entrar </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        )
    }
}

export default Login