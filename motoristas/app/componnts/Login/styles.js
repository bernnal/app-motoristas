import React, { Component } from 'react'
import { StyleSheet, Dimensions } from 'react-native'

const { width: WIDTH } = Dimensions.get('window')

const styles = StyleSheet.create({
    backgroundContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center'
    },
    logoContainer: {
        flexDirection: 'row',
        justifyContent: 'center',  
        alignItems: 'center',
        flexWrap: 'wrap',
        width: 200,
        height: 200,
        backgroundColor: '#fff',
        borderRadius: 100,
        marginTop: 50,
    },
    logo: {
        justifyContent: 'center',  
        flexWrap: 'wrap',
        maxWidth: 150,
        maxHeight: 150,
        alignItems: 'center',
        resizeMode:'contain',
    },
    input: {
        width: WIDTH - 130,
        height: 45,
        fontSize: 16,
        paddingLeft: 15,
        backgroundColor: 'rgba(255, 255, 255, 255)',
    },
    inputText: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
        borderRadius: 5,
    },
    iconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 45,
        height: 45,
        backgroundColor: '#535557',
    },  
    icon: {
        color: '#fff',
    },
    buttonsLogin: {
        width: WIDTH - 85,
        alignItems: 'center',
        justifyContent: 'space-between', //testar space around e depois paddingHorizontal
        flexDirection: 'row',        
        flexWrap: 'wrap', //quebrar linha
        marginTop: 40,
        //borderWidth: 1,
        //borderColor: 'red',
    },
    btnSair: {
        width: WIDTH - 260,
        height: 45,
        borderRadius: 10,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',  
        alignItems:'center',      
    },
    btnEntrar: {
        width: WIDTH - 260,
        height: 45,
        borderRadius: 10,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems:'center',
    },
    text: {
        color:'#fff',
        fontSize: 22,
        textAlign: 'center',
    },
})

export default styles;