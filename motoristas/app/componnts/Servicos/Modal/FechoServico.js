import React, { Component } from 'react'
import {
    Text,
    View,
    Modal,
    TextInput,
    TouchableWithoutFeedback,
    TouchableOpacity,
    ScrollView,
    Platform,
    Picker,
    Button,
    Animated,
    StyleSheet,
    Dimensions,
    Alert,
} from 'react-native'
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
    SlideAnimation,
    ScaleAnimation,
} from 'react-native-popup-dialog';
import Textarea from 'react-native-textarea'
import { Col, Row, Grid } from "react-native-easy-grid"
import Icon from 'react-native-vector-icons/FontAwesome';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../resources/fonts/selection.json'
import {
    API_MOTORISTAS_FORMAS_PAGAMENTOS,
    API_TIPOS_COBRANCA,
    API_ENVIA_EMAIL_CLIENTE,
    API_MOTORISTA_FECHO_SERVICO
} from '../../../config'
import axios from 'axios'
import FechoServicoRetorno from './FechoServicoRetorno'
import ConfirmaFecharServico from './ConfirmaFecharServico'
import { withNavigation } from 'react-navigation'

const { width: WIDTH } = Dimensions.get('window')
const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class FechoServico extends Component {

    constructor(props) {
        super(props)
        this.state = {
            api_token: '',

            descFormaPagamento: '',
            descFormaPagamentoIOS: 'Pagamento',

            descFormaAtribuir: '',
            descFormaAtribuirIOS: 'Atribuir a',

            observacoes: '',

            showPopup: false,

            showModalRetorno: false,
            showModalConfirmaFechoServico: false,
            modalIsVisibleAtribuir: false,
            modalIsVisible: false,
            modalAnimatedValue: new Animated.Value(0),
        }
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_FORMAS_PAGAMENTOS}`, {
            params: {
                'param1': this.props.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSource) => {
                console.log('formas de pagamento: ', dataSource)
                this.setState({
                    dataSource: dataSource.data['data'],
                })
                console.log('data source formas de pagamento', this.state.dataSource)
            })
            .catch(error => console.log(error));

        axios.get(`${API_TIPOS_COBRANCA}`, {
            params: {
                'param1': this.props.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceAtribuir) => {
                this.setState({
                    dataSourceAtribuir: dataSourceAtribuir.data['data'],
                })
            })
            .catch(error => console.log(error));
    }

    onValueChange(value: string, index: string) {

        let formaPagamento = this.state.dataSource[index].nome

        this.setState({
            descFormaPagamento: value,
            descFormaPagamentoIOS: formaPagamento,
        })
    }

    onValueChangeAtribuir(value: string, index: string) {

        let atribuir = this.state.dataSourceAtribuir[index].nome

        this.setState({
            descFormaAtribuir: value,
            descFormaAtribuirIOS: atribuir,
        })
    }

    _handlePressOpenAtribuir = () => {
        if (this.state.modalIsVisibleAtribuir) {
            return;
        }

        this.setState({ modalIsVisibleAtribuir: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        })
    }

    _handlePressDoneAtribuir = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleAtribuir: false })
        })
    }

    _handlePressOpen = () => {
        if (this.state.modalIsVisible) {
            return;
        }

        this.setState({ modalIsVisible: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        })
    }

    _handlePressDone = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisible: false })
        })
    }

    sendEmail = () => {
        axios.post(`${API_ENVIA_EMAIL_CLIENTE}`, {
            'param1': this.props.api_token,
            'param2': this.props.emailF,
            'param3': this.props.clienteIdF,
            'param4': this.props.totalCobrarF,
            'param5': this.props.codigoServicoF,
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((jsonResponseEmail) => {
                if (jsonResponseEmail.data.error) {
                    console.log('Rejeitado.');
                    Alert.alert(jsonResponseEmail.data.error)
                } else {
                    Alert.alert('Email não enviado!')
                }
            })
            .catch((error) => {
                console.log("axios error:", error);
            })
    }

    fecharServico = () => {
        axios.post(`${API_MOTORISTA_FECHO_SERVICO}`, {
            'param1': this.props.api_token,
            'param2': this.props.idF,
            'param3': 'TZ',
            'param4': this.props.totalCobrarF,
            'param5': this.state.observacoes,
            'param6': '',
            'param7': '',
            'param8': 5,
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((jsonResponseFechoServico) => {
                if (jsonResponseFechoServico.data.error) {
                } else {
                    Alert.alert((jsonResponseFechoServico.data.error));
                }
            })
            .catch((error) => {
                console.log("axios error:", error);
            })
    }

    render() {

        // let totTotal = Number(this.state.receita) - Number(this.state.totalSomaD)
        // let totFixed = totTotal.toFixed(2)
        // this.state.totalTotal = totFixed

        let valorFalta = Number(this.props.totalCobrarF) - Number(this.props.valorServicoF)
        let faltaFixed = valorFalta.toFixed(2)
        console.log('falta fixed: ', faltaFixed)



        let token = this.props.api_token
        this.state.api_token = token

        let emailCliente = this.props.emailF !== null ? this.props.emailF : 'null'

        let list_pagamento = this.state.dataSource;
        if (typeof (list_pagamento) !== undefined) {
            list_pagamento = [list_pagamento][0];
        }

        let list_atribuir = this.state.dataSourceAtribuir;
        if (typeof (list_atribuir) !== undefined) {
            list_atribuir = [list_atribuir][0];
        }

        let pickerFormasPagamento = null
        let pickerAtribuir = null
        if (Platform.OS === 'ios') {
            pickerFormasPagamento = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Grid>
                        <Col>
                            <Row>
                                <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Valor Cobrado</Text>
                            </Row>
                            <Row>
                                <View style={styles.iconContainer}>
                                    <MyIcon style={styles.iconStyle} name='fonte-tg-47' />
                                    <TextInput
                                        style={{ flex: 1, paddingHorizontal: 5 }}
                                        placeholder="0.0"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor='#222'
                                        textAlign="right"
                                        value={this.props.valorServicoF}
                                    />
                                </View>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <Text style={{ paddingVertical: 5 }}>Forma de Pagamento</Text>
                            </Row>
                            <Row>
                                <View style={{
                                    width: '93%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: '#FFF',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    height: 45,
                                    borderRadius: 5,
                                }}>
                                    <TouchableWithoutFeedback onPress={this._handlePressOpen}>
                                        <View>
                                            <Text style={{
                                                justifyContent: 'center',
                                                backgroundColor: '#fff',
                                                paddingLeft: 10,
                                            }}>{this.state.descFormaPagamentoIOS}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </Row>
                        </Col>
                    </Grid>
                </View>
            )

            pickerAtribuir = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Grid>
                        <Col>
                            <Row>
                                <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Valor em Falta</Text>
                            </Row>
                            <Row>
                                <View style={styles.iconContainer}>
                                    <MyIcon style={styles.iconStyle} name='fonte-tg-47' />
                                    <TextInput
                                        style={{ flex: 1, paddingHorizontal: 5 }}
                                        placeholder="30.0"
                                        underlineColorAndroid="transparent"
                                        placeholderTextColor='#222'
                                        textAlign="right"
                                        value={faltaFixed}
                                    />
                                </View>
                            </Row>
                        </Col>
                        <Col>
                            <Row>
                                <Text style={{ paddingVertical: 5 }}>Atribuir</Text>
                            </Row>
                            <Row>
                                <View style={{
                                    width: '93%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: '#FFF',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    height: 45,
                                    borderRadius: 5,
                                }}>
                                    <TouchableWithoutFeedback onPress={this._handlePressOpenAtribuir}>
                                        <View>
                                            <Text style={{
                                                justifyContent: 'center',
                                                backgroundColor: '#fff',
                                                paddingLeft: 10,
                                            }}>{this.state.descFormaAtribuirIOS}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </Row>
                        </Col>
                    </Grid>
                </View>
            )
        } else {
            pickerFormasPagamento = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Valor Cobrado</Text>
                        <View style={styles.iconContainer}>
                            <MyIcon style={styles.iconStyle} name="fonte-tg-47" />
                            <TextInput
                                style={{ flex: 1, paddingHorizontal: 5 }}
                                placeholder="30.0"
                                underlineColorAndroid="transparent"
                                placeholderTextColor='#222'
                                textAlign="right"
                            />
                        </View>
                    </View>
                    <View>
                        <Text style={{ paddingVertical: 5 }}>Forma de Pagamento</Text>
                        <View style={{
                            height: 45,
                            flexDirection: 'column',
                            width: '68%',
                            paddingRight: 5
                        }}>
                            <Picker
                                placeholderText='Forma Pagamento'
                                mode='dropdown'
                                placeholderStyle={{ color: '#000' }}
                                selectedValue={this.state.descFormaPagamento}
                                itemStyle={{
                                    fontSize: 14,
                                    color: '#000',
                                }}
                                onValueChange={this.onValueChange.bind(this)}>
                                {list_pagamento.map((item, key) => {
                                    return (
                                        <Picker.Item
                                            label={item.nome}
                                            value={item.id}
                                            key={key}
                                        />
                                    );
                                })}
                            </Picker>
                        </View>
                    </View>
                </View>
            )
            pickerAtribuir = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View>
                        <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Valor em Falta</Text>
                        <View style={styles.iconContainer}>
                            <MyIcon style={styles.iconStyle} name="fonte-tg-47" />
                            <TextInput
                                style={{ flex: 1, paddingHorizontal: 5 }}
                                placeholder="0.0"
                                underlineColorAndroid="transparent"
                                placeholderTextColor='#222'
                                textAlign="right"
                            />
                        </View>
                    </View>
                    <View>
                        <Text style={{ paddingVertical: 5 }}>Atribuir a</Text>
                        <View style={{
                            height: 45,
                            flexDirection: 'column',
                            width: '68%',
                            paddingRight: 5
                        }}>
                            <Picker
                                placeholderText='Atribuir'
                                mode='dropdown'
                                placeholderStyle={{ color: '#000' }}
                                selectedValue={this.state.descFormaAtribuir}
                                itemStyle={{
                                    fontSize: 14,
                                    color: '#000',
                                }}
                                onValueChange={this.onValueChangeAtribuir.bind(this)}>
                                {list_atribuir.map((item, key) => {
                                    return (
                                        <Picker.Item
                                            label={item.nome}
                                            value={item.id}
                                            key={key}
                                        />
                                    );
                                })}
                            </Picker>
                        </View>
                    </View>
                </View>
            )
        }

        _maybeRenderModal = () => {
            if (!this.state.modalIsVisible) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            })

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisible ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDone} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descFormaPagamento}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChange.bind(this)}>
                            {list_pagamento.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalAtribuir = () => {
            if (!this.state.modalIsVisibleAtribuir) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            })

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleAtribuir ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneAtribuir} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descFormaAtribuir}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeAtribuir.bind(this)}>
                            {list_atribuir.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={false}>
                <FechoServicoRetorno
                    isVisible={this.state.showModalRetorno}
                    codigoServicoF={this.props.codigoServicoF}
                    api_token={this.props.api_token}
                    onCancel={() => this.setState({ showModalRetorno: false })}
                />
                <ConfirmaFecharServico
                    isVisible={this.state.showModalConfirmaFechoServico}
                />

                {/* {
                    this.state.showPopup ?
                        <View style={{ height: 50, borderWidth: 1, marginVertical: 50 }}>
                            <Dialog
                                visible={this.state.showPopup}
                                onTouchOutside={() => {
                                    this.setState({ showPopup: false });
                                }}
                            >
                                <DialogContent style={{ backgroundColor: 'green', height: 100, borderWidth: 1, }}>
                                    <View><Text>Teste Popup</Text></View>
                                </DialogContent>
                            </Dialog>
                        </View>
                        :
                        null
                } */}


                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.dashboardHeader}>
                        <Text style={styles.header}> Fecho de Serviço </Text>
                        <Text style={styles.header}> {this.props.codigoServicoF} </Text>
                    </View>
                </View>
                <View
                    style={{ height: 1, width: '100%', backgroundColor: '#E6AC4A' }}
                />
                <ScrollView>
                    <View>
                        <Text style={{ padding: 5, fontWeight: 'bold' }}>Valores a Cobrar do Serviço</Text>
                    </View>
                    <View
                        style={{ height: 2, width: '100%', backgroundColor: '#E6AC4A' }}
                    />
                    <View>
                        <View style={{ marginVertical: 5, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                            <Text>Serviço Atual</Text>
                            <Text>{this.props.valorServicoF}€</Text>
                        </View>
                        <View style={{ marginVertical: 5, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                            <Text>Extras ao Serviço</Text>
                            <Text>{this.props.extrasServicoF}€</Text>
                        </View>
                        <View style={{ marginVertical: 5, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                            <Text>Valor do Retorno</Text>
                            <Text>{this.props.valorRetornoF}€</Text>
                        </View>
                        <View style={{ marginVertical: 5, flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                            <Text>Total a Pagar</Text>
                            <Text>{this.props.totalCobrarF}€</Text>
                        </View>
                    </View>
                    <View
                        style={{ height: 2, width: '100%', backgroundColor: '#E6AC4A' }}
                    />
                    <View>
                        <Text style={{ padding: 5, fontWeight: 'bold' }}>Formas de Pagamento</Text>
                    </View>
                    <View
                        style={{ height: 2, width: '100%', backgroundColor: '#E6AC4A', marginBottom: 5 }}
                    />
                    {pickerFormasPagamento}
                    {pickerAtribuir}
                    <View style={{ paddingHorizontal: 5, paddingVertical: 20 }}>
                        <Text>Observações:</Text>
                        <View style={styles.obsContainer}>
                            <MyIcon style={styles.iconObs} name="fonte-tg-60" size={20} />
                            <Textarea
                                style={{ marginTop: 65, marginLeft: 10, height: 45, width: '90%', }}
                                underlineColorAndroid={'transparent'}
                                borderRadius={6}
                                value={this.state.observacoes}
                                onChangeText={observacoes => this.setState({ observacoes })}
                            />
                        </View>
                    </View>
                    <View
                        style={{ height: 2, width: '100%', backgroundColor: '#E6AC4A' }}
                    />
                    <View>
                        <Text style={{ padding: 5, fontWeight: 'bold' }}>Comprovativo de Pagamento</Text>
                    </View>
                    <View
                        style={{ height: 2, width: '100%', backgroundColor: '#E6AC4A' }}
                    />
                    <View style={{ width: '100%', flexDirection: 'column', }}>
                        <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Email Cliente</Text>
                        <View style={styles.iconEmail}>
                            <Icon style={styles.iconStyle} name="at" size={20} color="#222" />
                            <TextInput
                                style={{ flex: 1, paddingLeft: 5, width: '90%' }}
                                placeholder={"null"}
                                value={emailCliente}
                                underlineColorAndroid="transparent"
                                placeholderTextColor='#222'
                                textAlign="left"
                            />
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                        <TouchableOpacity onPress={this.props.onCancel}>
                            <View style={styles.buttonQuestionario}>
                                <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>CANCELAR</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.fecharServico()}>
                            <View style={styles.buttonFecharServico}>
                                <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>FECHAR SERVIÇO</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5, marginBottom: 30 }}>
                        <TouchableOpacity onPress={() => this.setState({ showModalRetorno: true })}>
                            <View style={styles.buttonCriarRetorno}>
                                <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>CRIAR RETORNO</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.sendEmail()}>
                            <View style={styles.buttonCancelar}>
                                <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>ENV / COMPROVATIVO</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                {_maybeRenderModal()}
                {_maybeRenderModalAtribuir()}
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#f6f6f6',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 20
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        //marginTop: 50,
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    iconStyle: {
        padding: 5,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    iconObs: {
        paddingLeft: 15,
        marginLeft: 15,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    iconContainer: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        marginLeft: 5
    },
    iconEmail: {
        width: '95%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        marginLeft: 5
    },
    obsContainer: {
        width: '97.5%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        // marginLeft: 5
    },
    buttonEnviar: {
        backgroundColor: '#3C80BC',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 100,
        height: 45,
        marginTop: 25
    },
    buttonCancelar: {
        backgroundColor: '#3C80BC',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonFecharServico: {
        backgroundColor: '#7BB758',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonCriarRetorno: {
        backgroundColor: '#269940',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonQuestionario: {
        backgroundColor: '#00AAC6',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    toolbar: {
        width: '100%',
        backgroundColor: '#f1f1f1',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    toolbarRight: {
        alignSelf: 'flex-end',
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.65)',
    },
})

export default withNavigation(FechoServico);