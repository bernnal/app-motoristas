import React, { Component } from 'react'
import { Modal, View, Text, TouchableWithoutFeedback, TouchableOpacity, StyleSheet } from 'react-native'

export default class FormadePagamento extends Component {

    render() {
        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={true}>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.dashboardHeader}>
                        <Text style={styles.header}> Existem Valores de Retorno a Cobrar </Text>
                    </View>
                    <View>
                        <Text style={{ color: '#222', padding: 5, marginVertical: 10 }}>Selecione uma forma de pagamento:</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.buttonServicoActual}>
                            <Text style={{ fontSize: 12,  padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>SERVIÇO ACTUAL 30,00€</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonServicoActualRetorno}>
                            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>SERVIÇO ACTUAL + RETORNO 60,00€</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5, marginBottom: 10 }}>
                        <TouchableOpacity style={styles.buttonTotalRetorno}>
                            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>TOTAL NO RETORNO 60,00€</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonManual}>
                            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>MANUAL</Text>
                        </TouchableOpacity>
                    </View>                    
                </View>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#F2F2F2',
        justifyContent: 'space-between',
        width: '100%',
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
    },
    buttonServicoActual: {
        backgroundColor: '#00AAC6',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonServicoActualRetorno: {
        backgroundColor: '#7BB758',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonTotalRetorno: {
        backgroundColor: '#8FA594',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonManual: {
        backgroundColor: '#269940',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
})