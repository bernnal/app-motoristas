import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default props => {

    return (
        <View style={styles.container}>
            <View>
                <Text style={styles.description}>{props.typeExtra}</Text>
                <Text style={styles.description}>{props.price}</Text>
                <Text style={styles.description}>{props.qtde}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    description: {
        color: '#222',
        fontSize: 15,
    }
})