import React, { Component } from 'react'
import {
    View,
    Text,
    Dimensions,
    TextInput,
    Platform,
    ScrollView,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TouchableHighlight,
    Animated,
    StyleSheet,
    Button,
    Picker,
    DatePickerIOS,
    Alert,
    Modal,
    FlatList,
} from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid"
import DatePicker from 'react-native-datepicker'
// import { Container, Content, Picker, Form, } from "native-base";
import Textarea from 'react-native-textarea'
import {
    API_MOTORISTAS_TIPOS_EXTRAS,
    API_MOTORISTAS_INCLUI_EXTRAS_TRANSFERZONA,
    API_MOTORISTAS_EXTRAS_TRANSFERZONA
} from '../../../config'
import axios from 'axios'
import { withNavigation } from 'react-navigation'

const { width: WIDTH } = Dimensions.get('window')

class Extras extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showAddExtra: false,
            descExtra: '',
            descExtraIOS: 'Selecione um Extra',
            idExtra: '',
            extra: false,
            dataSourceTiposExtras: [],
            modalIsVisibleExtras: false,
            precoExtra: 0,
            qtdeExtra: 0,
            totalMultiplicacao: 0,
            observacoes: '',
            sizeExtras: 0,
            runExtras: false,

            modalAnimatedValue: new Animated.Value(0),
        }
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_TIPOS_EXTRAS}`, {
            params: {
                'param1': this.props.api_token,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceTiposExtras) => {
                this.setState({
                    dataSourceTiposExtras: dataSourceTiposExtras.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));

        // axios.get(`${API_MOTORISTAS_EXTRAS_TRANSFERZONA}`, {
        //     params: {
        //         'param1': this.props.api_token,
        //         'param2': this.props.idE,
        //     }
        // }, {
        //         "headers": {
        //             'Content-Type': 'application/json',
        //         }
        //     }).then((dataSourceQtdeExtras) => {
        //         console.log('dataSourceQtdeExtras', dataSourceQtdeExtras)
        //         this.setState({
        //             dataSourceQtdeExtras: dataSourceQtdeExtras.data['data'],
        //         })
        //             .catch(error => console.log(error));
        //     })
        //     .catch(error => console.log(error));

        // this.setState({ arrayHolder: [...this.array] })
    }

    componentDidUpdate() {
        axios.get(`${API_MOTORISTAS_EXTRAS_TRANSFERZONA}`, {
            params: {
                'param1': this.props.api_token,
                'param2': this.props.idE,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceQtdeExtras) => {
                let tamanhoBagagens = dataSourceQtdeExtras.data['data'].length
                console.log('tamanhoBagagens', tamanhoBagagens)
                this.setState({
                    tamanhoBagagens,
                    dataSourceQtdeExtras: dataSourceQtdeExtras.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));
    }

    inserirExtra = () => {

        this.state.tamanhoBagagens > 0 && this.state.showAddExtra == false ?
            this.setState({
                showAddExtra: true
            })
            :
            axios.post(`${API_MOTORISTAS_INCLUI_EXTRAS_TRANSFERZONA}`, {
                'param1': this.props.api_token,
                'param2': this.state.idExtra,
                'param3': this.state.qtdeExtra,
                'param4': this.state.totalMultiplicacao,
                'param5': this.state.observacoes,
                'param6': this.props.idE,
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((jsonResponseInserirExtra) => {
                    if (jsonResponseInserirExtra.data.error) {
                        this.getExtras();
                        Alert.alert((jsonResponseInserirExtra.data.error))
                    } else {
                        console.log('Não Rejeitado.');
                    }
                })
                .catch((error) => {
                    console.log("axios error:", error);
                })
    }

    onValueChangeTiposExtras(value: string, index: string) {
        let idExtra = this.state.dataSourceTiposExtras[index].id
        let extraSelecionado = this.state.dataSourceTiposExtras[index].nome
        let precoExtra = this.state.dataSourceTiposExtras[index].preco

        this.setState({
            descExtra: value,
            idExtra: idExtra,
            descExtraIOS: extraSelecionado,
            precoExtra: precoExtra,
        });
    }

    _handlePressOpenTiposExtras = () => {
        if (this.state.modalIsVisibleExtras) {
            return;
        }

        this.setState({ modalPickerTiposExtras: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressDoneTiposExtras = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalPickerTiposExtras: false });
        });
    };

    getExtras = () => {
        // axios.get(`${API_MOTORISTAS_EXTRAS_TRANSFERZONA}`, {
        //     params: {
        //         'param1': this.props.api_token,
        //         'param2': this.props.idE,
        //     }
        // }, {
        //         "headers": {
        //             'Content-Type': 'application/json',
        //         }
        //     }).then(responseJsonExtra => {
        //         let tamanhoExtras = responseJsonExtra.data['data'].length
        //         console.log('tamanhoExtras dentro de Extras: ', tamanhoExtras)
        //         const dataSourceExtra = responseJsonExtra.data['data']
        //         responseJsonExtra.data['data'].length >= 1 ?
        //             this.setState({
        //                 sizeExtras: tamanhoExtras,
        //                 dataSourceExtra,
        //             })
        //             : null
        //     })
        //     .catch(error => console.log(error));
    }

    joinData = () => {
        // this.array.push({
        //     typeExtra: this.state.typeExtraHolder,
        //     qtde: this.state.qtdeHolder,
        //     price: this.state.priceHolder,
        // })
        // this.setState({ arrayHolder: [...this.array] })

        this.setState({
            showAddExtra: true
        })
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{ height: 1, width: '100%', backgroundColor: '#607D88', marginTop: 3 }}
            />
        )
    }

    render() {

        console.log('tamanhoBagagens', this.state.tamanhoBagagens)

        this.state.totalMultiplicacao = this.state.precoExtra * this.state.qtdeExtra

        let list_tiposExtras = this.state.dataSourceTiposExtras;
        if (typeof (list_tiposExtras) !== undefined) {
            list_tiposExtras = [list_tiposExtras][0];
        }

        let modalPickerTiposExtras = null
        if (Platform.OS === 'ios') {
            modalPickerTiposExtras = (
                <View style={{
                    height: 45,
                    flexDirection: 'column',
                    backgroundColor: '#FFF',
                    borderWidth: 1,
                    borderColor: '#CCC',
                    borderRadius: 6
                }}>
                    <TouchableOpacity onPress={this._handlePressOpenTiposExtras}>
                        <View style={{
                            justifyContent: 'center',
                            height: 45,
                            paddingLeft: 15,
                        }}>
                            <Text>
                                {this.state.descExtraIOS}
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
            )
        } else {
            modalPickerTiposExtras = (
                <View style={{
                    height: 45,
                    flexDirection: 'column',
                }}>
                    {/* <Container style={{ backgroundColor: '#FFF', borderWidth: 1, borderColor: '#CCC', borderRadius: 6 }}>
                        <Content>
                            <Form>
                                <Picker
                                    mode="dropdown"
                                    iosHeader="Selecione o Extra"
                                    style={{ width: undefined }}
                                    selectedValue={this.state.selected}
                                    onValueChange={this.onValueChange.bind(this)}
                                >
                                    <Picker.Item label="Água 1,5 L" value="key0" />
                                    <Picker.Item label="Vinho" value="key1" />
                                    <Picker.Item label="Suco" value="key2" />
                                    <Picker.Item label="Garrafa de Champagne" value="key3" />
                                    <Picker.Item label="Refrigerante" value="key4" />
                                </Picker>
                            </Form>
                        </Content>
                    </Container> */}
                </View>
            )
        }

        _maybeRenderModalTiposExtras = () => {
            if (!this.state.modalPickerTiposExtras) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalPickerTiposExtras ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneTiposExtras} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descExtra}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeTiposExtras.bind(this)}>
                            {list_tiposExtras.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={false}>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <ScrollView>
                    <View style={styles.container}>
                        <View style={styles.dashboardHeader}>
                            <Text style={styles.header}> Extras ao Serviço </Text>
                            <Text style={styles.header}> {this.props.codigoServicoE} </Text>
                        </View>
                        {
                            this.state.tamanhoBagagens > 0 ?
                                <View>
                                    <View style={{ paddingHorizontal: 5, backgroundColor: '#d9d9d9', height: 25 }}>
                                        <Grid>
                                            <Col style={{ width: '40%' }}><Text style={{ margintop: 10 }} >Nome</Text></Col>
                                            <Col style={{ width: '20%' }}><Text style={{ margintop: 10 }} >Qtde</Text></Col>
                                            <Col style={{ width: '18%' }}><Text style={{ margintop: 10 }} >P/Unit</Text></Col>
                                            <Col style={{ width: '18%' }}><Text style={{ margintop: 10 }} >P/Total</Text></Col>
                                        </Grid>
                                    </View>
                                    <FlatList
                                        data={this.state.dataSourceQtdeExtras}
                                        ItemSeparatorComponent={this.FlatListItemSeparator}
                                        renderItem={({ item, index }) =>
                                            <Grid>
                                                <Col style={{ width: '40%', marginTop: 10, marginLeft: 5 }}>
                                                    <Col>
                                                        <Text>{item.nome}</Text>
                                                        <Text style={{ fontWeight: 'bold' }}>Obs: <Text style={{ fontWeight: '400' }}>{item.observacao}</Text></Text>
                                                    </Col>
                                                </Col>
                                                <Col style={{ width: '20%', marginVertical: 10 }}><Text>{item.qtde}</Text></Col>
                                                <Col style={{ width: '18%', marginVertical: 10 }}><Text>{item.preco} €</Text></Col>
                                                <Col style={{ width: '18%', marginVertical: 10 }}><Text>{(item.preco * item.qtde).toFixed(2)} €</Text></Col>
                                            </Grid>
                                        }
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                                :
                                null
                        }
                        {
                            this.state.showAddExtra ?
                                <View>
                                    <View
                                        style={{ height: 3, width: '100%', backgroundColor: '#E6AC4A', marginTop: 20 }}
                                    />
                                    <View
                                        isVisible={this.state.showAddExtra}
                                        onSave={this.joinData}
                                        onCancel={() => this.setState({ showAddExtra: false })}
                                        style={{ marginTop: 10, marginHorizontal: 5 }}>
                                        <View style={{ marginHorizontal: 5 }}>
                                            <Text>Tipo de Extras</Text>
                                            {modalPickerTiposExtras}
                                        </View>
                                        <View style={{ marginTop: 5, marginHorizontal: 5 }}>
                                            <Grid>
                                                <Col>
                                                    <Text>Quantidade</Text>
                                                    <TextInput
                                                        style={styles.inputQtde}
                                                        placeholderTextColor={'#222'}
                                                        keyboardType='numeric'
                                                        placeholder={"0"}
                                                        value={this.state.qtdeExtra}
                                                        onChangeText={qtdeExtra => this.setState({ qtdeExtra })}
                                                    />
                                                </Col>
                                                <Col>
                                                    <Text>P/Unit</Text>
                                                    <Text style={{ height: 40, textAlign: 'left', paddingTop: 15, margintop: 3 }}>{this.state.precoExtra} €</Text>
                                                </Col>
                                                <Col>
                                                    <Text>P/Total</Text>
                                                    <Text style={{ height: 40, textAlign: 'left', paddingTop: 15, margintop: 3 }}>{this.state.totalMultiplicacao.toFixed(2)} €</Text>
                                                </Col>
                                            </Grid>
                                        </View>
                                        <View style={{ marginHorizontal: 5 }}>
                                            <Text>Observações:</Text>
                                            <Textarea
                                                containerStyle={styles.textareaContainer}
                                                maxLength={120}
                                                placeholderTextColor={'#535557'}
                                                underlineColorAndroid={'transparent'}
                                                borderRadius={6}
                                                value={this.state.observacoes}
                                                onChangeText={observacoes => this.setState({ observacoes })}
                                            />
                                        </View>
                                    </View>
                                </View>
                                : null
                        }
                        <View
                            style={{ height: 1, width: '100%', backgroundColor: '#222', marginBottom: 10 }}
                        />
                    </View>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={this.props.onCancel} style={styles.btnSair}>
                            <Text style={styles.text}> SAIR </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.inserirExtra} style={styles.btnEntrar}>
                            <Text style={styles.text}> CRIAR EXTRA </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                {_maybeRenderModalTiposExtras()}
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {

        backgroundColor: '#F2F2F2',
        justifyContent: 'space-between',
        width: '100%',
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    input: {
        width: '100%',
        height: 40,
        marginBottom: 10,
        marginTop: 3,
        backgroundColor: '#F2F2F2',
        borderWidth: 1,
        borderColor: '#222',
        borderRadius: 6,
    },
    inputQtde: {
        width: '80%',
        height: 40,
        marginBottom: 10,
        marginTop: 3,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 6,
        textAlign: 'right',
        paddingRight: 10,
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    btnEntrar: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'rgba(255, 255, 255, 255)',
        fontSize: 14,
        textAlign: 'center',
    },
    // extraContainer: {
    //     backgroundColor: '#e0e0e0'
    // },
    textareaContainer: {
        height: 45,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 6,
        marginBottom: 10
    },
    toolbar: {
        width: '100%',
        backgroundColor: '#f1f1f1',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    toolbarRight: {
        alignSelf: 'flex-end',
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.65)',
    },
})

export default withNavigation(Extras)