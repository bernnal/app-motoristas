import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Modal } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../resources/fonts/selection.json'
import { withNavigation } from 'react-navigation'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Cliente extends Component {

    constructor(props) {
        super(props)
        this.state = {
            textSize: 30
        }
    }

    increaseSize = () => {
        this.setState({
            textSize: this.state.textSize + 10
        })
    }

    decreaseSize = () => {
        this.setState({
            textSize: this.state.textSize - 10
        })
    }

    render() {
        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={true}>
                <View style={{ height: '100%' }}>
                    <View style={{ backgroundColor: '#FBB040' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 15, marginTop: 20 }}>
                            <TouchableOpacity onPress={this.decreaseSize}>
                                <Icon name="minus-circle" size={30} color="#1A1A1A" />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.increaseSize}>
                                <Icon name="plus-circle" size={30} color="#1A1A1A" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.container}>
                        <Text style={[styles.text, { fontSize: this.state.textSize, textTransform: 'uppercase' }]}>{this.props.cliente}</Text>
                    </View>
                    <View style={styles.modalFooter}>
                        {
                            this.props.app_footer == 3 && this.props.numero_voo === undefined ?
                                <View style={{ flexDirection: 'row' }}>
                                    <View>
                                        <MyIcon style={{ color: '#FFF', }} name="fonte-tg-53" size={30} />
                                    </View>
                                    <View>
                                        <Text style={{ color: '#FFF', marginTop: 5, paddingLeft: 5, }}>{this.props.morada_destino}</Text>
                                    </View>
                                </View>
                                :
                                this.props.app_footer == 3 ?
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View>
                                                <MyIcon style={{ color: '#FFF', }} name="fonte-tg-17" size={30} />
                                            </View>
                                            <View>
                                                <Text style={{ color: '#FFF', marginTop: 5, paddingLeft: 5, }}>{this.props.numero_voo}</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View>
                                                <MyIcon style={{ color: '#FFF', }} name="fonte-tg-53" size={30} />
                                            </View>
                                            <View>
                                                <Text style={{ color: '#FFF', marginTop: 5, paddingLeft: 5, }}>{this.props.morada_destino}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    :
                                    this.props.app_footer == 2 ?
                                        <View style={{ flexDirection: 'row' }}>
                                            <View>
                                                <MyIcon style={{ color: '#FFF', }} name="fonte-tg-53" size={30} />
                                            </View>
                                            <View>
                                                <Text style={{ color: '#FFF', marginTop: 5, paddingLeft: 5, }}>{this.props.morada_destino}</Text>
                                            </View>
                                        </View>
                                        :
                                        this.props.app_footer == 1 ?
                                            <View style={{ flexDirection: 'row' }}>
                                                <View>
                                                    <MyIcon style={{ color: '#FFF', }} name="fonte-tg-17" size={30} />
                                                </View>
                                                <View>
                                                    <Text style={{ color: '#FFF', marginTop: 5, paddingLeft: 5, }}>{this.props.numero_voo}</Text>
                                                </View>
                                            </View>
                                            : null
                        }
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.props.onCancel}>
                                <Icon name="times-circle" size={30} color="#FFF" />
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 8,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FBB040',
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold'
    },
    modalFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 5,
        paddingHorizontal: 10,
        width: '100%',
        height: '10%',
        backgroundColor: '#1A1A1A',
    }
})

export default withNavigation(Cliente);