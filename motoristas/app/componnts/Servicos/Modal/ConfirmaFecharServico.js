import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Modal,
    StyleSheet,
    TouchableWithoutFeedback,
    ScrollView,
    TextInput,
    FlatList,
} from 'react-native'
import { Col, Row, Grid } from "react-native-easy-grid"
import { Container, Content, Picker, Form, } from "native-base";
import Textarea from 'react-native-textarea'
import { withNavigation } from 'react-navigation'

class ConfirmaFecharServico extends Component {

    constructor(props) {
        super(props)
        this.state = {
        }
    }

    onValueChange(value: string) {
        this.setState({
        });
    }

    componentDidMount() {

    }

    render() {
        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={true}>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <Text>
                        Gravação Efectuada.
                    </Text>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Trocar de Motorista')} style={styles.btnSair}>
                            <Text style={styles.textSair}> SAIR </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Login')} style={styles.btnSair}>
                            <Text style={styles.textSair}> OK </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#F2F2F2',
        justifyContent: 'space-between',
        width: '100%',
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    input: {
        width: '100%',
        height: 40,
        marginBottom: 10,
        marginTop: 3,
        backgroundColor: '#F2F2F2',
        borderWidth: 1,
        borderColor: '#222',
        borderRadius: 6,
    },
    inputQtde: {
        width: '80%',
        height: 40,
        marginBottom: 10,
        marginTop: 3,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 6,
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    btnEntrar: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'rgba(255, 255, 255, 255)',
        fontSize: 14,
        textAlign: 'center',
    },
    // extraContainer: {
    //     backgroundColor: '#e0e0e0'
    // },
    textareaContainer: {
        height: 45,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 6
    },
})

export default withNavigation(ConfirmaFecharServico);