import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    Modal,
    StyleSheet,
    TouchableWithoutFeedback,
    TextInput,
    FlatList,
} from 'react-native'
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button'
import Textarea from 'react-native-textarea'
import { API_ATUALIZA_ESTADO_SERVICO } from '../../../config'
import axios from 'axios'
import { withNavigation } from 'react-navigation'

var radio_props = [
    { label: 'Acidente', value: 0 },
    { label: 'Avaria', value: 1 },
    { label: 'Fora de Serviço', value: 2 },
    { label: 'No-show', value: 3 },
    { label: 'Trânsito', value: 4 },
]

class Rejeitar extends Component {

    constructor(props) {
        super(props)
        this.state = {
            observacoes: '',
        }
    }

    initialState = () => {
        return {
            value: 0,
        }
    }

    rejeitar = () => {
        axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
            'param1': this.props.api_token,
            'param2': this.props.idR,
            'param3': 6,
            'param4': 'TZ',
            'param5': this.state.value,
            'param6': this.state.observacoes,
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((jsonResponse) => {
                if (jsonResponse.data.error) {
                    console.log('Rejeitado.');
                    // this.props.navigation.navigate('Dashboard')
                } else {
                    console.log('Não Rejeitado.');
                }
            })
            .catch((error) => {
                console.log("axios error:", error);
            })
    }

    render() {

        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={true}>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.dashboardHeader}>
                        <Text style={styles.header}> Rejeitar Serviço </Text>
                        <Text style={styles.header}> {this.props.codigoServicoR} </Text>
                    </View>
                    <View>
                        <Text style={styles.textMotivo}>
                            Se pretende realmente REJEITAR o serviço, informe o motivo da recusa:
                        </Text>
                    </View>
                    <View style={styles.radioButtton}>
                        <RadioForm
                            radio_props={radio_props}
                            initial={''}
                            buttonColor={'#222'}
                            onPress={(value) => { this.setState({ value: value }) }}
                        />
                    </View>
                    <View style={{ marginHorizontal: 5 }}>
                        <Text>Observações:</Text>
                        <Textarea
                            containerStyle={styles.textareaContainer} 
                            //style={styles.input}
                            //defaultValue={this.state.text}
                            // onChangeText={this.onChange}
                            maxLength={120}
                            placeholderTextColor={'#535557'}
                            underlineColorAndroid={'transparent'}
                            borderRadius={6}
                            value={this.state.observacoes}
                            onChangeText={observacoes => this.setState({ observacoes })}
                        />
                    </View>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={this.props.onCancel} style={styles.btnSair}>
                            <Text style={styles.text}> CANCELAR </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.rejeitar()} style={styles.btnEntrar}>
                            <Text style={styles.text}> REJEITAR </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#f6f6f6',
        justifyContent: 'space-between',
        width: '100%',
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    textMotivo: {
        color: '#000',
        fontSize: 16,
        padding: 5,
    },
    radioButtton: {
        marginLeft: 5,
        marginVertical: 20
    },
    textareaContainer: {
        height: 45,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#CCC',
        borderRadius: 6
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    btnEntrar: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#28a745',
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: 'rgba(255, 255, 255, 255)',
        fontSize: 14,
        textAlign: 'center',
    },
})

export default withNavigation(Rejeitar);