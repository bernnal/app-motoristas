import React, { Component } from 'react'
import {
    Modal,
    View,
    Text,
    ImageBackground,
    Dimensions,
    TextInput,
    Platform,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Animated,
    TouchableWithoutFeedback,
    StyleSheet,
    Button,
    Picker,
    DatePickerIOS,
    Alert
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid"
import Textarea from 'react-native-textarea'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../../resources/fonts/selection.json'
import moment from 'moment'
import axios from 'axios'
import { GET_ATRIBUIR, API_MOTORISTAS_FORMAS_PAGAMENTOS, API_OPERADORES, API_TIPOS_COBRANCA } from '../../../config'

const { width: WIDTH } = Dimensions.get('window')
const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class FechoServicoRetorno extends Component {

    constructor(props) {
        super(props)
        this.state = {
            // api_token: this.props.navigation.state.params.api_token,

            time: '',
            date: new Date(),
            auxDate: '',
            modalIsVisibleDate: false,
            modalIsVisibleTime: false,

            numberAdults: '1',
            numberChilds: '0',
            numberBaby: '0',
            modalIsVisibleAdults: false,
            modalIsVisibleChilds: false,
            modalIsVisibleBaby: false,

            descFormaAtribuir: '',
            descFormaAtribuirIOS: 'Atribuir a',
            modalIsVisibleAtribuir: false,

            descOperador: '',
            descOperadorIOS: 'Selecione o Operador',
            idOperador: '',
            operador: false,
            dataSourceOperadores: [],
            modalIsVisibleOperadores: false,
            transfer_zona: 'transfer_zona',

            descFormaPagamento: '',
            descFormaPagamentoIOS: 'Pagamento',
            modalIsVisibleFormaPagamento: false,

            valor: '',

            enableGerarRetorno: false,
            enableRegularizadoCobrarRetorno: false,

            modalAnimatedValue: new Animated.Value(0),
        }
        this.setDate = this.setDate.bind(this)
    }

    // componentDidMount() {
    //     axios.get(`${API_TIPOS_COBRANCA}`, {
    //         params: {
    //             'param1': this.props.api_token
    //         }
    //     }, {
    //             "headers": {
    //                 'Content-Type': 'application/json',
    //             }
    //         }).then((dataSourceAtribuir) => {
    //             this.setState({
    //                 dataSourceAtribuir: dataSourceAtribuir.data['data'],
    //             })
    //         })
    //         .catch(error => console.log(error));

    //     axios.get(`${API_MOTORISTAS_FORMAS_PAGAMENTOS}`, {
    //         // 'param1': this.props.api_token
    //     }, {
    //             "headers": {
    //                 'Content-Type': 'application/json',
    //             }
    //         }).then((dataSource) => {
    //             this.setState({
    //                 dataSource: dataSource.data['data'],
    //             })
    //             console.log('data source formas de pagamento', this.state.dataSource)
    //         })
    //         .catch(error => console.log(error));

    //     axios.get(`${API_OPERADORES}`, {
    //         params: {
    //             // 'param1': this.state.api_token,
    //             // 'param2': this.state.transfer_zona,
    //         }
    //     }, {
    //             "headers": {
    //                 'Content-Type': 'application/json',
    //             }
    //         }).then((dataSourceOperadores) => {
    //             this.setState({
    //                 // operador: true,
    //                 dataSourceOperadores: dataSourceOperadores.data['data'],
    //             })
    //                 .catch(error => console.log(error));
    //         })
    //         .catch(error => console.log(error));
    // }

    /*handlePressOpen*/
    _handlePressOpenDate = () => {
        if (this.state.modalIsVisibleDate) {
            return;
        }

        this.setState({ modalIsVisibleDate: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenTime = () => {
        if (this.state.modalIsVisibleTime) {
            return;
        }

        this.setState({ modalIsVisibleTime: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenAdults = () => {
        if (this.state.modalIsVisibleAdults) {
            return;
        }

        this.setState({ modalIsVisibleAdults: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenChilds = () => {
        if (this.state.modalIsVisibleChilds) {
            return;
        }

        this.setState({ modalIsVisibleChilds: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenBaby = () => {
        if (this.state.modalIsVisibleBaby) {
            return;
        }

        this.setState({ modalIsVisibleBaby: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenAtribuir = () => {
        if (this.state.modalIsVisibleAtribuir) {
            return;
        }

        this.setState({ modalIsVisibleAtribuir: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        })
    }

    _handlePressOpenFormaPagamento = () => {
        if (this.state.modalIsVisibleFormaPagamento) {
            return;
        }

        this.setState({ modalIsVisibleFormaPagamento: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        })
    }

    _handlePressOpenOperador = () => {
        if (this.state.modalIsVisibleOperadores) {
            return;
        }

        this.setState({ modalIsVisibleOperadores: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    /*handlePressDone*/
    _handlePressDoneDate = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleDate: false });
        });
    };

    _handlePressDoneTime = () => {

        axios.get(`${API_TIPOS_COBRANCA}`, {
            params: {
                'param1': this.props.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceAtribuir) => {
                this.setState({
                    dataSourceAtribuir: dataSourceAtribuir.data['data'],
                })
            })
            .catch(error => console.log(error));

        axios.get(`${API_MOTORISTAS_FORMAS_PAGAMENTOS}`, {
            params: {
                'param1': this.props.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSource) => {
                console.log('formas de pagamento: ', dataSource)
                this.setState({
                    dataSource: dataSource.data['data'],
                })
                console.log('data source formas de pagamento', this.state.dataSource)
            })
            .catch(error => console.log(error));

        axios.get(`${API_OPERADORES}`, {
            params: {
                'param1': this.props.api_token,
                'param2': this.state.transfer_zona,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceOperadores) => {
                this.setState({
                    // operador: true,
                    dataSourceOperadores: dataSourceOperadores.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));



        this.state.auxDate = this.state.date
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleTime: false });
        });
    };

    _handlePressDoneAdults = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleAdults: false });
        });
    };

    _handlePressDoneChilds = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleChilds: false });
        });
    };

    _handlePressDoneBaby = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleBaby: false });
        });
    };

    _handlePressDoneAtribuir = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({
                modalIsVisibleAtribuir: false
            })
        })
    }

    _handlePressDoneFormaPagamento = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleFormaPagamento: false })
        })
    }

    _handlePressDoneOperador = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleOperadores: false });
        });
    };

    /*onValueChange*/
    onValueChangeAdults(label: string) {
        this.setState({
            numberAdults: label
        })
        console.log('numberAdults', this.state.numberAdults)
    }

    onValueChangeChild(value: string) {
        this.setState({
            numberChilds: value
        })
    }

    onValueChangeBaby(value: string) {
        this.setState({
            numberBaby: value
        })
    }

    onValueChangeAtribuir(value: string, index: string) {

        let atribuir = this.state.dataSourceAtribuir[index].nome

        value === 1 ?
            this.setState({
                enableGerarRetorno: true,
                enableRegularizadoCobrarRetorno: false,
                descFormaAtribuir: value,
                descFormaAtribuirIOS: atribuir,
            })
            :
            this.setState({
                enableGerarRetorno: false,
                enableRegularizadoCobrarRetorno: true,
                descFormaAtribuir: value,
                descFormaAtribuirIOS: atribuir,
            })
    }

    onValueChange(value: string, index: string) {

        let formaPagamento = this.state.dataSource[index].nome

        this.setState({
            descFormaPagamento: value,
            descFormaPagamentoIOS: formaPagamento,
        })
    }

    onValueChangeOperadores(value: string, index: string) {
        let idOperador = this.state.dataSourceOperadores[index].id
        let operadorSelecionado = this.state.dataSourceOperadores[index].nome

        this.setState({
            descOperador: value,
            idOperador: idOperador,
            descOperadorIOS: operadorSelecionado,
            categoria: true,
        });
        console.log('idOperador', this.state.idOperador)
    }

    setDate(newDate) {
        this.setState({ date: newDate })
    }

    render() {

        let list_atribuir = this.state.dataSourceAtribuir;
        if (typeof (list_atribuir) !== undefined) {
            list_atribuir = [list_atribuir][0];
        }

        let list_pagamento = this.state.dataSource;
        if (typeof (list_pagamento) !== undefined) {
            list_pagamento = [list_pagamento][0];
        }

        let list_operadores = this.state.dataSourceOperadores;
        if (typeof (list_operadores) !== undefined) {
            list_operadores = [list_operadores][0];
        }

        let datePicker = null
        let modalPickerNumberPassenger = null
        let pickerAtribuir = null
        let pickerFormasPagamento = null
        let modalPickerOperadores = null
        if (Platform.OS === 'ios') {
            datePicker = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        width: '45%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-48" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenDate}>
                            <View style={{ height: 45, flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 10,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>
                                    {moment(this.state.date).format('DD-MM-YYYY')}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '45%',
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-16" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenTime}>
                            <View style={{ height: 45, flexDirection: 'column' }}>
                                <Text style={{
                                    placeholder: 'Selecionar Hora',
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>
                                    {moment(this.state.date).format('LT')}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
            modalPickerNumberPassenger = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-24" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenAdults}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberAdults}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-25" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenChilds}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberChilds}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-26" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenBaby}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberBaby}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
            pickerAtribuir = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Grid>
                        <Col>
                            <Row>
                                <View style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: '#FFF',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    height: 45,
                                    borderRadius: 5,
                                }}>
                                    <TouchableWithoutFeedback onPress={this._handlePressOpenAtribuir}>
                                        <View>
                                            <Text style={{
                                                justifyContent: 'center',
                                                backgroundColor: '#fff',
                                                paddingLeft: 10,
                                            }}>{this.state.descFormaAtribuirIOS}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </Row>
                        </Col>
                    </Grid>
                </View>
            )
            pickerFormasPagamento = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Grid>
                        <Col>
                            <Row>
                                <View style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: '#FFF',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    height: 45,
                                    borderRadius: 5,
                                }}>
                                    <TouchableWithoutFeedback onPress={this._handlePressOpenFormaPagamento}>
                                        <View>
                                            <Text style={{
                                                justifyContent: 'center',
                                                backgroundColor: '#fff',
                                                paddingLeft: 10,
                                            }}>{this.state.descFormaPagamentoIOS}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </Row>
                        </Col>
                    </Grid>
                </View>
            )
            modalPickerOperadores = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                    <Grid>
                        <Col>
                            <Row>
                                <View style={{
                                    width: '100%',
                                    flexDirection: 'row',
                                    justifyContent: 'flex-start',
                                    alignItems: 'center',
                                    backgroundColor: '#FFF',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    height: 45,
                                    borderRadius: 5,
                                }}>
                                    <TouchableWithoutFeedback onPress={this._handlePressOpenOperador}>
                                        <View>
                                            <Text style={{
                                                justifyContent: 'center',
                                                backgroundColor: '#fff',
                                                paddingLeft: 10,
                                            }}>{this.state.descOperadorIOS}</Text>
                                        </View>
                                    </TouchableWithoutFeedback>
                                </View>
                            </Row>
                        </Col>
                    </Grid>
                </View>
            )
        } else {
            datePicker = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        width: '45%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-48" size={25} />
                        </View>
                        <DatePicker
                            style={{
                                width: '72%',
                                height: 45,
                                fontSize: 16,
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: 'rgba(0, 0, 0, 0.6)',
                            }}
                            date={this.state.date}
                            onDateChange={(date) => { this.setState({ date: date }) }}
                            mode="date"
                            format="YYYY-MM-DD"
                            minDate={new Date()}
                            maxDate="2099-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    paddingLeft: 5,
                                    alignItems: 'flex-start',
                                    width: 10,
                                },
                                placeholderText: {
                                    fontSize: 16,
                                    backgroundColor: '#fff',
                                    color: 'rgba(0, 0, 0, 0.6)',
                                }
                            }}
                            showIcon={false}
                        ></DatePicker>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '45%',
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-16" size={25} />
                        </View>
                        <DatePicker
                            style={{
                                width: '72%',
                                height: 45,
                                fontSize: 12,
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: 'rgba(0, 0, 0, 0.6)',
                            }}
                            placeholder="Selecionar Hora"
                            date={this.state.time}
                            onDateChange={(time) => { this.setState({ time: time }) }}
                            mode="time"
                            format="HH:MM"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    paddingLeft: 5,
                                    alignItems: 'flex-start',
                                },
                                placeholderText: {
                                    fontSize: 12,
                                    backgroundColor: '#fff',
                                    color: 'rgba(0, 0, 0, 0.6)',
                                }
                            }}
                            showIcon={false}
                        ></DatePicker>
                    </View>
                </View>
            )
            modalPickerNumberPassenger = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-24" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                selectedValue={this.state.numberAdults}
                                onValueChange={this.onValueChangeAdults.bind(this)}
                            >
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                                <Picker.Item label="11" value="11" />
                                <Picker.Item label="12" value="12" />
                                <Picker.Item label="13" value="13" />
                                <Picker.Item label="14" value="14" />
                                <Picker.Item label="15" value="15" />
                                <Picker.Item label="16" value="16" />
                                <Picker.Item label="17" value="17" />
                                <Picker.Item label="18" value="18" />
                                <Picker.Item label="19" value="19" />
                                <Picker.Item label="20" value="20" />
                                <Picker.Item label="21" value="21" />
                                <Picker.Item label="22" value="22" />
                                <Picker.Item label="23" value="23" />
                                <Picker.Item label="24" value="24" />
                                <Picker.Item label="25" value="25" />
                                <Picker.Item label="26" value="26" />
                                <Picker.Item label="27" value="27" />
                                <Picker.Item label="28" value="28" />
                                <Picker.Item label="29" value="29" />
                                <Picker.Item label="30" value="30" />
                            </Picker>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-25" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={this.state.numberChilds}
                                onValueChange={this.onValueChangeChild.bind(this)}
                            >
                                <Picker.Item label="0" value="0" />
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                            </Picker>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-26" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={this.state.numberBaby}
                                onValueChange={this.onValueChangeBaby.bind(this)}
                            >
                                <Picker.Item label="0" value="0" />
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                            </Picker>
                        </View>
                    </View>
                </View>
            )
            pickerAtribuir = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{
                        height: 45,
                        flexDirection: 'column',
                        width: '100%',
                        paddingRight: 5
                    }}>
                        <Picker
                            placeholderText='Atribuir'
                            mode='dropdown'
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descFormaAtribuir}
                            itemStyle={{
                                fontSize: 14,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeAtribuir.bind(this)}>
                            {list_atribuir.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </View>
                </View>
            )
            pickerFormasPagamento = (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>>
                        <View style={{
                        height: 45,
                        flexDirection: 'column',
                        width: '68%',
                        paddingRight: 5
                    }}>
                        <Picker
                            placeholderText='Forma Pagamento'
                            mode='dropdown'
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descFormaPagamento}
                            itemStyle={{
                                fontSize: 14,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChange.bind(this)}>
                            {list_pagamento.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </View>
                </View>
            )
            modalPickerOperadores = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-43" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            placeholder="Selecione o Operador"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descOperador}
                            onValueChange={this.onValueChangeOperadores.bind(this)}
                        >
                            {
                                list_operadores.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
        }

        /*maybeRenderModal*/
        _maybeRenderModalDate = () => {
            if (!this.state.modalIsVisibleDate) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleDate ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneDate} />
                            </View>
                        </View>
                        <DatePickerIOS
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            mode="date"
                            date={this.state.date}
                            onDateChange={this.setDate}
                            timeZoneOffsetInMinutes={-1}
                            locale={'pt'}
                        >
                        </DatePickerIOS>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalTime = () => {
            if (!this.state.modalIsVisibleTime) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleTime ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneTime} />
                            </View>
                        </View>
                        <DatePickerIOS
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            mode="time"
                            date={this.state.date}
                            onDateChange={this.setDate}
                        >
                        </DatePickerIOS>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalAdults = () => {
            if (!this.state.modalIsVisibleAdults) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleAdults ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneAdults} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberAdults}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeAdults.bind(this)}>
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                            <Picker.Item label="11" value="11" />
                            <Picker.Item label="12" value="12" />
                            <Picker.Item label="13" value="13" />
                            <Picker.Item label="14" value="14" />
                            <Picker.Item label="15" value="15" />
                            <Picker.Item label="16" value="16" />
                            <Picker.Item label="17" value="17" />
                            <Picker.Item label="18" value="18" />
                            <Picker.Item label="19" value="19" />
                            <Picker.Item label="20" value="20" />
                            <Picker.Item label="21" value="21" />
                            <Picker.Item label="22" value="22" />
                            <Picker.Item label="23" value="23" />
                            <Picker.Item label="24" value="24" />
                            <Picker.Item label="25" value="25" />
                            <Picker.Item label="26" value="26" />
                            <Picker.Item label="27" value="27" />
                            <Picker.Item label="28" value="28" />
                            <Picker.Item label="29" value="29" />
                            <Picker.Item label="30" value="30" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalChild = () => {
            if (!this.state.modalIsVisibleChilds) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleChilds ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneChilds} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberChilds}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeChild.bind(this)}>
                            <Picker.Item label="0" value="0" />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalBaby = () => {
            if (!this.state.modalIsVisibleBaby) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleBaby ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneBaby} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberBaby}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeBaby.bind(this)}>
                            <Picker.Item label="0" value="0" />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalAtribuir = () => {
            if (!this.state.modalIsVisibleAtribuir) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            })

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleAtribuir ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneAtribuir} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descFormaAtribuir}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeAtribuir.bind(this)}>
                            {list_atribuir.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalFormaPagamento = () => {
            if (!this.state.modalIsVisibleFormaPagamento) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            })

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleFormaPagamento ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneFormaPagamento} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descFormaPagamento}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChange.bind(this)}>
                            {list_pagamento.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalOperadores = () => {
            if (!this.state.modalIsVisibleOperadores) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleOperadores ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneOperador} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descOperador}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeOperadores.bind(this)}>
                            {list_operadores.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        return (
            <Modal
                onRequestClose={this.props.onCancel}
                visible={this.props.isVisible}
                animationType='slide'
                transparent={true}>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                <View style={styles.container}>
                    <View style={styles.dashboardHeader}>
                        <Text style={styles.header}> Criar Retorno </Text>
                        <Text style={styles.header}> {this.props.codigoServicoF} </Text>
                    </View>
                    <ScrollView>


                        <View
                            style={{ height: 1, width: '100%', backgroundColor: '#E6AC4A' }}
                        />
                        <View>
                            <Text style={{ padding: 5, fontWeight: 'bold' }}>Dados do Retorno</Text>
                        </View>
                        <View
                            style={{ height: 1, width: '100%', backgroundColor: '#E6AC4A' }}
                        />

                        {datePicker}

                        {modalPickerNumberPassenger}

                        <View
                            style={{ height: 1, width: '100%', backgroundColor: '#E6AC4A', marginTop: 5 }}
                        />
                        <View>
                            <Text style={{ padding: 5, fontWeight: 'bold', }}>Formas de Pagamento</Text>
                        </View>
                        <View
                            style={{ height: 1, width: '100%', backgroundColor: '#E6AC4A' }}
                        />
                        <View>
                            <Text style={{ paddingTop: 5, paddingLeft: 5 }}>Valor a Cobrar:</Text>
                            <View style={styles.iconContainer}>
                                <MyIcon style={styles.iconStyle} name="fonte-tg-47" />
                                <TextInput
                                    style={{ flex: 1, paddingHorizontal: 5 }}
                                    placeholder="0.0"
                                    placeholderTextColor={'#000'}
                                    underlineColorAndroid="transparent"
                                    placeholderTextColor='#222'
                                    textAlign="right"
                                    keyboardType='numeric'
                                    value={this.state.valor}
                                    onChangeText={valor => this.setState({ valor })}
                                />
                            </View>
                        </View>
                        <View style={{ paddingLeft: 5 }}>
                            <Text style={{ paddingTop: 5 }}>Cobrança:</Text>
                            <View style={{
                                height: 45,
                                paddingRight: 5
                            }}>
                                {pickerAtribuir}
                            </View>
                        </View>
                        <View style={{ paddingLeft: 5 }}>
                            <Text style={{ paddingTop: 5, paddingLeft: 5 }}>Operador:</Text>
                            <View style={{
                                height: 45,
                                paddingRight: 5
                            }}>
                                {modalPickerOperadores}
                            </View>
                        </View>
                        <View style={{ paddingLeft: 5 }}>
                            <Text style={{ paddingTop: 5 }}>Forma de Pagamento:</Text>
                            <View style={{
                                height: 45,
                                paddingRight: 5
                            }}>
                                {pickerFormasPagamento}
                            </View>
                        </View>
                        <View style={{ paddingVertical: 20, }}>
                            <Text style={{ paddingVertical: 5, paddingLeft: 5 }}>Observações:</Text>
                            <View style={styles.obsContainer}>
                                <MyIcon style={styles.iconObs} name="fonte-tg-60" size={20} />
                                <Textarea
                                    onChangeText={this.onChange}
                                    underlineColorAndroid={'transparent'}
                                    borderRadius={6}
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5, paddingVertical: 5 }}>
                            <TouchableOpacity onPress={this.props.onCancel}>
                                <View style={styles.buttonQuestionario}>
                                    <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>CANCELAR</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity disabled={this.state.enableGerarRetorno}>
                                <View style={[styles.buttonFecharServico, { backgroundColor: this.state.enableGerarRetorno ? '#CCC' : '#7BB758' }]}>
                                    <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>CRIAR RETORNO</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 5, paddingVertical: 5, marginBottom: 30 }}>
                            <TouchableOpacity disabled={this.state.enableRegularizadoCobrarRetorno}>
                                <View style={[styles.buttonCriarRetorno, { backgroundColor: this.state.enableRegularizadoCobrarRetorno ? '#CCC' : '#269940' }]}>
                                    <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>REGULARIZADO</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity disabled={this.state.enableRegularizadoCobrarRetorno}>
                                <View style={[styles.buttonCancelar, { backgroundColor: this.state.enableRegularizadoCobrarRetorno ? '#CCC' : '#3C80BC' }]}>
                                    <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: '#FFF', textAlign: 'center' }}>COBRAR NO RETORNO</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
                <TouchableWithoutFeedback onPress={this.props.onCancel}>
                    <View style={styles.offset}></View>
                </TouchableWithoutFeedback>
                {_maybeRenderModalDate()}
                {_maybeRenderModalTime()}
                {_maybeRenderModalAdults()}
                {_maybeRenderModalChild()}
                {_maybeRenderModalBaby()}
                {_maybeRenderModalAtribuir()}
                {_maybeRenderModalFormaPagamento()}
                {_maybeRenderModalOperadores()}
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    offset: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    container: {
        backgroundColor: '#F2F2F2',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 20,
    },
    header: {
        fontSize: 15,
        padding: 5,
        color: 'white',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    spaceInputs: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 5,
        marginHorizontal: 5,
    },
    data: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 45,
        height: 45,
        backgroundColor: '#535557',
    },
    icon: {
        color: '#fff',
    },
    iconContainer: {
        width: '97%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        marginHorizontal: 5,
    },
    iconStyle: {
        padding: 5,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    iconObs: {
        paddingLeft: 15,
        marginLeft: 15,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    obsContainer: {
        width: '97.5%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        marginLeft: 5
    },
    toolbar: {
        width: '100%',
        backgroundColor: '#f1f1f1',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    toolbarRight: {
        alignSelf: 'flex-end',
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.65)',
    },
    buttonQuestionario: {
        backgroundColor: '#00AAC6',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonCancelar: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonFecharServico: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
    buttonCriarRetorno: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
        borderRadius: 5,
        width: 165,
        height: 60,
        marginTop: 5
    },
})

export default FechoServicoRetorno