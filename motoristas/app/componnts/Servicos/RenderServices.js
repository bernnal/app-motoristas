import React from 'react';
import { Text, View } from 'react-native';
import {
  createMaterialTopTabNavigator,
  createAppContainer,
} from 'react-navigation';
import YesterdayServices from './YesterdayServices'
import TodayServices from './TodayServices'
import TomorrowServices from './TomorrowServices'

const RenderServices = createMaterialTopTabNavigator(
  {
    'ONTEM': { screen: YesterdayServices },
    'HOJE': { screen: TodayServices },
    'AMANHÃ': { screen: TomorrowServices },
  },
  {
    initialRouteName: 'HOJE',
    style: {
        paddingTop: 30,
    }
  }
);

export default RenderServices;