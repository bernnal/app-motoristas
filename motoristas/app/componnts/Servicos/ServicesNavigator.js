import React, { Component } from 'react';
import { Modal } from 'react-native'
//import { createBottomTabNavigator, createMaterialTopTabNavigator, createStackNavigator, createAppContainer } from 'react-navigation'
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import YesterdayServices from './YesterdayServices';
import TodayServices from './TodayServices';
import TomorrowServices from './TomorrowServices';

export default class ServicesNavigator extends Component {

    constructor(props) {
        super(props)
        this.state = {
            
        }
    }


    render() {
        return (
            <Modal>
                <Container>
                    <Tab heading="ONTEM">
                        <YesterdayServices />
                    </Tab>
                    <Tab heading="HOJE">
                        <TodayServices />
                    </Tab>
                    <Tab heading="AMANHÃ">
                        <TomorrowServices />
                    </Tab>
                </Container>
            </Modal>
        );
    }
}