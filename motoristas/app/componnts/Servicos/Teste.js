import * as React from 'react';
import {
    Text,
    View,
    ImageBackground,
    Image,
    TextInput,
    Dimensions,
    TouchableOpacity,
    Alert,
    Button,
    Picker,
    Animated,
    Platform,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogFooter,
    DialogButton,
    SlideAnimation,
    ScaleAnimation,
} from 'react-native-popup-dialog';
import axios from 'axios';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { fetchPosts } from '../../store/actions'
import { Col, Row, Grid } from "react-native-easy-grid"
import { API_MOTORISTAS_FORMAS_PAGAMENTOS, GET_ATRIBUIR } from '../../config'

const { width: WIDTH } = Dimensions.get('window')

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false,
        };
    }

    goTo = () => {
        // this.setState({ showPopup: false })
        this.state.showPopup = false
        this.state.showPopup == false ?
            this.props.navigation.navigate('Trocar de Motorista')
            :
            console.log('ahdsufhaudhfuahfdua: ', this.state.showPopup)
    }

    render() {

        console.log('showpopuo', this.state.showPopup)


        return (
            <View style={styles.container}>

                <TouchableOpacity style={{
                    height: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: '#CCC'
                }}
                    onPress={() => this.setState({ showPopup: true, })}
                >
                    <View>
                        <Text>
                            Show Dialog - Default Animation
                            </Text>
                    </View>
                </TouchableOpacity>

                <Dialog
                    onDismiss={() => {
                        this.setState({ showPopup: false });
                    }}
                    width={0.9}
                    visible={this.state.showPopup}
                    rounded
                    actionsBordered
                    dialogTitle={
                        <DialogTitle
                            title="Popup Dialog - Default Animation"
                            style={{
                                backgroundColor: '#F7F7F8',
                            }}
                            hasTitleBar={false}
                            align="left"
                        />
                    }
                    footer={
                        <DialogFooter>
                            <DialogButton
                                text="CANCEL"
                                bordered
                                onPress={() => {
                                    this.setState({ showPopup: false });
                                }}
                                key="button-1"
                            />
                            <DialogButton
                                text="OK"
                                bordered
                                onPress={this.goTo}
                                key="button-2"
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#F7F7F8',
                        }}
                    >
                        <Text>Default Animation</Text>
                        <Text>No onTouchOutside handler. will not dismiss when touch overlay.</Text>
                    </DialogContent>
                </Dialog>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        padding: 8,
    },
    iconContainer: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#CCC',
        height: 45,
        borderRadius: 5,
        marginLeft: 5
    },
    toolbar: {
        width: '100%',
        backgroundColor: '#f1f1f1',
        paddingVertical: 5,
        paddingHorizontal: 15,
    },
    toolbarRight: {
        alignSelf: 'flex-end',
    },
    overlay: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: 'rgba(0,0,0,0.65)',
    },
});

export default App