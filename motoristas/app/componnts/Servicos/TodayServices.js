import React, { Component } from 'react'
import {
    View,
    Text,
    Platform,
    UIManager,
    TouchableOpacity,
    ScrollView,
    LayoutAnimation,
    Alert,
    FlatList,
    Linking,
    Dimensions,
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../resources/fonts/selection.json'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid"
import axios from 'axios'
import moment from 'moment'
import styles from './styles'
import Extras from './Modal/Extras'
import Rejeitar from './Modal/Rejeitar'
import Cliente from './Modal/Cliente'
import FechoServico from './Modal/FechoServico'
import {
    API_MOTORISTAS_SERVICOS,
    API_MOTORISTAS_BAGAGENS,
    API_ATUALIZA_ESTADO_SERVICO,
    API_MOTORISTAS_EXTRAS_TRANSFERZONA
} from '../../config'
import { withNavigation } from 'react-navigation'
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = 'AIzaSyDRMWdQLj7bze-4tPSpkEoUTyfz7JRR4WE';

class Map extends Component {
    constructor(props) {
        super(props);

        this.state = {
            coordinates: [
                {
                    latitude: 37.3317876,
                    longitude: -122.0054812,
                },
                {
                    latitude: 37.771707,
                    longitude: -122.4053769,
                },
            ],
        };

        this.mapView = null;
    }

    onMapPress = (e) => {
        this.setState({
            coordinates: [
                ...this.state.coordinates,
                e.nativeEvent.coordinate,
            ],
        });
    }

    render() {
        console.log('funcao foi chamada')
        return (
            <MapView
                initialRegion={{
                    latitude: LATITUDE,
                    longitude: LONGITUDE,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                }}
                style={StyleSheet.absoluteFill}
                ref={c => this.mapView = c}
                onPress={this.onMapPress}
            >
                {this.state.coordinates.map((coordinate, index) =>
                    <MapView.Marker key={`coordinate_${index}`} coordinate={coordinate} />
                )}
                {(this.state.coordinates.length >= 2) && (
                    <MapViewDirections
                        origin={this.state.coordinates[0]}
                        waypoints={(this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1) : null}
                        destination={this.state.coordinates[this.state.coordinates.length - 1]}
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={3}
                        strokeColor="hotpink"
                        optimizeWaypoints={true}
                        onStart={(params) => {
                            console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                        }}
                        onReady={result => {
                            console.log('Distance: ${result.distance} km', result.distance)
                            console.log('Duration: ${result.duration} min.', result.duration)

                            this.mapView.fitToCoordinates(result.coordinates, {
                                edgePadding: {
                                    right: (width / 20),
                                    bottom: (height / 20),
                                    left: (width / 20),
                                    top: (height / 20),
                                }
                            });
                        }}
                        onError={(errorMessage) => {
                            // console.log('GOT AN ERROR');
                        }}
                    />
                )}
            </MapView>
        );
    }
}

// COLOCAR AQUI O MODAL QUANDO CLICA NO ICONE DE 'CHEGADA' OU 'PARTIDA'

class TodayServices extends Component {

    constructor(props) {
        super(props);
        this.state = {
            api_token: this.props.navigation.state.params.api_token,
            app_footer: this.props.navigation.state.params.app_footer,
            date: new Date(),
            auxDateD: moment(this.date).format('DD/MM/YYYY'),
            auxDate: moment(this.date).format('HH:mm'),
            cliente: '',
            moradaDestino: '',
            numeroVoo: '',

            hasService: false,
            hasBagage: false,
            copyHasBagage: false,
            sizeBagage: 0,
            copydataSourceBagagens: [],
            estado: 'Aceitar',
            codigoServico: '',
            idServico: '',

            idSelecionado: 0,
            idR: '',
            estadoR: '',
            codigoServicoR: '',

            idF: '',
            codigoServicoF: '',
            valorServicoF: '',
            extrasServicoF: '',
            valorRetornoF: '',
            totalCobrarF: '',
            emailF: '',
            clienteIdF: '',

            codigoServicoE: '',
            sizeExtras: 0,
            idE: '',

            // motoristasServicos: false,
            // dataSource: [],
            // codigoServico: '',
            // horaServico: '',
            // tempoViagem: '',

            expanded: false,
            alterarEstado: false,
            showModalExtras: false,
            showModalRejeitar: false,
            showModalCliente: false,
            showModalFecharServico: false,

        }

        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }

    mudarEstados = (
        codigoServico,
        id,
        estado,
        valorServico,
        extrasServico,
        valorRetorno,
        totalCobrarFFF,
        email,
        clienteId,
    ) => {

        let codeToClose = codigoServico
        let idwww = id
        let valorServicowwww = valorServico
        let extrasServicowwww = extrasServico
        let valorRetornowwww = valorRetorno
        let totalCobrarwwww = totalCobrarFFF
        let emailwwww = email
        let clienteIdwww = clienteId

        console.log('totalCobrarFFF', totalCobrarFFF)

        switch (estado) {
            case 2:
                axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
                    'param1': this.state.api_token,
                    'param2': id,
                    'param3': 3,
                    'param4': 'TZ',
                    'param5': '',
                    'param6': '',
                }, {
                        "headers": {
                            'Content-Type': 'application/json',
                        }
                    }).then((jsonResponseEstado) => {
                        console.log('if 2', jsonResponseEstado)
                        if (jsonResponseEstado.data.error) {
                            console.log((jsonResponseEstado.data.error));
                            this.componentDidMount();
                        } else {
                            console.log('Criado com Sucesso.');
                        }
                    })
                    .catch((error) => {
                        console.log("axios error:", error);
                    })

                break;

            case 3:
                axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
                    'param1': this.state.api_token,
                    'param2': id,
                    'param3': 9,
                    'param4': 'TZ',
                    'param5': '',
                    'param6': '',
                }, {
                        "headers": {
                            'Content-Type': 'application/json',
                        }
                    }).then((jsonResponseEstado) => {
                        console.log('if 3', jsonResponseEstado)
                        if (jsonResponseEstado.data.error) {
                            console.log((jsonResponseEstado.data.error));
                            this.componentDidMount();
                        } else {
                            console.log('Criado com Sucesso.');
                        }
                    })
                    .catch((error) => {
                        console.log("axios error:", error);
                    })

                break;

            case 9:
                axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
                    'param1': this.state.api_token,
                    'param2': id,
                    'param3': 4,
                    'param4': 'TZ',
                    'param5': '',
                    'param6': '',
                }, {
                        "headers": {
                            'Content-Type': 'application/json',
                        }
                    }).then((jsonResponseEstado) => {
                        console.log('if 9', jsonResponseEstado)
                        if (jsonResponseEstado.data.error) {
                            console.log((jsonResponseEstado.data.error));
                            this.componentDidMount();
                        } else {
                            console.log('Criado com Sucesso.');
                        }
                    })
                    .catch((error) => {
                        console.log("axios error:", error);
                    })

                break;

            case 4:
                this.setState({
                    codigoServicoF: codeToClose,
                    idF: idwww,
                    valorServicoF: valorServicowwww,
                    extrasServicoF: extrasServicowwww,
                    valorRetornoF: valorRetornowwww,
                    totalCobrarF: totalCobrarwwww,
                    emailF: emailwwww,
                    clienteIdF: clienteIdwww,
                    showModalFecharServico: true,
                })
                // axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
                //     'param1': this.state.api_token,
                //     'param2': id,
                //     'param3': 5,
                //     'param4': 'TZ',
                //     'param5': '',
                //     'param6': '',
                // }, {
                //         "headers": {
                //             'Content-Type': 'application/json',
                //         }
                //     }).then((jsonResponseEstado) => {
                //         console.log('if 4', jsonResponseEstado)
                //         if (jsonResponseEstado.data.error) {
                //             console.log((jsonResponseEstado.data.error));
                //             this.setState({
                //                 codigoServicoF: codeToClose,
                //                 idF: idwww,
                //                 valorServicoF: valorServicowwww,
                //                 extrasServicoF: extrasServicowwww,
                //                 valorRetornoF: valorRetornowwww,
                //                 totalCobrarF: totalCobrarwwww,
                //                 emailF: emailwwww,
                //                 clienteIdF: clienteIdwww,
                //                 showModalFecharServico: true,
                //             })
                //             this.componentDidMount();
                //         } else {
                //             console.log('Criado com Sucesso.');
                //         }
                //     })
                //     .catch((error) => {
                //         console.log("axios error:", error);
                //     })

                break;

            case 5:
                this.setState({
                    codigoServicoF: codeToClose,
                    idF: idwww,
                    valorServicoF: valorServicowwww,
                    extrasServicoF: extrasServicowwww,
                    valorRetornoF: valorRetornowwww,
                    totalCobrarF: totalCobrarwwww,
                    emailF: emailwwww,
                    clienteIdF: clienteIdwww,
                    // showModalFecharServico: true,
                })
                // axios.post(`${API_ATUALIZA_ESTADO_SERVICO}`, {
                //     'param1': this.state.api_token,
                //     'param2': id,
                //     'param3': 5,
                //     'param4': 'TZ',
                //     'param5': '',
                //     'param6': '',
                // }, {
                //         "headers": {
                //             'Content-Type': 'application/json',
                //         }
                //     }).then((jsonResponseEstado) => {
                //         console.log('if 4', jsonResponseEstado)
                //         if (jsonResponseEstado.data.error) {
                //             console.log((jsonResponseEstado.data.error));
                //             this.setState({
                //                 codigoServicoF: codeToClose,
                //                 idF: idwww,
                //                 valorServicoF: valorServicowwww,
                //                 extrasServicoF: extrasServicowwww,
                //                 valorRetornoF: valorRetornowwww,
                //                 totalCobrarF: totalCobrarwwww,
                //                 emailF: emailwwww,
                //                 clienteIdF: clienteIdwww,
                //                 showModalFecharServico: true,
                //             })
                //             this.componentDidMount();
                //         } else {
                //             console.log('Criado com Sucesso.');
                //         }
                //     })
                //     .catch((error) => {
                //         console.log("axios error:", error);
                //     })

                break;

            default:
                return estado
        }
    };

    onChangeLayout = (codigoServico, id) => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.state.idServico = id
        this.setState({ expanded: codigoServico });
        console.log('dentro de onchanagelayout: ', this.state.idServico)
        this.componentDidMount();
        this.getBagagens();
    }

    getBagagens = () => {
        console.log('dentro de getBagagens: ', this.state.idServico)
        axios.get(`${API_MOTORISTAS_BAGAGENS}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.idServico,
                'param3': 'TZ'
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((responseJson) => {
                let tamanhoBagagens = responseJson.data['data'].length
                // console.log('tamanho bagagensSSSSS: ', tamanhoBagagens)
                responseJson.data['data'].length >= 1 ?
                    this.setState({
                        hasBagage: true,
                        copyHasBagage: true,
                        sizeBagage: tamanhoBagagens,
                        dataSourceBagagens: responseJson.data['data'],
                    })
                    : null
                console.log('lista de Bagagens dentro do axios', this.state.dataSourceBagagens)
            })
            .catch(error => console.log(error));
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_SERVICOS}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.auxDateD,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then(responseJson => {
                const dataSource = responseJson.data['data']
                responseJson.data['data'].length >= 1 ?
                    this.setState({
                        hasService: true,
                        motoristasServicos: true,
                        dataSource,
                    })
                    : null
            })
            .catch(error => console.log(error));

        // axios.get(`${API_MOTORISTAS_EXTRAS_TRANSFERZONA}`, {
        //     params: {
        //         'param1': this.state.api_token,
        //         // 'param2': 
        //     }
        // }, {
        //         "headers": {
        //             'Content-Type': 'application/json',
        //         }
        //     }).then(responseJsonExtra => {
        //         let tamanhoExtras = responseJsonExtra.data['data'].length
        //         const dataSourceExtra = responseJsonExtra.data['data']
        //         responseJsonExtra.data['data'].length >= 1 ?
        //             this.setState({
        //                 // hasService: true,
        //                 // motoristasServicos: true,
        //                 sizeExtras: tamanhoExtras,
        //                 dataSourceExtra,
        //             })
        //             : null
        //     })
        //     .catch(error => console.log(error));
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-27" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    goToNomeCliente = (nomeCliente, moradaDestino, numeroVoo) => {
        this.setState({
            showModalCliente: true,
            cliente: nomeCliente,
            moradaDestino: moradaDestino,
            numeroVoo: numeroVoo,
        })
    }

    goToRejeitar = (id, estado, codigoServico) => {
        let idR = id
        let estadoR = estado
        let codigoServicoR = codigoServico

        this.setState({
            showModalRejeitar: true,
            idR: idR,
            estadoR: estadoR,
            codigoServicoR: codigoServico
        })

    }

    goToExtras = (codigoServico, id, totalExtras) => {
        this.setState({
            showModalExtras: true,
            codigoServicoE: codigoServico,
            idE: id,
            sizeExtras: totalExtras

            // sizeExtras: valorExtras,
        })
    }

    _pressCall = () => {
        let phoneNumber = '';

        if (Platform.OS === 'android') {
            phoneNumber = 'tel:${1234567890}';
        }
        else {
            phoneNumber = 'telprompt:${1234567890}';
        }

        Linking.openURL(phoneNumber);
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{ marginVertical: 10, }}
            />
        )
    }

    render() {

        let start = this.state.auxDate //Hora Atual
        let end = moment(this.props.data_hora).format('HH:mm'); //Hora do servico
        var diferenca = moment.utc(moment(end, 'HH:mm').diff(moment(start, 'HH:mm'))).format('HH:mm')

        this.state.copydataSourceBagagens = this.state.dataSourceBagagens
        console.log('dataSourceBagagens', this.state.dataSourceBagagens)


        let renderBagagens = null;
        let renderObservacoesBagagens = null;
        renderBagagens = (
            this.state.hasBagage ?
                <FlatList
                    data={this.state.dataSourceBagagens}
                    numColumns={this.state.sizeBagage}
                    renderItem={({ item, index }) =>
                        <Grid>
                            <Row>
                                <Row>
                                    <MyIcon style={{ color: '#535557' }} name={item.imagem.substring(5)} size={20} />
                                </Row>
                                <Row>
                                    <Text style={{ color: '#086e6c', fontSize: 14, fontWeight: 'bold' }}>{item.quantidade}</Text>
                                </Row>
                            </Row>
                        </Grid>
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
                : null
        )

        // renderObservacoesBagagens = (
        //     this.state.copyHasBagage ?
        //         <View>
        //             <Text style={styles.title}>Observações Bagagens:</Text>
        //             <FlatList
        //                 data={this.state.copydataSourceBagagens}
        //                 // numColumns={3}
        //                 renderItem={({ item, index }) =>
        //                     <Grid>
        //                         <Row>
        //                             <Text style={styles.body}>{item.observacao}, </Text>
        //                         </Row>
        //                     </Grid>
        //                 }
        //                 keyExtractor={(item, index) => index.toString()}
        //             />
        //         </View>
        //         : null
        // )

        return (
            <View style={styles.backgroundContainer}>
                <View>
                    <Extras
                        isVisible={this.state.showModalExtras}
                        api_token={this.state.api_token}
                        codigoServicoE={this.state.codigoServicoE}
                        idE={this.state.idE}
                        sizeExtras={this.state.sizeExtras}
                        onCancel={() => this.setState({ showModalExtras: false })}
                    />
                    <Rejeitar
                        isVisible={this.state.showModalRejeitar}
                        idR={this.state.idR}
                        estadoR={this.state.estadoR}
                        api_token={this.state.api_token}
                        codigoServicoR={this.state.codigoServicoR}
                        onCancel={() => this.setState({ showModalRejeitar: false })}
                    />
                    <Cliente
                        cliente={this.state.cliente}
                        app_footer={this.state.app_footer}
                        morada_destino={this.state.moradaDestino}
                        numero_voo={this.state.numero_voo}
                        isVisible={this.state.showModalCliente}
                        onCancel={() => this.setState({ showModalCliente: false })}
                    />
                    <FechoServico
                        isVisible={this.state.showModalFecharServico}
                        api_token={this.state.api_token}
                        idF={this.state.idF}
                        codigoServicoF={this.state.codigoServicoF}
                        valorServicoF={this.state.valorServicoF}
                        extrasServicoF={this.state.extrasServicoF}
                        valorRetornoF={this.state.valorRetornoF}
                        totalCobrarF={this.state.totalCobrarF}
                        emailF={this.state.emailF}
                        clienteIdF={this.state.clienteIdF}
                        onCancel={() => this.setState({ showModalFecharServico: false })}
                    />
                </View>
                <ScrollView>
                    {
                        this.state.hasService ?
                            <View>
                                <FlatList
                                    data={this.state.dataSource}
                                    ItemSeparatorComponent={this.FlatListItemSeparator}
                                    renderItem={({ item, index }) =>
                                        item.estado_id !== 5
                                            ?
                                            <View>
                                                <TouchableOpacity onPress={() => { this.onChangeLayout(item.codigo_servico, item.id) }} style={{ height: 50, justifyContent: 'center', alignItems: 'center', backgroundColor: '#1EACA8', }}>
                                                    <Grid>
                                                        <Col style={{ backgroundColor: '#1EACA8', width: '25%', height: 50, justifyContent: 'center' }}>
                                                            <View style={{
                                                                justifyContent: 'center',
                                                                alignItems: 'center',
                                                                width: 40,
                                                                height: 40,
                                                                backgroundColor: item.cor !== null ? item.cor : '#fff',
                                                                borderRadius: 20,
                                                                marginLeft: 10
                                                            }}>
                                                                <MyIcon style={{ color: '#000', alignItems: 'center', justifyContent: 'center' }} name={item.icone !== null ? item.icone.substring(5) : 'fonte-tg-30'} size={30} />
                                                            </View>
                                                        </Col>
                                                        <Col style={{ backgroundColor: '#1EACA8', height: 50, justifyContent: 'center' }}>
                                                            <Row style={{ marginTop: 5 }}>
                                                                <Text style={styles.title}>ID:</Text>
                                                                <Text style={{ color: '#fff' }}>{item.codigo_servico ? item.codigo_servico : null}</Text>
                                                            </Row>
                                                            <Row>
                                                                <Text style={styles.title}>Hora:</Text>
                                                                <Text style={{ color: '#fff' }}> {item.data_hora ? moment(item.data_hora).format('HH:mm') : null} / {item.tempo_viagem}</Text>
                                                            </Row>
                                                        </Col>
                                                        <Col style={{ backgroundColor: '#1EACA8', height: 50, justifyContent: 'center' }}>
                                                            <Row style={{ marginTop: 3 }}>
                                                                <Text style={{ color: '#fff', fontWeight: 'bold' }}>{item.tipo_servico}</Text>
                                                            </Row>
                                                            <Row>
                                                                <Text style={styles.title}>Faltam: </Text>
                                                                <Text>{item.id}</Text>
                                                                {/* <Text style={styles.title}>Faltam: {diferenca} </Text> */}
                                                            </Row>
                                                        </Col>
                                                    </Grid>
                                                </TouchableOpacity>
                                                <View style={{ height: (this.state.expanded === item.codigo_servico) ? null : 0, overflow: 'hidden' }}>
                                                    <ScrollView>
                                                        <Grid>
                                                            <Col>
                                                                <Text style={styles.title}>Data:</Text>
                                                                <Text style={styles.body}>{item.data_hora ? moment(item.data_hora).format('DD/MM/YYYY') : null}</Text>
                                                            </Col>
                                                            <Col>
                                                                <Text style={styles.title}>Hora:</Text>
                                                                <Text style={styles.body}>{item.data_hora ? moment(item.data_hora).format('HH:MM') : null}</Text>
                                                            </Col>
                                                            <Col>
                                                                <Text style={styles.title}>Voo:</Text>
                                                                <Text style={styles.body}>{item.numero_voo}</Text>
                                                            </Col>
                                                        </Grid>
                                                        <Grid>
                                                            <Col style={{ width: '66.666%' }}>
                                                                <Text style={styles.title}>Cliente:</Text>
                                                                <Text style={styles.body}>{item.cliente}</Text>
                                                            </Col>
                                                            <Col>
                                                                <Text style={styles.title}>Telefone:</Text>
                                                                <Text style={styles.body}>{item.cliente_telefone}</Text>
                                                            </Col>
                                                        </Grid>
                                                        <Grid>
                                                            <Col>
                                                                <Text style={styles.title}>Recolha:</Text>
                                                                {
                                                                    item.zona_origem !== null ?
                                                                        <Text style={styles.body}>{item.zona_origem}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.morada_origem !== null ?
                                                                        <Text style={styles.body}>{item.morada_origem}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.local_frequente_origem !== null ?
                                                                        <Text style={styles.body}>{item.local_frequente_origem}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.ponto_referencia !== null ?
                                                                        <Text style={styles.bodyReference}>{item.ponto_referencia}</Text>
                                                                        : null
                                                                }
                                                            </Col>
                                                        </Grid>
                                                        <Grid>
                                                            <Col>
                                                                <Text style={styles.title}>Entrega:</Text>
                                                                {
                                                                    item.zona_destino !== null ?
                                                                        <Text style={styles.body}>{item.zona_destino}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.morada_destino !== null ?
                                                                        <Text style={styles.body}>{item.morada_destino}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.local_frequente_destino !== null ?
                                                                        <Text style={styles.body}>{item.local_frequente_destino}</Text>
                                                                        : null
                                                                }
                                                                {
                                                                    item.ponto_referencia_entrega !== null ?
                                                                        <Text style={styles.bodyReference}>{item.ponto_referencia_entrega}</Text>
                                                                        : null
                                                                }
                                                            </Col>
                                                        </Grid>
                                                        <Grid>
                                                            <Col>
                                                                <Grid>
                                                                    <Col style={{ width: '25%' }}>
                                                                        <MyIcon style={{ color: '#535557' }} name="fonte-tg-24" size={25} />
                                                                    </Col>
                                                                    <Col>
                                                                        <Text style={{ color: '#086e6c', fontSize: 20, fontWeight: 'bold' }}>{item.adultos}</Text>
                                                                    </Col>
                                                                </Grid>
                                                            </Col>
                                                            <Col>
                                                                <Grid>
                                                                    <Col style={{ width: '25%' }}>
                                                                        <MyIcon style={{ color: '#535557' }} name="fonte-tg-25" size={25} />
                                                                    </Col>
                                                                    <Col>
                                                                        <Text style={{ color: '#086e6c', fontSize: 20, fontWeight: 'bold' }}>{item.criancas}</Text>
                                                                    </Col>
                                                                </Grid>
                                                            </Col>
                                                            <Col>
                                                                <Grid>
                                                                    <Col style={{ width: '25%' }}>
                                                                        <MyIcon style={{ color: '#535557' }} name="fonte-tg-26" size={25} />
                                                                    </Col>
                                                                    <Col>
                                                                        <Text style={{ color: '#086e6c', fontSize: 20, fontWeight: 'bold' }}>{item.bebes}</Text>
                                                                    </Col>
                                                                </Grid>
                                                            </Col>
                                                        </Grid>
                                                        {renderBagagens}
                                                        {/* {renderObservacoesBagagens} */}
                                                        <Grid>
                                                            <Col style={{ width: '66.666%' }}>
                                                                <Text style={styles.title}>Operador:</Text>
                                                                <Text style={styles.body}>{item.operador}</Text>
                                                            </Col>
                                                            <Col>
                                                                <Text style={styles.title}>Ticket:</Text>
                                                                <Text style={styles.body}>{item.ticket}</Text>
                                                            </Col>
                                                        </Grid>
                                                        <Grid>
                                                            <Col style={{ width: '66.666%' }}>
                                                                <Text style={styles.title}>Viatura:</Text>
                                                                <Text style={styles.body}>{item.marca} / {item.modelo}</Text>
                                                            </Col>
                                                            <Col>
                                                                <Text style={styles.title}>Matrícula:</Text>
                                                                <Text style={styles.body}>{item.matricula}</Text>
                                                            </Col>
                                                        </Grid>
                                                    </ScrollView>
                                                </View>
                                                <View style={{ height: 170, marginTop: 5 }}>
                                                    <Grid>
                                                        <Col onPress={() => this.mudarEstados(
                                                            item.codigo_servico,
                                                            item.id,
                                                            item.estado_id,
                                                            item.valor_servico,
                                                            item.valor_extras,
                                                            item.total_retorno,
                                                            item.total_servico,
                                                            item.email,
                                                            item.cliente_id,
                                                        )} style={{ backgroundColor: '#839608', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                                                            {
                                                                item.estado_id === 2 ?
                                                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                        <MyIcon style={{ color: '#fff' }} name="fonte-tg-36" size={20} />
                                                                        <Text style={{ color: '#fff', fontSize: 12 }}>ACEITAR</Text>
                                                                    </View>
                                                                    :
                                                                    item.estado_id === 3 ?
                                                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                            <MyIcon style={{ color: '#fff' }} name="fonte-tg-53" size={20} />
                                                                            <Text style={{ color: '#fff', fontSize: 12 }}>RECOLHA</Text>
                                                                        </View>
                                                                        :
                                                                        item.estado_id === 9 ?
                                                                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                                <Icon name="play" size={20} color="#fff" />
                                                                                <Text style={{ color: '#fff', fontSize: 12 }}>INICIAR</Text>
                                                                            </View>
                                                                            :
                                                                            item.estado_id === 4 ?
                                                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                                    <Icon name="stop" size={20} color="#fff" />
                                                                                    <Text style={{ color: '#fff', fontSize: 12 }}>FECHAR</Text>
                                                                                </View>
                                                                                : null
                                                                // item.estado_id === 5 ?
                                                                //     <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                                                //         <Icon name="stop" size={20} color="#fff" />
                                                                //         <Text style={{ color: '#fff', fontSize: 12 }}>FECHAR</Text>
                                                                //     </View> : null
                                                            }
                                                        </Col>
                                                        <Col onPress={() => this.goToRejeitar(item.id, item.estado_id, item.codigo_servico)} style={{ backgroundColor: '#B72500', borderTopRightRadius: 5, borderBottomRightRadius: 5, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#fff' }} name="fonte-tg-90" size={20} />
                                                            <Text style={{ color: '#fff', fontSize: 12 }}>REJEITAR</Text>
                                                        </Col>
                                                    </Grid>
                                                    <Grid>
                                                        <Col onPress={() => this.goToNomeCliente(item.cliente, item.morada_destino, item.numero_voo)} style={{ backgroundColor: '#6D9481', borderTopLeftRadius: 5, borderBottomLeftRadius: 5, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#fff' }} name="fonte-tg-39" size={20} />
                                                            <Text style={{ color: '#fff', fontSize: 12 }}>NOME</Text>
                                                        </Col>
                                                        <Col onPress={() => { <Map /> }} style={{ backgroundColor: '#007892', borderTopRightRadius: 5, borderBottomRightRadius: 5, height: 50, alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#fff' }} name="fonte-tg-41" size={20} />
                                                            <Text style={{ color: '#fff', fontSize: 12 }}>NAVEGAR</Text>
                                                        </Col>
                                                    </Grid>
                                                    <Grid>
                                                        <Col style={{ backgroundColor: '#D7D8DA', alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#000' }} name="fonte-tg-22" size={20} />
                                                            <Text style={{ color: '#000', fontSize: 12 }}>CLIENTE</Text>
                                                        </Col>
                                                        <Col onPress={this._pressCall} style={{ backgroundColor: '#D7D8DA', alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#000' }} name="fonte-tg-12" size={20} />
                                                            <Text style={{ color: '#000', fontSize: 12 }}>LIGAR</Text>
                                                        </Col>
                                                        <Col onPress={item.numero_voo !== null ? () => { Linking.openURL('https://www.google.com/search?hl=pt-PT&q=' + item.numero_voo) } : console.log('')}
                                                            style={{ backgroundColor: '#D7D8DA', alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#000' }} name="fonte-tg-17" size={20} />
                                                            <Text style={{ color: '#000', fontSize: 12 }}>VOO</Text>
                                                        </Col>
                                                        <Col onPress={() => this.goToExtras(item.codigo_servico, item.id, item.total_extras)} style={{ backgroundColor: '#D7D8DA', alignItems: 'center', justifyContent: 'center' }}>
                                                            <MyIcon style={{ color: '#000' }} name="fonte-tg-50" size={20} />
                                                            <Text style={{ color: '#000', fontSize: 12 }}>EXTRAS({item.total_extras})</Text>
                                                        </Col>
                                                    </Grid>
                                                </View>
                                            </View>
                                            : null
                                    }
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null
                    }
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                            <Text style={styles.textSair}> SAIR </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default withNavigation(TodayServices);