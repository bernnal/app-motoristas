import { StyleSheet, Dimensions, Platform} from 'react-native'

const { width: WIDTH } = Dimensions.get('window')

const styles = StyleSheet.create({
    backgroundContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fafafa',
        marginTop: 10,
    },
    dashboard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '5%',
        backgroundColor: '#E6AC4A',
        flexWrap: 'wrap',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    logoContainer: {
        //flexDirection: 'row',
        justifyContent: 'center',  
        alignItems: 'center',
        //flexWrap: 'wrap',
        width: 40,
        height: 40,
        backgroundColor: '#fff',
        borderRadius: 20,
        marginLeft: 10
    },
    textDashboard: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center'
    },
    title: {
        color: '#252525',
        fontWeight: 'bold'
    },
    body: {
        color: '#086e6c',
        marginTop: 2,
    },
    bodyReference: {
        color: '#086e6c',
        marginTop: 2,
        fontStyle: 'italic',
    },
    container: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center',
        paddingTop: (Platform.OS === 'ios') ? 20 : 0
    },     
    text: {
        fontSize: 17,
        color: 'black',
        padding: 10
    },    
    btnText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20
    },    
    btnTextHolder: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.5)'
    },    
    Btn: {
        height: 50
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 50,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    textSair: {
        color: 'rgba(255, 255, 255, 255)',
        fontSize: 14,
        textAlign: 'center',
    },
})

export default styles;