import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Dimensions,
    TextInput,
    Platform,
    ScrollView,
    TouchableOpacity,
    StyleSheet
} from 'react-native'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import { Container, Content, Picker, Form, } from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';
import icoMoonConfig from '../resources/fonts/selection.json'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class Header extends Component {

    constructor(props) {
        super(props)
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-113" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    render() {
        return (
            <View style={styles.dashboard}>
                {/* onPress={() => this.props.navigation.openDrawer()} */}
                <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                    <Icon style={{ marginLeft: 10 }} name="bars" size={30} color="#535557" />
                </TouchableOpacity>
                <Text style={styles.textDashboard}></Text>
                <MyIcon style={{ color: '#535557', marginRight: 10 }} name="fonte-tg-27" size={30} />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    dashboard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '5%',
        backgroundColor: '#E6AC4A',
        flexWrap: 'wrap',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    textDashboard: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center'
    },
})