import React, { Component } from 'react'
import {
    View,
    Text,
    ImageBackground,
    Dimensions,
    TextInput,
    Platform,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Animated,
    TouchableWithoutFeedback,
    StyleSheet,
    Button,
    Picker,
    DatePickerIOS,
    Alert
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import SwitchToggle from 'react-native-switch-toggle';
import Icon from 'react-native-vector-icons/FontAwesome';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Textarea from 'react-native-textarea'
import axios from 'axios'
import icoMoonConfig from '../../resources/fonts/selection.json'
import bgImage from '../../images/background_v.jpg'
import styles from './styles'
import Footer from '../Footer'
import moment from 'moment'
import {
    API_OPERADORES,
    API_CATEGORIAS,
    API_CLASSES,
    API_ZONA_ORIGEM,
    API_ZONA_DESTINO,
    API_GOOGLE_AUTOCOMPLETE,
    API_GOOGLE_CONVERT_LAT_LONG,
    API_INCLUI_SERVICO_TRANSFERZONA,
    API_TIPOS_COBRANCA,
    API_TIPOS_SERVICOS,
} from '../../config'

const { width: WIDTH } = Dimensions.get('window')
const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class NovoServico extends Component {
    constructor(props) {
        super(props)
        this.state = {
            api_token: this.props.navigation.state.params.api_token,

            time: '',
            date: new Date(),
            auxDate: '',
            modalIsVisibleDate: false,
            modalIsVisibleTime: false,

            numberAdults: '1',
            numberChilds: '0',
            numberBaby: '0',
            modalIsVisibleAdults: false,
            modalIsVisibleChilds: false,
            modalIsVisibleBaby: false,

            descOperador: '',
            descOperadorIOS: 'Selecione o Operador',
            idOperador: '',
            operador: false,
            dataSourceOperadores: [],
            modalIsVisibleOperadores: false,
            transfer_zona: 'transfer_zona',

            descCategoria: '',
            descCategoriaIOS: 'Selecione a Categoria',
            idCategoria: '',
            categoria: false,
            dataSourceCategorias: [],
            modalIsVisibleCategorias: false,

            descClasseGrupo: '',
            descClasseIOS: 'Selecione a Classe',
            descGrupoIOS: 'Selecione a Classe',
            classeId: '',
            grupoId: '',
            classeProdutoId: '',
            c: '',
            classeGrupo: false,
            dataSourceClasseGrupo: [],
            modalIsVisibleClasseGrupo: false,

            dataSourceTiposServicos: [],
            servicoId: '',
            servicoNome: '',
            modalIsVisibleTipoServico: false,
            descTipoServico: '',
            descTipoServicoIOS: 'Tipo Serviço',

            descZonaOrigem: '',
            descZonaOrigemIOS: 'Zona Recolha',
            origemId: '',
            zonaOrigem: false,
            dataSourceZonaOrigem: [],
            modalIsVisibleZonaOrigem: false,

            descZonaDestino: '',
            descZonaDestinoIOS: 'Zona Entrega',
            destinoId: '',
            preco: '',
            km: '',
            zonaDestino: false,
            dataSourceZonaDestino: [],
            modalIsVisibleZonaDestino: false,

            region: {},

            destination: "",
            placeId: "",
            predictions: [],
            destination2: "",
            placeId2: "",
            predictions2: [],
            keyGoogle: 'AIzaSyDRMWdQLj7bze-4tPSpkEoUTyfz7JRR4WE',

            dataSourceTiposCobrancas: [],
            cobrancaId: '',
            cobrancaNome: '',
            modalIsVisibleTipoCobranca: false,
            descTipoCobranca: '',
            descTipoCobrancaIOS: 'Cobrança',


            nome: '',
            email: '',
            telefone: '',
            observacoes: '',

            emitirVoucher: false,
            emitirVoucherValue: 0,
            atribuirMim: false,
            atribuirMimValue: 0,

            modalAnimatedValue: new Animated.Value(0),
        }
        this.setDate = this.setDate.bind(this)
    }

    componentDidMount() {
        axios.get(`${API_OPERADORES}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.transfer_zona,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceOperadores) => {
                this.setState({
                    // operador: true,
                    dataSourceOperadores: dataSourceOperadores.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));

        axios.get(`${API_TIPOS_COBRANCA}`, {
            params: {
                'param1': this.state.api_token,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((responseTiposCobrancas) => {
                this.setState({
                    dataSourceTiposCobrancas: responseTiposCobrancas.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));

        axios.get(`${API_TIPOS_SERVICOS}`, {
            params: {
                'param1': this.state.api_token,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((responseTiposServicos) => {
                console.log('responseTiposServicos', responseTiposServicos)
                this.setState({
                    dataSourceTiposServicos: responseTiposServicos.data['data'],
                })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));
    }

    async onChangeDestination(destination) {
        this.setState({ destination })
        const apiUrl = `${API_GOOGLE_AUTOCOMPLETE}` + this.state.keyGoogle + `&input=${destination}`
        try {
            const result = await fetch(apiUrl)
            const json = await result.json()
            this.setState({
                predictions: json.predictions
            })

        } catch (err) {
            console.log(err)
        }
    }

    async onChangeDestination2(destination2) {
        this.setState({ destination2 })
        const apiUrl = `${API_GOOGLE_AUTOCOMPLETE}` + this.state.keyGoogle + `&input=${destination2}`
        try {
            const result = await fetch(apiUrl)
            const json = await result.json()
            this.setState({
                predictions2: json.predictions
            })
            console.log('prediction2', prediction2)
        } catch (err) {
            console.log(err)
        }
    }

    pressedPrediction(prediction) {
        this.setState({
            predictions: [],
            destination: prediction.description,
        });
        this.state.placeId = prediction.place_id
        this.getLatLongByPlaceId();
    }

    pressedPrediction2(prediction2) {
        this.setState({
            predictions2: [],
            destination2: prediction2.description,
        });
        this.state.placeId2 = prediction2.place_id
        this.getLatLongByPlaceId2();
    }

    getLatLongByPlaceId = () => {
        axios.get(`${API_GOOGLE_CONVERT_LAT_LONG}`, {
            params: {
                'place_id': this.state.placeId,
                'key': this.state.keyGoogle,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataGoogle) => {
                this.setState({
                    dataLocation: dataGoogle.data.results[0].geometry.location,
                })
                console.log('origem: ', this.state.dataLocation)
            })
            .catch(error => console.log(error));
    }

    getLatLongByPlaceId2 = () => {
        axios.get(`${API_GOOGLE_CONVERT_LAT_LONG}`, {
            params: {
                'place_id': this.state.placeId2,
                'key': this.state.keyGoogle,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataGoogle2) => {
                this.setState({
                    dataLocation2: dataGoogle2.data.results[0].geometry.location,
                })
                console.log('destino: ', this.state.dataLocation2)
            })
            .catch(error => console.log(error));
    }

    /* #region functions */

    /*onValueChange*/
    onValueChangeAdults(label: string) {
        this.setState({
            numberAdults: label
        })
        console.log('numberAdults', this.state.numberAdults)
    }

    onValueChangeChild(value: string) {
        this.setState({
            numberChilds: value
        })
    }

    onValueChangeBaby(value: string) {
        this.setState({
            numberBaby: value
        })
    }

    onValueChangeOperadores(value: string, index: string) {
        let idOperador = this.state.dataSourceOperadores[index].id
        let operadorSelecionado = this.state.dataSourceOperadores[index].nome

        this.setState({
            descOperador: value,
            idOperador: idOperador,
            descOperadorIOS: operadorSelecionado,
            categoria: true,
        });
        console.log('idOperador', this.state.idOperador)
    }

    onValueChangeCategorias(value: string, index: string) {
        let idCategoria = this.state.dataSourceCategorias[index].id
        let categoriaSelecionada = this.state.dataSourceCategorias[index].nome

        this.setState({
            //categoria: true,
            descCategoria: value,
            idCategoria: idCategoria,
            descCategoriaIOS: categoriaSelecionada
        });
    }

    onValueChangeClasseGrupo(value: string, index: string) {
        let idClasse = this.state.dataSourceClasseGrupo[index].classe_id
        let classeSelecionada = this.state.dataSourceClasseGrupo[index].classe_nome
        let idGrupo = this.state.dataSourceClasseGrupo[index].grupo_id
        let grupoSelecionado = this.state.dataSourceClasseGrupo[index].grupo_nome
        let min_pessoas = this.state.dataSourceClasseGrupo[index].min_pessoas
        let max_pessoas = this.state.dataSourceClasseGrupo[index].max_pessoas
        let classeProdutoId = this.state.dataSourceClasseGrupo[index].classe_produto_id

        let classeelecionadaInt = classeSelecionada + ': lotação' + ' ' + min_pessoas + '-' + max_pessoas
        let grupoSelecionadoInt = grupoSelecionado + ': lotação' + ' ' + min_pessoas + '-' + max_pessoas

        let classe_selecionada = null
        idClasse !== null ? classe_selecionada = 'C' : ''

        this.setState({
            descClasseGrupo: value,
            classeId: idClasse,
            descClasseIOS: classeelecionadaInt,
            grupoId: idGrupo,
            descGrupoIOS: grupoSelecionadoInt,
            classeProdutoId: classeProdutoId,
            c: classe_selecionada
        });
    }

    onValueChangeTipoServico(value: string, index: string) {
        let serivcoId = this.state.dataSourceTiposServicos[index].id
        let nome = this.state.dataSourceTiposServicos[index].nome

        this.setState({
            descTipoServico: value,
            serivcoId: serivcoId,
            descTipoServicoIOS: nome
        })
    }

    onValueChangeZonaOrigem(value: string, index: string) {
        let origemId = this.state.dataSourceZonaOrigem[index].id
        let nome = this.state.dataSourceZonaOrigem[index].nome
        let regiaoNome = this.state.dataSourceZonaOrigem[index].regiao_nome
        console.log('dataSourceZonaOrigem: ', this.state.dataSourceZonaOrigem)

        let zona_origem = nome + ', ' + regiaoNome

        this.setState({
            descZonaOrigem: value,
            descZonaOrigemIOS: zona_origem,
            origemId: origemId
        })
    }

    onValueChangeZonaDestino(value: string, index: string) {
        let destinoId = this.state.dataSourceZonaDestino[index].id
        let nome = this.state.dataSourceZonaDestino[index].nome
        let preco = this.state.dataSourceZonaDestino[index].preco
        let km = this.state.dataSourceZonaDestino[index].km
        let regiaoNome = this.state.dataSourceZonaDestino[index].regiao_nome

        let zona_destino = nome + ', ' + regiaoNome

        console.log('dataSourceZonaDestino', this.state.dataSourceZonaDestino)

        this.setState({
            descZonaDestino: value,
            descZonaDestinoIOS: zona_destino,
            destinoId: destinoId,
            preco: preco,
            km: km
        })
    }

    onValueChangeTipoCobranca(value: string, index: string) {
        let cobrancaId = this.state.dataSourceTiposCobrancas[index].id
        let nome = this.state.dataSourceTiposCobrancas[index].nome

        this.setState({
            descTipoCobranca: value,
            cobrancaId: cobrancaId,
            descTipoCobrancaIOS: nome
        })
    }

    /*handlePressOpen*/
    _handlePressOpenDate = () => {
        if (this.state.modalIsVisibleDate) {
            return;
        }

        this.setState({ modalIsVisibleDate: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenTime = () => {
        if (this.state.modalIsVisibleTime) {
            return;
        }

        this.setState({ modalIsVisibleTime: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenAdults = () => {
        if (this.state.modalIsVisibleAdults) {
            return;
        }

        this.setState({ modalIsVisibleAdults: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenChilds = () => {
        if (this.state.modalIsVisibleChilds) {
            return;
        }

        this.setState({ modalIsVisibleChilds: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenBaby = () => {
        if (this.state.modalIsVisibleBaby) {
            return;
        }

        this.setState({ modalIsVisibleBaby: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenOperador = () => {
        if (this.state.modalIsVisibleOperadores) {
            return;
        }

        this.setState({ modalIsVisibleOperadores: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenCategoria = () => {
        if (this.state.modalIsVisibleCategorias) {
            return;
        }

        this.setState({ modalIsVisibleCategorias: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenClasseGrupo = () => {
        if (this.state.modalIsVisibleClasseGrupo) {
            return;
        }

        this.setState({ modalIsVisibleClasseGrupo: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenTipoServico = () => {
        if (this.state.modalIsVisibleTipoServico) {
            return;
        }

        this.setState({ modalIsVisibleTipoServico: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenZonaOrigem = () => {
        if (this.state.modalIsVisibleZonaOrigem) {
            return;
        }

        this.setState({ modalIsVisibleZonaOrigem: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenZonaDestino = () => {
        if (this.state.modalIsVisibleZonaDestino) {
            return;
        }

        this.setState({ modalIsVisibleZonaDestino: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    _handlePressOpenTipoCobranca = () => {
        if (this.state.modalIsVisibleTipoCobranca) {
            return;
        }

        this.setState({ modalIsVisibleTipoCobranca: true }, () => {
            Animated.timing(this.state.modalAnimatedValue, {
                toValue: 1,
                duration: 200,
                useNativeDriver: true,
            }).start();
        });
    };

    /*handlePressDone*/
    _handlePressDoneDate = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleDate: false });
        });
    };

    _handlePressDoneTime = () => {
        this.state.auxDate = this.state.date
        console.log('auxDateeee', this.state.auxDate)
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleTime: false });
        });
    };

    _handlePressDoneAdults = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleAdults: false });
        });
    };

    _handlePressDoneChilds = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleChilds: false });
        });
    };

    _handlePressDoneBaby = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleBaby: false });
        });
    };

    _handlePressDoneOperador = () => {
        console.log('idOperar dentro do Done', this.state.idOperador)
        axios.get(`${API_CATEGORIAS}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.idOperador
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceCategorias) => {
                this.setState({
                    categoria: true,
                    dataSourceCategorias: dataSourceCategorias.data['data'],
                })
            })
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleOperadores: false });
        });
    };

    _handlePressDoneCategoria = () => {
        console.log('id categoria dentro do Done', this.state.idCategoria)
        axios.get(`${API_CLASSES}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.idCategoria
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceClasseGrupo) => {
                this.setState({
                    classeGrupo: true,
                    dataSourceClasseGrupo: dataSourceClasseGrupo.data['data'],
                })
            })
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleCategorias: false });
        });
    };

    _handlePressDoneClasseGrupo = () => {
        this.state.c === null ?
            axios.get(`${API_ZONA_ORIGEM}`, {
                params: {
                    'param1': this.state.api_token,
                    'param2': this.state.idOperador,
                    'param3': this.state.idCategoria,
                    'param4': this.state.c,
                    'param5': this.state.grupoId,
                    'param6': this.state.classeProdutoId,
                }
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((dataSourceZonaOrigem) => {
                    this.setState({
                        zonaOrigem: true,
                        dataSourceZonaOrigem: dataSourceZonaOrigem.data['data'],
                    })
                    console.log('lista de grupos dentro do axios', this.state.dataSourceZonaOrigem)
                })
            :
            axios.get(`${API_ZONA_ORIGEM}`, {
                params: {
                    'param1': this.state.api_token,
                    'param2': this.state.idOperador,
                    'param3': this.state.idCategoria,
                    'param4': this.state.c,
                    'param5': this.state.classeId,
                    'param6': this.state.classeProdutoId,
                }
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((dataSourceZonaOrigem) => {
                    this.setState({
                        zonaOrigem: true,
                        dataSourceZonaOrigem: dataSourceZonaOrigem.data['data'],
                    })
                })
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleClasseGrupo: false });
        });
    };

    _handlePressDoneTipoServico = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleTipoServico: false });
        });
    };

    _handlePressDoneZonaOrigem = () => {
        this.state.c === null ?
            axios.get(`${API_ZONA_DESTINO}`, {
                params: {
                    'param1': this.state.api_token,
                    'param2': this.state.idOperador,
                    'param3': this.state.idCategoria,
                    'param4': this.state.c,
                    'param5': this.state.grupoId,
                    'param6': this.state.origemId,
                    'param7': moment(this.state.auxDate).format('YYYY-MM-DD HH:MM:ss'),
                    'param8': this.state.classeProdutoId
                }
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((dataSourceZonaDestino) => {
                    console.log('entrou aqui 1')
                    this.setState({
                        zonaDestino: true,
                        dataSourceZonaDestino: dataSourceZonaDestino.data['data'],
                    })
                    console.log('lista de grupos dentro do axios', this.state.dataSourceZonaDestino)
                })
            :
            axios.get(`${API_ZONA_DESTINO}`, {
                params: {
                    'param1': this.state.api_token,
                    'param2': this.state.idOperador,
                    'param3': this.state.idCategoria,
                    'param4': this.state.c,
                    'param5': this.state.classeId,
                    'param6': this.state.origemId,
                    'param7': moment(this.state.auxDate).format('YYYY-MM-DD HH:MM:ss'),
                    'param8': this.state.classeProdutoId
                }
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((dataSourceZonaDestino) => {
                    console.log('entrou aqui 2')
                    this.setState({
                        zonaDestino: true,
                        dataSourceZonaDestino: dataSourceZonaDestino.data['data'],
                    })
                    console.log('lista de classes dentro do axios', this.state.dataSourceZonaDestino)
                })
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleZonaOrigem: false });
        });
    };

    _handlePressDoneZonaDestino = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleZonaDestino: false });
        });
    };

    _handlePressDoneTipoCobranca = () => {
        Animated.timing(this.state.modalAnimatedValue, {
            toValue: 0,
            duration: 150,
            useNativeDriver: true,
        }).start(() => {
            this.setState({ modalIsVisibleTipoCobranca: false });
        });
    };

    setDate(newDate) {
        this.setState({ date: newDate })
    }

    getButtonText() {
        return this.state.emitirVoucher ? 'Sim' : 'Não';
    }

    getRightText() {
        return this.state.emitirVoucher ? '' : 'Sim';
    }

    getLeftText() {
        return this.state.emitirVoucher ? 'Não' : '';
    }

    getButtonText2() {
        return this.state.atribuirMim ? 'Sim' : 'Não';
    }

    getRightText2() {
        return this.state.atribuirMim ? '' : 'Sim';
    }

    getLeftText2() {
        return this.state.atribuirMim ? 'Não' : '';
    }

    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-57" style={{ fontSize: 24, color: tintColor }} />
        )
    }

    createService = () => {
        try {
            this.setState({ error: '', loading: true })

            axios.post(`${API_INCLUI_SERVICO_TRANSFERZONA}`, {
                'param1': this.state.api_token,
                'param2': moment(this.state.date).format('DD-MM-YYYY'),
                'param3': moment(this.state.date).format('HH:mm'),
                'param4': this.state.numberAdults,
                'param5': this.state.numberChilds,
                'param6': this.state.numberBaby,
                'param7': this.state.idOperador,
                'param8': this.state.idCategoria,
                'param9': this.state.c === null ? 'G' + this.state.grupoId : 'C' + this.state.classeId,
                'param10': this.state.origemId,
                'param11': this.state.destination,
                'param12': this.state.destinoId,
                'param13': this.state.destination2,
                'param14': this.state.nome,
                'param15': this.state.email,
                'param16': this.state.telefone,
                'param17': this.state.cobrancaId,
                'param18': this.state.preco,
                'param19': this.state.observacoes,
                // 'param21': this.state.,
                // 'param22': this.state.,
                // 'param23': this.state.,
                // 'param24': this.state.,
                'param25': this.state.preco,
                'param26': this.state.km,
                'param27': this.state.emitirVoucherValue,
                'param28': this.state.atribuirMimValue,
                'param29': this.state.servicoId,
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((jsonResponse) => {
                    if (jsonResponse.data.error) {
                        Alert.alert((jsonResponse.data.error))
                    } else {
                        Alert.alert(('Criado com Sucesso.'));
                    }
                })
                .catch((error) => {
                    console.log("axios error:", error);
                });
        } catch (err) {
            Alert.alert((err))
        }
    }

    /* #endregion */

    /*render*/
    render() {

        const predictions = this.state.predictions.map(prediction => (
            <TouchableHighlight
                key={prediction.id}
                onPress={() => this.pressedPrediction(prediction)}>
                <Text style={styles.suggestions}>
                    {prediction.description}
                </Text>
            </TouchableHighlight>
        ))

        const predictions2 = this.state.predictions2.map(prediction2 => (
            <TouchableHighlight
                key={prediction2.id}
                onPress={() => this.pressedPrediction2(prediction2)}>
                <Text style={styles.suggestions}>
                    {prediction2.description}
                </Text>
            </TouchableHighlight>
        ))

        /* #region funcoes dentro do render */
        let list_operadores = this.state.dataSourceOperadores;
        if (typeof (list_operadores) !== undefined) {
            list_operadores = [list_operadores][0];
        }
        
        let list_categorias = this.state.dataSourceCategorias;
        if (typeof (list_categorias) !== undefined) {
            list_categorias = [list_categorias][0];
        }

        let list_classeGrupo = this.state.dataSourceClasseGrupo;
        if (typeof (list_classeGrupo) !== undefined) {
            list_classeGrupo = [list_classeGrupo][0];
        }

        let list_tiposServico = this.state.dataSourceTiposServicos;
        if (typeof (list_tiposServico) !== undefined) {
            list_tiposServico = [list_tiposServico][0];
        }

        let list_zonaOrigem = this.state.dataSourceZonaOrigem;
        if (typeof (list_zonaOrigem) !== undefined) {
            list_zonaOrigem = [list_zonaOrigem][0];
        }

        let list_zonaDestino = this.state.dataSourceZonaDestino;
        if (typeof (list_zonaDestino) !== undefined) {
            list_zonaDestino = [list_zonaDestino][0];
        }

        let list_tiposCobrancas = this.state.dataSourceTiposCobrancas;
        if (typeof (list_tiposCobrancas) !== undefined) {
            list_tiposCobrancas = [list_tiposCobrancas][0];
        }

        let precoServico = typeof (this.state.preco) !== 'undefined' ? this.state.preco : 0

        let datePicker = null
        let modalPickerNumberPassenger = null
        let modalPickerOperadores = null
        let modalPickerCategorias = null
        let modalPickerClasseGrupo = null
        let modalPickerTiposServicos = null
        let modalPickerZonaOrigem = null
        let modalPickerZonaDestino = null
        let modalPickerTiposCobrancas = null
        if (Platform.OS === 'ios') {
            datePicker = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        width: '45%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-48" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenDate}>
                            <View style={{ height: 45, flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 10,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>
                                    {moment(this.state.date).format('DD-MM-YYYY')}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '45%',
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-16" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenTime}>
                            <View style={{ height: 45, flexDirection: 'column' }}>
                                <Text style={{
                                    placeholder: 'Selecionar Hora',
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>
                                    {moment(this.state.date).format('LT')}
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
            modalPickerNumberPassenger = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-24" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenAdults}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberAdults}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-25" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenChilds}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberChilds}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-26" size={25} />
                        </View>
                        <TouchableWithoutFeedback onPress={this._handlePressOpenBaby}>
                            <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>{this.state.numberBaby}</Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            )
            modalPickerOperadores = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-43" size={25} />
                    </View>
                    <TouchableWithoutFeedback onPress={this._handlePressOpenOperador}>
                        <View style={{
                            height: 45,
                            width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                            borderWidth: 1,
                            borderColor: '#CCC',
                            flexDirection: 'column'
                        }}>
                            <Text style={{
                                paddingTop: 13,
                                height: 45,
                                fontSize: 14,
                                paddingLeft: 15,
                                borderWidth: 1,
                                borderColor: '#CCC',
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: '#000'
                            }}>{this.state.descOperadorIOS}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
            modalPickerCategorias = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-67" size={25} />
                    </View>
                    {
                        this.state.categoria ?
                            <TouchableWithoutFeedback onPress={this._handlePressOpenCategoria}>
                                <View style={{
                                    height: 45,
                                    width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        paddingTop: 13,
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 15,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}>{this.state.descCategoriaIOS}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            :
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>Categorias</Text>
                            </View>
                    }
                </View>
            )
            modalPickerClasseGrupo = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
                    </View>
                    {
                        this.state.classeGrupo ?
                            <TouchableWithoutFeedback onPress={this._handlePressOpenClasseGrupo}>
                                <View style={{
                                    height: 45,
                                    width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        paddingTop: 13,
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 15,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}>{this.state.classeId === null ? this.state.descGrupoIOS : this.state.descClasseIOS}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            :
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>Classe</Text>
                            </View>
                    }
                </View>
            )
            modalPickerTiposServicos = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-30" size={25} />
                    </View>
                    <TouchableWithoutFeedback onPress={this._handlePressOpenTipoServico}>
                        <View style={{
                            height: 45,
                            width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                            borderWidth: 1,
                            borderColor: '#CCC',
                            flexDirection: 'column'
                        }}>
                            <Text style={{
                                paddingTop: 13,
                                height: 45,
                                fontSize: 14,
                                paddingLeft: 15,
                                borderWidth: 1,
                                borderColor: '#CCC',
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: '#000'
                            }}>{this.state.descTipoServicoIOS}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )

            modalPickerZonaOrigem = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-99" size={25} />
                    </View>
                    {
                        this.state.zonaOrigem ?
                            <TouchableWithoutFeedback onPress={this._handlePressOpenZonaOrigem}>
                                <View style={{
                                    height: 45,
                                    width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        paddingTop: 13,
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 15,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}>{this.state.descZonaOrigemIOS}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            :
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column'
                            }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}>Zona Origem</Text>
                            </View>
                    }
                </View>
            )
            modalPickerZonaDestino = (
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginTop: 10,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-99" size={25} />
                    </View>
                    {
                        this.state.zonaDestino ?
                            <TouchableWithoutFeedback onPress={this._handlePressOpenZonaDestino}>
                                <View style={{
                                    height: 45,
                                    width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        paddingTop: 13,
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 15,
                                        borderWidth: 1,
                                        borderColor: '#CCC',
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}>{this.state.descZonaDestinoIOS}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                            :
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                borderWidth: 1,
                                borderColor: '#CCC',
                                flexDirection: 'column',
                                zIndex: 0,
                            }}>
                                <Text style={{
                                    paddingTop: 13,
                                    height: 45,
                                    fontSize: 14,
                                    paddingLeft: 15,
                                    borderWidth: 1,
                                    borderColor: '#CCC',
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000',
                                    zIndex: 0,
                                }}>Zona Destino</Text>
                            </View>
                    }
                </View>
            )
            modalPickerTiposCobrancas = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '40%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-47" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <TextInput
                                style={{
                                    height: 45,
                                    fontSize: 16,
                                    paddingLeft: 16,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}
                                editable={false}
                                keyboardType='numeric'
                                value={precoServico}
                                placeholderTextColor={'#000'}
                            >
                            </TextInput>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '55%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-58" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <TouchableWithoutFeedback onPress={this._handlePressOpenTipoCobranca}>
                                <View style={{
                                    height: 45,
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        paddingTop: 13,
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 15,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}>{this.state.descTipoCobrancaIOS}</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </View>
            )
        } else {
            datePicker = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        width: '45%',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-48" size={25} />
                        </View>
                        <DatePicker
                            style={{
                                width: '72%',
                                height: 45,
                                fontSize: 16,
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: 'rgba(0, 0, 0, 0.6)',
                            }}
                            date={this.state.date}
                            onDateChange={(date) => { this.setState({ date: date }) }}
                            mode="date"
                            format="YYYY-MM-DD"
                            minDate={new Date()}
                            maxDate="2099-06-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    paddingLeft: 5,
                                    alignItems: 'flex-start',
                                    width: 10,
                                },
                                placeholderText: {
                                    fontSize: 16,
                                    backgroundColor: '#fff',
                                    color: 'rgba(0, 0, 0, 0.6)',
                                }
                            }}
                            showIcon={false}
                        ></DatePicker>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '45%',
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-16" size={25} />
                        </View>
                        <DatePicker
                            style={{
                                width: '72%',
                                height: 45,
                                fontSize: 12,
                                backgroundColor: 'rgba(255, 255, 255, 255)',
                                color: 'rgba(0, 0, 0, 0.6)',
                            }}
                            placeholder="Selecionar Hora"
                            date={this.state.time}
                            onDateChange={(time) => { this.setState({ time: time }) }}
                            mode="time"
                            format="HH:MM"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                    paddingLeft: 5,
                                    alignItems: 'flex-start',
                                },
                                placeholderText: {
                                    fontSize: 12,
                                    backgroundColor: '#fff',
                                    color: 'rgba(0, 0, 0, 0.6)',
                                }
                            }}
                            showIcon={false}
                        ></DatePicker>
                    </View>
                </View>
            )
            modalPickerNumberPassenger = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-24" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                selectedValue={this.state.numberAdults}
                                onValueChange={this.onValueChangeAdults.bind(this)}
                            >
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                                <Picker.Item label="11" value="11" />
                                <Picker.Item label="12" value="12" />
                                <Picker.Item label="13" value="13" />
                                <Picker.Item label="14" value="14" />
                                <Picker.Item label="15" value="15" />
                                <Picker.Item label="16" value="16" />
                                <Picker.Item label="17" value="17" />
                                <Picker.Item label="18" value="18" />
                                <Picker.Item label="19" value="19" />
                                <Picker.Item label="20" value="20" />
                                <Picker.Item label="21" value="21" />
                                <Picker.Item label="22" value="22" />
                                <Picker.Item label="23" value="23" />
                                <Picker.Item label="24" value="24" />
                                <Picker.Item label="25" value="25" />
                                <Picker.Item label="26" value="26" />
                                <Picker.Item label="27" value="27" />
                                <Picker.Item label="28" value="28" />
                                <Picker.Item label="29" value="29" />
                                <Picker.Item label="30" value="30" />
                            </Picker>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-25" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={this.state.numberChilds}
                                onValueChange={this.onValueChangeChild.bind(this)}
                            >
                                <Picker.Item label="0" value="0" />
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                            </Picker>
                        </View>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '32%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-26" size={25} />
                        </View>
                        <View style={{ height: 45, width: '60%', flexDirection: 'column' }}>
                            <Picker
                                iosHeader="Selecione a quantidade"
                                placeholderStyle={{ color: '#000' }}
                                iosIcon={<Icon name="arrow-down" />}
                                selectedValue={this.state.numberBaby}
                                onValueChange={this.onValueChangeBaby.bind(this)}
                            >
                                <Picker.Item label="0" value="0" />
                                <Picker.Item label="1" value="1" />
                                <Picker.Item label="2" value="2" />
                                <Picker.Item label="3" value="3" />
                                <Picker.Item label="4" value="4" />
                                <Picker.Item label="5" value="5" />
                                <Picker.Item label="6" value="6" />
                                <Picker.Item label="7" value="7" />
                                <Picker.Item label="8" value="8" />
                                <Picker.Item label="9" value="9" />
                                <Picker.Item label="10" value="10" />
                            </Picker>
                        </View>
                    </View>
                </View>
            )
            modalPickerOperadores = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-43" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            placeholder="Selecione o Operador"
                            mode='dropdown'
                            placeholderStyle={{ color: '#000' }}
                            itemStyle={{
                                fontSize: 14,
                                color: '#000',
                            }}
                            selectedValue={this.state.descOperador}
                            onValueChange={this.onValueChangeOperadores.bind(this)}
                        >
                            {
                                list_operadores.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerCategorias = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-67" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            iosHeader="Selecione a Categoria"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descCategoria}
                            onValueChange={this.onValueChangeCategorias.bind(this)}
                        >
                            {
                                list_categorias.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerClasseGrupo = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-27" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            iosHeader="Selecione a Classe"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descClasseGrupo}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeClasseGrupo.bind(this)}
                        >
                            {
                                list_classeGrupo.map((item, key) => {
                                    let tipo = null
                                    item.classe_id === null ?
                                        tipo = (
                                            <Picker.Item
                                                label={item.grupo_nome + ': lotação ' + item.min_pessoas + '-' + item.max_pessoas}
                                                value={item.grupo_id}
                                                key={item}
                                            />
                                        )
                                        :
                                        tipo = (
                                            <Picker.Item
                                                label={item.classe_nome + ': lotação ' + item.min_pessoas + '-' + item.max_pessoas}
                                                value={item.classe_id}
                                                key={item}
                                            />
                                        )
                                    return tipo;
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerTiposServicos = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-30" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            iosHeader="Tipo Servico"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descTipoServico}
                            onValueChange={this.onValueChangeTipoServico.bind(this)}
                        >
                            {
                                list_tiposServico.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerZonaOrigem = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-99" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            iosHeader="Zona Recolha"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descZonaOrigem}
                            onValueChange={this.onValueChangeZonaOrigem.bind(this)}
                        >
                            {
                                list_zonaOrigem.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome + ', ' + item.regiao_nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerZonaDestino = (
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                    backgroundColor: 'rgba(255, 255, 255, 255)',
                    borderColor: '#CCC',
                    borderWidth: 1,
                }}>
                    <View style={styles.data}>
                        <MyIcon style={styles.icon} name="fonte-tg-99" size={25} />
                    </View>
                    <View style={{
                        height: 45,
                        width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                        flexDirection: 'column'
                    }}>
                        <Picker
                            iosHeader="Zona Entrega"
                            placeholderStyle={{ color: '#000' }}
                            selectedValue={this.state.descZonaDestino}
                            onValueChange={this.onValueChangeZonaDestino.bind(this)}
                        >
                            {
                                list_zonaDestino.map((item, key) => {
                                    return <Picker.Item
                                        label={item.nome + ', ' + item.regiao_nome}
                                        value={item.id}
                                        key={item}
                                    />
                                })
                            }
                        </Picker>
                    </View>
                </View>
            )
            modalPickerTiposCobrancas = (
                <View style={styles.spaceInputs}>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        borderColor: '#CCC',
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        width: '40%'
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-47" size={25} />
                        </View>
                        <View style={{ height: 45, width: '80%', flexDirection: 'column' }}>
                            <TextInput
                                style={{
                                    height: 45,
                                    fontSize: 16,
                                    paddingLeft: 16,
                                    backgroundColor: 'rgba(255, 255, 255, 255)',
                                    color: '#000'
                                }}
                                editable={false}
                                keyboardType='numeric'
                                value={precoServico}
                                placeholderTextColor={'#000'}
                            >
                            </TextInput>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        borderWidth: 1,
                        backgroundColor: 'rgba(255, 255, 255, 255)',
                        borderColor: '#CCC',
                        width: '55%',
                    }}>
                        <View style={styles.data}>
                            <MyIcon style={styles.icon} name="fonte-tg-58" size={25} />
                        </View>
                        <View style={{ height: 45, width: '80%', flexDirection: 'column' }}>
                            <Picker
                                placeholderStyle={{ color: '#000' }}
                                selectedValue={this.state.descTipoCobranca}
                                onValueChange={this.onValueChangeTipoCobranca.bind(this)}
                            >
                                {
                                    list_tiposCobrancas.map((item, key) => {
                                        return <Picker.Item
                                            label={item.nome}
                                            value={item.id}
                                            key={item}
                                        />
                                    })
                                }
                            </Picker>
                        </View>
                    </View>
                </View>
            )
        }

        /*maybeRenderModal*/
        _maybeRenderModalDate = () => {
            if (!this.state.modalIsVisibleDate) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleDate ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneDate} />
                            </View>
                        </View>
                        <DatePickerIOS
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            mode="date"
                            date={this.state.date}
                            onDateChange={this.setDate}
                            timeZoneOffsetInMinutes={-1}
                            locale={'pt'}
                        >
                        </DatePickerIOS>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalTime = () => {
            if (!this.state.modalIsVisibleTime) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleTime ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneTime} />
                            </View>
                        </View>
                        <DatePickerIOS
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            mode="time"
                            date={this.state.date}
                            onDateChange={this.setDate}
                        >
                        </DatePickerIOS>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalAdults = () => {
            if (!this.state.modalIsVisibleAdults) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleAdults ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneAdults} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberAdults}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeAdults.bind(this)}>
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                            <Picker.Item label="11" value="11" />
                            <Picker.Item label="12" value="12" />
                            <Picker.Item label="13" value="13" />
                            <Picker.Item label="14" value="14" />
                            <Picker.Item label="15" value="15" />
                            <Picker.Item label="16" value="16" />
                            <Picker.Item label="17" value="17" />
                            <Picker.Item label="18" value="18" />
                            <Picker.Item label="19" value="19" />
                            <Picker.Item label="20" value="20" />
                            <Picker.Item label="21" value="21" />
                            <Picker.Item label="22" value="22" />
                            <Picker.Item label="23" value="23" />
                            <Picker.Item label="24" value="24" />
                            <Picker.Item label="25" value="25" />
                            <Picker.Item label="26" value="26" />
                            <Picker.Item label="27" value="27" />
                            <Picker.Item label="28" value="28" />
                            <Picker.Item label="29" value="29" />
                            <Picker.Item label="30" value="30" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalChild = () => {
            if (!this.state.modalIsVisibleChilds) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleChilds ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneChilds} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberChilds}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeChild.bind(this)}>
                            <Picker.Item label="0" value="0" />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalBaby = () => {
            if (!this.state.modalIsVisibleBaby) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleBaby ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneBaby} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.numberBaby}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeBaby.bind(this)}>
                            <Picker.Item label="0" value="0" />
                            <Picker.Item label="1" value="1" />
                            <Picker.Item label="2" value="2" />
                            <Picker.Item label="3" value="3" />
                            <Picker.Item label="4" value="4" />
                            <Picker.Item label="5" value="5" />
                            <Picker.Item label="6" value="6" />
                            <Picker.Item label="7" value="7" />
                            <Picker.Item label="8" value="8" />
                            <Picker.Item label="9" value="9" />
                            <Picker.Item label="10" value="10" />
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalOperadores = () => {
            if (!this.state.modalIsVisibleOperadores) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleOperadores ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneOperador} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descOperador}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeOperadores.bind(this)}>
                            {list_operadores.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalCategorias = () => {
            if (!this.state.modalIsVisibleCategorias) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleCategorias ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneCategoria} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descCategoria}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeCategorias.bind(this)}>
                            {list_categorias.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalClasseGrupo = () => {
            if (!this.state.modalIsVisibleClasseGrupo) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleClasseGrupo ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneClasseGrupo} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            selectedValue={this.state.descClasseGrupo}
                            onValueChange={this.onValueChangeClasseGrupo.bind(this)}
                        >
                            {
                                list_classeGrupo.map((item, key) => {
                                    let tipo = null
                                    item.classe_id === null ?
                                        tipo = (
                                            <Picker.Item
                                                label={item.grupo_nome + ': lotação ' + item.min_pessoas + '-' + item.max_pessoas}
                                                value={item.grupo_id}
                                                key={item}
                                            />
                                        )
                                        :
                                        tipo = (
                                            <Picker.Item
                                                label={item.classe_nome + ': lotação ' + item.min_pessoas + '-' + item.max_pessoas}
                                                value={item.classe_id}
                                                key={item}
                                            />
                                        )
                                    return tipo;
                                })
                            }
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalTipoServico = () => {
            if (!this.state.modalIsVisibleTipoServico) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleTipoServico ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneTipoServico} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descTipoServico}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeTipoServico.bind(this)}>
                            {list_tiposServico.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalZonaOrigem = () => {
            if (!this.state.modalIsVisibleZonaOrigem) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleZonaOrigem ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneZonaOrigem} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descZonaOrigem}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeZonaOrigem.bind(this)}>
                            {list_zonaOrigem.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome + ', ' + item.regiao_nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalZonaDestino = () => {
            if (!this.state.modalIsVisibleZonaDestino) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleZonaDestino ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneZonaDestino} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descZonaDestino}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeZonaDestino.bind(this)}>
                            {list_zonaDestino.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome + ', ' + item.regiao_nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }

        _maybeRenderModalTipoCobranca = () => {
            if (!this.state.modalIsVisibleTipoCobranca) {
                return null;
            }

            const { modalAnimatedValue } = this.state;
            const opacity = modalAnimatedValue;
            const translateY = modalAnimatedValue.interpolate({
                inputRange: [0, 1],
                outputRange: [300, 0],
            });

            return (
                <View
                    style={StyleSheet.absoluteFill}
                    pointerEvents={this.state.modalIsVisibleTipoCobranca ? 'auto' : 'none'}>
                    <TouchableWithoutFeedback onPress={this.props.onCancel}>
                        <Animated.View style={[styles.overlay, { opacity }]} />
                    </TouchableWithoutFeedback>
                    <Animated.View
                        style={{
                            width: '100%',
                            position: 'absolute',
                            bottom: 0,
                            left: 0,
                            transform: [{ translateY }],

                        }}>
                        <View style={styles.toolbar}>
                            <View style={styles.toolbarRight}>
                                <Button title="OK" onPress={this._handlePressDoneTipoCobranca} />
                            </View>
                        </View>
                        <Picker
                            style={{ width: WIDTH, backgroundColor: '#e1e1e1', top: 0 }}
                            selectedValue={this.state.descTipoCobranca}
                            itemStyle={{
                                fontSize: 18,
                                color: '#000',
                            }}
                            onValueChange={this.onValueChangeTipoCobranca.bind(this)}>
                            {list_tiposCobrancas.map((item, key) => {
                                return (
                                    <Picker.Item
                                        label={item.nome}
                                        value={item.id}
                                        key={key}
                                    />
                                );
                            })}
                        </Picker>
                    </Animated.View>
                </View>
            )
        }
        /* #endregion */

        return (
            <ImageBackground
                source={bgImage}
                style={styles.backgroundContainer}>
                <View style={styles.dashboard}>
                    <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                        <Icon style={{ marginLeft: 10 }} name="bars" size={30} color="#535557" />
                    </TouchableOpacity>
                    <Text style={styles.textDashboard}></Text>
                    <MyIcon style={{ color: '#535557', marginRight: 10 }} name="fonte-tg-27" size={30} />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            height: (Dimensions.get('window').height / 10) * 10,
                            width: '98%',
                            marginTop: 10,

                        }}>

                        {datePicker}

                        {modalPickerNumberPassenger}

                        {modalPickerOperadores}

                        {modalPickerCategorias}

                        {modalPickerClasseGrupo}

                        {modalPickerTiposServicos}

                        {modalPickerZonaOrigem}

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10,
                            backgroundColor: 'rgba(255, 255, 255, 255)',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            zIndex: 9999,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-53" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                flexDirection: 'column',
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 16,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000',
                                        zIndex: 9999,
                                    }}
                                    placeholder={"Morada Origem"}
                                    placeholderTextColor={'#000'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.destination}
                                    onChangeText={destination => this.onChangeDestination(destination)}
                                />
                                {predictions}
                            </View>
                        </View>

                        {modalPickerZonaDestino}

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10,
                            backgroundColor: 'rgba(255, 255, 255, 255)',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            zIndex: 10
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-53" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                flexDirection: 'column',
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 14,
                                        paddingLeft: 16,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000',
                                        zIndex: 9999,
                                    }}
                                    placeholder={"Morada Destino"}
                                    placeholderTextColor={'#000'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.destination2}
                                    onChangeText={destination2 => this.onChangeDestination2(destination2)}
                                />
                                {predictions2}
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10,
                            backgroundColor: 'rgba(255, 255, 255, 255)',
                            borderColor: '#CCC',
                            borderWidth: 1,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-39" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                flexDirection: 'column'
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000',
                                        zIndex: 0,
                                    }}
                                    placeholder={"Nome"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.nome}
                                    onChangeText={nome => this.setState({ nome })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10,
                            backgroundColor: 'rgba(255, 255, 255, 255)',
                            borderColor: '#CCC',
                            borderWidth: 1,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-54" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                flexDirection: 'column',
                                zIndex: 0,
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    keyboardType='email-address'
                                    placeholder={"Email"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.email}
                                    onChangeText={email => this.setState({ email })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginTop: 10,
                            backgroundColor: 'rgba(255, 255, 255, 255)',
                            borderColor: '#CCC',
                            borderWidth: 1,
                        }}>
                            <View style={styles.data}>
                                <MyIcon style={styles.icon} name="fonte-tg-12" size={25} />
                            </View>
                            <View style={{
                                height: 45,
                                width: Platform.OS === 'ios' ? '87.5%' : '88.5%',
                                flexDirection: 'column',
                                zIndex: 0,
                            }}>
                                <TextInput
                                    style={{
                                        height: 45,
                                        fontSize: 16,
                                        paddingLeft: 16,
                                        backgroundColor: 'rgba(255, 255, 255, 255)',
                                        color: '#000'
                                    }}
                                    keyboardType='numeric'
                                    placeholder={"Telefone"}
                                    placeholderTextColor={'#000'}
                                    value={this.state.telefone}
                                    onChangeText={telefone => this.setState({ telefone })}
                                >
                                </TextInput>
                            </View>
                        </View>

                        {modalPickerTiposCobrancas}

                        <View style={styles.container}>
                            <View style={{
                                flexDirection: 'column',
                                width: '50%',
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                            }}>
                                <View>
                                    <Text style={{ fontWeight: 'bold' }}>Emitir Voucher</Text>
                                </View>
                                <View>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        alignItems: 'center',
                                        marginTop: 10,
                                    }}>
                                        <SwitchToggle
                                            buttonText={this.getButtonText()}
                                            backTextRight={this.getRightText()}
                                            backTextLeft={this.getLeftText()}

                                            type={1}
                                            buttonStyle={{
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                position: 'absolute'
                                            }}

                                            rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                                            leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                                            // buttonTextStyle={{fontSize: 20}}
                                            // textRightStyle={{fontSize: 20}}
                                            // textLeftStyle={{fontSize: 20}}

                                            containerStyle={{
                                                //marginTop: 16,
                                                width: 130,
                                                height: 48,
                                                borderRadius: 25,
                                                backgroundColor: '#CCC',
                                                padding: 5,
                                            }}
                                            backgroundColorOn='#fff'
                                            backgroundColorOff='#fff'
                                            circleStyle={{
                                                width: 60,
                                                height: 45,
                                                borderRadius: 19,
                                                backgroundColor: 'blue', // rgb(102,134,205)
                                            }}
                                            switchOn={this.state.emitirVoucher}
                                            onPress={this.onPress4}
                                            circleColorOff='#e5e1e0'
                                            circleColorOn='#28a745'
                                            duration={500}
                                        />
                                    </View>
                                </View>
                            </View>
                            <View style={{
                                flexDirection: 'column',
                                width: '50%',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}>
                                <View>
                                    <Text style={{ fontWeight: 'bold' }}>Atribuir a Mim</Text>
                                </View>
                                <View>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'flex-start',
                                        alignItems: 'center',
                                        marginTop: 10,
                                    }}>
                                        <SwitchToggle
                                            buttonText={this.getButtonText2()}
                                            backTextRight={this.getRightText2()}
                                            backTextLeft={this.getLeftText2()}

                                            type={1}
                                            buttonStyle={{
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                position: 'absolute'
                                            }}

                                            rightContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                                            leftContainerStyle={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}

                                            // buttonTextStyle={{fontSize: 20}}
                                            // textRightStyle={{fontSize: 20}}
                                            // textLeftStyle={{fontSize: 20}}

                                            containerStyle={{
                                                //marginTop: 16,
                                                width: 130,
                                                height: 48,
                                                borderRadius: 25,
                                                backgroundColor: '#CCC',
                                                padding: 5,
                                            }}
                                            backgroundColorOn='#fff'
                                            backgroundColorOff='#fff'
                                            circleStyle={{
                                                width: 60,
                                                height: 45,
                                                borderRadius: 19,
                                                backgroundColor: 'blue', // rgb(102,134,205)
                                            }}
                                            switchOn={this.state.atribuirMim}
                                            onPress={this.onPress5}
                                            circleColorOff='#e5e1e0'
                                            circleColorOn='#28a745'
                                            duration={500}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.container}>
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={styles.textarea}
                                // onChangeText={this.onChange}
                                // defaultValue={this.state.text}
                                maxLength={120}
                                placeholder={'Observações...'}
                                placeholderTextColor={'#535557'}
                                underlineColorAndroid={'transparent'}
                                borderRadius={10}
                                value={this.state.observacoes}
                                onChangeText={observacoes => this.setState({ observacoes })}
                            />
                        </View>

                    </View>
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                            <Text style={styles.text}> Sair </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.createService} style={styles.btnEntrar}>
                            <Text style={styles.text}>Guardar</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <Footer />
                {_maybeRenderModalDate()}
                {_maybeRenderModalTime()}
                {_maybeRenderModalAdults()}
                {_maybeRenderModalChild()}
                {_maybeRenderModalBaby()}
                {_maybeRenderModalOperadores()}
                {_maybeRenderModalCategorias()}
                {_maybeRenderModalClasseGrupo()}
                {_maybeRenderModalTipoServico()}
                {_maybeRenderModalZonaOrigem()}
                {_maybeRenderModalZonaDestino()}
                {_maybeRenderModalTipoCobranca()}
            </ImageBackground>
        )
    }

    onPress4 = () => {
        this.setState({ emitirVoucher: !this.state.emitirVoucher });
        this.setValueMotoristaVoucher();
    };

    onPress5 = () => {
        this.setState({ atribuirMim: !this.state.atribuirMim });
        this.setValueMotoristaVoucher();
    };

    setValueMotoristaVoucher() {
        this.state.emitirVoucher ? this.state.emitirVoucherValue = 1 : this.state.emitirVoucherValue = 0
        this.state.atribuirMim ? this.state.atribuirMimValue = 1 : this.state.atribuirMimValue = 0
    }
}