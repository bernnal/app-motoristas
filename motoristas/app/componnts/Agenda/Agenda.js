import React, { Component } from 'react'
import { View, StyleSheet, Platform, TouchableOpacity, Text, Image, ScrollView, FlatList } from 'react-native'
import { CalendarList, LocaleConfig, Calendar } from 'react-native-calendars'
import { Col, Row, Grid } from "react-native-easy-grid"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../resources/fonts/selection.json'
import Icon from 'react-native-vector-icons/FontAwesome';
import { API_MOTORISTAS_TOTAL_SERVICOS, API_MOTORISTAS_SERVICOS } from '../../config'
import logoFooter from '../../images/logo-tg-h-01.png'
import Footer from '../Footer'
import axios from 'axios'
import moment from 'moment'
import { withNavigation } from 'react-navigation'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Agenda extends Component {

    constructor(props) {
        super(props)
        this.state = {
            api_token: this.props.navigation.state.params.api_token,
            hasService: false,
            dayFormated: null,
            date: new Date(),
            auxDate: moment(this.date).format('DD/MM/YYYY'),
        }
    }

    componentDidMount() {
        axios.get(`${API_MOTORISTAS_TOTAL_SERVICOS}`, {
            params: {
                'param1': this.state.api_token
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSource) => {
                let filterD = dataSource.data['data']
                const items = []
                for (const [index, value] of filterD.entries()) {
                    let values = moment(value.data_hora).format('YYYY-MM-DD')
                    items[values] = { selected: true, marked: true, selectedColor: '#FBB040' }
                }
                this.setState({
                    //viaturas: true,
                    dataSource: items
                })
                console.log('dataSource', this.state.dataSource)
            })
            .catch(error => console.log(error));

        axios.get(`${API_MOTORISTAS_SERVICOS}`, {
            params: {
                'param1': this.state.api_token,
                'param2': this.state.auxDate,
            }
        }, {
                "headers": {
                    'Content-Type': 'application/json',
                }
            }).then((dataSourceDay) => {
                this.setState({
                    hasService: true,
                    dataSourceDay: dataSourceDay.data['data'],
                })
                console.log('dataSourceDay', this.state.dataSourceDay)
            })
            .catch(error => console.log(error));


    }

    dayService = (day) => {
        this.state.dayFormated = moment(day.dateString).format('DD/MM/YYYY')
        console.log('dia selecionado:', this.state.dayFormated)

        if (this.state.dayFormated !== null) {
            axios.get(`${API_MOTORISTAS_SERVICOS}`, {
                params: {
                    'param1': this.state.api_token,
                    'param2': this.state.dayFormated,
                }
            }, {
                    "headers": {
                        'Content-Type': 'application/json',
                    }
                }).then((dataSourceDay) => {
                    this.setState({
                        hasService: true,
                        dataSourceDay: dataSourceDay.data['data'],
                    })
                    console.log('dataSourceDay', this.state.dataSourceDay)
                })
                .catch(error => console.log(error));
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{ marginVertical: 10, backgroundColor: 'black' }}
            />
        )
    }

    changeLanguage = () => {
        LocaleConfig.locales.en = LocaleConfig.locales['']
        LocaleConfig.locales.pt = {
            monthNames: [
                'Janeiro',
                'Fevereiro',
                'Março',
                'Abril',
                'Maio',
                'Junho',
                'Julho',
                'Agosto',
                'Setembro',
                'Outubro',
                'Novembro',
                'Dezembro',
            ],
            monthNamesShort: [
                'Jan',
                'Fev',
                'Mar',
                'Abr',
                'Mai',
                'Jun',
                'Jul',
                'Ago',
                'Set',
                'Out',
                'Nov',
                'Dez',
            ],
            dayNames: [
                'Domingo',
                'Segunda-feira',
                'Terça-feira',
                'Quarta-feira',
                'Quinta-feira',
                'Sexta-feira',
                'Sábado',
            ],
            dayNamesShort: [
                'Dom',
                'Seg',
                'Ter',
                'Qua',
                'Qui',
                'Sex',
                'Sab',
            ],
        }
        LocaleConfig.defaultLocale = 'pt';
    }

    render() {
        return (
            <View>
                <ScrollView>
                    <View style={styles.dashboard}>
                        <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
                            <Icon style={{ marginLeft: 10 }} name="bars" size={30} color="#535557" />
                        </TouchableOpacity>
                        <Text style={styles.textDashboard}></Text>
                        <MyIcon style={{ color: '#535557', marginRight: 10 }} name="fonte-tg-27" size={30} />
                    </View>
                    <Calendar
                        markedDates={this.state.dataSource}
                        hideArrows={false}
                        onDayPress={(day) => this.dayService(day)}
                        pastScrollRange={24}
                        futureScrollRange={24}
                        horizontal
                        locale={this.changeLanguage()}
                        pagingEnabled
                        theme={{
                            calendarBackground: '#F2F2F2',
                            textSectionTitleColor: '#222',
                            monthTextColor: '#222',
                            dayTextColor: '#222',
                            todayTextColor: '#989898',

                        }}
                    />
                    {
                        this.state.hasService ?
                            <View style={{ marginHorizontal: 5, }}>
                                <View style={styles.dashboardHeader}>
                                    {
                                        this.state.dataSourceDay.length > 1 ?
                                            <Text style={{
                                                fontSize: 15,
                                                color: 'black',
                                                textAlign: 'center',
                                            }}> Existem {this.state.dataSourceDay.length} Serviços - {this.state.dayFormated === null ? this.state.auxDate : this.state.dayFormated}  </Text>
                                            :
                                            <Text style={{
                                                fontSize: 15,
                                                color: 'black',
                                                textAlign: 'center',
                                            }}> Existe {this.state.dataSourceDay.length > 0 ? this.state.dataSourceDay.length : 0} Serviço - {this.state.dayFormated === null ? this.state.auxDate : this.state.dayFormated} </Text>
                                    }
                                </View>
                                <FlatList
                                    data={this.state.dataSourceDay}
                                    ItemSeparatorComponent={this.FlatListItemSeparator}
                                    renderItem={({ item, index }) =>
                                        <Grid style={{ backgroundColor: '#1EACA8', height: 140, borderRadius: 5, justifyContent: 'space-between' }}>
                                            <Row>
                                                <Col style={{ width: '15%', justifyContent: 'flex-start', marginTop: 5 }}>
                                                    <View style={{
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        width: 40,
                                                        height: 40,
                                                        backgroundColor: item.cor !== null ? item.cor : '#fff',
                                                        borderRadius: 20,
                                                        marginLeft: 5
                                                    }}>
                                                        <MyIcon style={{ color: '#000', alignItems: 'center', justifyContent: 'center' }} name={item.icone !== null ? item.icone.substring(5) : 'fonte-tg-30'} size={30} />
                                                    </View>
                                                </Col>
                                                <Col style={{}}>
                                                    <Col>
                                                        <Col style={{ marginTop: 5 }}>
                                                            <Text style={styles.title}>Data:</Text>
                                                            <Text style={{ color: '#fff', fontSize: 14 }}>{moment(item.data_hora).format('DD/MM/YYYY HH:MM')}</Text>
                                                        </Col>
                                                    </Col>
                                                </Col>
                                                <Col style={{}}>
                                                    <Col style={{ marginTop: 5 }}>
                                                        <Text style={styles.title}>Serviço:</Text>
                                                        <Text style={{ color: '#fff', fontSize: 14 }}>{item.tipo_servico}</Text>
                                                    </Col>
                                                </Col>
                                                <Col style={{ height: 130, width: '15%', justifyContent: 'flex-start', marginTop: 5, alignItems: 'flex-end', marginRight: 5 }}>
                                                    <View style={styles.logoContainerRight}>
                                                        <Text style={{ fontWeight: 'bold', fontSize: 10 }}>{item.comissao_motorista}</Text>
                                                    </View>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col style={{ marginTop: 10, marginLeft: 5 }}>
                                                    <Text style={styles.title}>Recolha:</Text>
                                                    <Text style={{ color: '#fff', fontSize: 14 }}>{item.servico === 'TZ' ? item.zona_origem : item.morada_origem}</Text>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col style={{ marginTop: 5, marginLeft: 5 }}>
                                                    <Text style={styles.title}>Entrega:</Text>
                                                    <Text style={{ color: '#fff', fontSize: 14 }}>{item.servico === 'TZ' ? item.zona_destino : item.morada_destino}</Text>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    }
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                            : null
                    }
                    <View style={styles.buttonsLogin}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Dashboard')} style={styles.btnSair}>
                            <Text style={styles.text}> SAIR </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    dashboard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '7%',
        backgroundColor: '#E6AC4A',
        flexWrap: 'wrap',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    textDashboard: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center'
    },
    logoContainerFooter: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '7%',
        backgroundColor: '#5A5B60',
        flexWrap: 'wrap',
    },
    logoFooter: {
        width: '40%',
        height: '40%',
        resizeMode: 'contain',
    },
    header: {
        fontSize: 15,
        color: 'white',
        textAlign: 'center',
    },
    dashboardHeader: {
        backgroundColor: '#E6AC4A',
        flexDirection: 'row',
        marginVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 30,
        borderRadius: 5,
    },
    logoContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        height: 40,
        backgroundColor: '#fff',
        borderRadius: 20,
        marginLeft: 5
    },
    logoContainerRight: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 40,
        height: 40,
        backgroundColor: '#fff',
        borderRadius: 20,
        marginLeft: 5,
        //marginTop: 5
    },
    title: {
        color: '#252525',
        fontWeight: 'bold',
        fontSize: 12,
    },
    buttonsLogin: {
        //width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 50,
        marginHorizontal: 5
    },
    btnSair: {
        width: '30%',
        height: 50,
        borderRadius: 5,
        backgroundColor: '#00b9d8',
        justifyContent: 'center',
        alignItems: 'center',
        //marginHorizontal: 10
    },
    text: {
        color: 'rgba(255, 255, 255, 255)',
        fontSize: 14,
        textAlign: 'center',
    },
})

export default withNavigation(Agenda)