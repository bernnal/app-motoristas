import React from 'react'
import { StyleSheet, Platform } from 'react-native'

const styles = StyleSheet.create({
    backgroundContainer: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    dashboard: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '5%',
        backgroundColor: '#E6AC4A',
        flexWrap: 'wrap',
        marginTop: Platform.OS === 'ios' ? 20 : 0,
    },
    textDashboard: {
        color: '#000',
        fontSize: 20,
        textAlign: 'center'
    },
    logoContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '50%',
        height: '50%',
        flexWrap: 'wrap'
    },
    logo: {
        flex: 1,
        resizeMode: 'contain',
    },
    menus: {
        flex: 1, 
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    servicos: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#84AD2C'
    },
    icon: {
        color: '#fff',
        textAlign: 'center',
        justifyContent: 'center',
    },
    texts: {
        color: '#fff',
        fontSize: 15,
        textAlign: 'center',
        justifyContent: 'flex-end',
        marginTop: 20,
    },
    agenda: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#C15F16'
    },
    novoServico: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#837700'
    },
    despesa: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#E95651'
    },
    contas: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#4A606F'
    },
    analise: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#DE8519'
    }
})

export default styles