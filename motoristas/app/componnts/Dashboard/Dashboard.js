import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native'
import { withNavigation } from 'react-navigation'
import { createIconSetFromIcoMoon } from 'react-native-vector-icons'
import icoMoonConfig from '../../resources/fonts/selection.json'
import Icon from 'react-native-vector-icons/FontAwesome';
import logo from '../../images/logo-tg-v-01.png'
import styles from './styles'
import Footer from '../Footer'

const MyIcon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Dashboard extends Component {

    constructor(props){
        super(props)
        this.state = {
            api_token: this.props.navigation.state.params.api_token,
            app_footer : this.props.navigation.state.params.app_footer,
        }
    }
    
    goToServico = () => {
        try {
            this.setState({ error: '', loading: true })
            
            const api_token = this.state.api_token
            const app_footer = this.state.app_footer
            this.props.navigation.navigate('Servicos', {
                api_token: api_token,
                app_footer: app_footer
            })

        } catch (err) {
            Alert.alert((err))
        }
    }

    goToAgenda = () => {
        try {
            this.setState({ error: '', loading: true })
            
            const api_token = this.state.api_token
            this.props.navigation.navigate('Agenda', {
                api_token: api_token,
            })
        } catch (err) {
            Alert.alert((err))
        }
    }

    goToNovoServico = () => {
        try {
            this.setState({ error: '', loading: true })
            
            const api_token = this.state.api_token
            this.props.navigation.navigate('Novo Servico', {
                api_token: api_token,
            })

        } catch (err) {
            Alert.alert((err))
        }
    }

    goToNovaDespesa = () => {
        try {
            this.setState({ error: '', loading: true })
            
            const api_token = this.state.api_token
            this.props.navigation.navigate('Despesas', {
                api_token: api_token,
            })

        } catch (err) {
            Alert.alert((err))
        }
    }

    goToContas = () => {
        try {
            this.setState({ error: '', loading: true })

            const api_token = this.state.api_token
            this.props.navigation.navigate('Contas', {
                api_token: api_token,
            })

        } catch (err) {
            Alert.alert((err))
        }
    }

    goToAnalise = () => {
        try {
            this.setState({ error: '', loading: true })

            const api_token = this.state.api_token
            this.props.navigation.navigate('TabNavigator', {
                api_token: api_token,
            })

        } catch (err) {
            Alert.alert((err))
        }
    }
    
    static navigationOptions = {
        drawerIcon: ({ tintColor }) => (
            <MyIcon name="fonte-tg-68" style={{ fontSize: 24, color: tintColor}}/>
        )
    }

    render() {
        return (
            <View style={styles.backgroundContainer}>
                <View style={styles.dashboard}>
                    <TouchableWithoutFeedback onPress={() => this.props.navigation.openDrawer()}>
                        <Icon style={{ marginLeft: 10 }} name="bars" size={30} color="#535557" />
                    </TouchableWithoutFeedback>
                    <Text style={styles.textDashboard}></Text>
                    <MyIcon style={{ color: '#535557', marginRight: 10 }} name="fonte-tg-27" size={30} />
                </View>
                <View style={styles.logoContainer}>
                    <Image source={logo} style={styles.logo}/>
                </View>
                <View style={styles.menus}>
                    <TouchableOpacity onPress={this.goToServico} style={styles.servicos}>
                        <MyIcon style={styles.icon} name="fonte-tg-27" size={70} />
                        <Text style={styles.texts}>SERVIÇOS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.goToAgenda} style={styles.agenda}>
                        <MyIcon style={styles.icon} name="fonte-tg-48" size={70} />
                        <Text style={styles.texts}>AGENDA</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.menus}>
                    <TouchableOpacity onPress={this.goToNovoServico} style={styles.novoServico}>
                        <MyIcon style={styles.icon} name="fonte-tg-57" size={70} />
                        <Text style={styles.texts}>NOVO SERVIÇO</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.goToNovaDespesa} style={styles.despesa}>
                        <MyIcon style={styles.icon} name="fonte-tg-44" size={70} />
                        <Text style={styles.texts}>DESPESAS</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.menus}>
                    <TouchableOpacity onPress={this.goToContas} style={styles.contas}>
                        <MyIcon style={styles.icon} name="fonte-tg-55" size={70} />
                        <Text style={styles.texts}>CONTAS</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.goToAnalise} style={styles.analise}>
                        <MyIcon style={styles.icon} name="fonte-tg-56" size={70} />
                        <Text style={styles.texts}>ANÁLISE</Text>
                    </TouchableOpacity>
                </View>
                <Footer />
            </View>
        )        
    }
}

export default withNavigation(Dashboard);