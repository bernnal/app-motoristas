//colocar action creators aqui
//colocar as actions creators aqui
import axios from 'axios'

const REQUEST_URL = 'https://jsonplaceholder.typicode.com';

export const fetchPosts = () => {
    //fazer pedidos HTTP para obter dados
    axios.get(`${REQUEST_URL}/posts`)
        .then((dataResponse) => {
            console.log(dataResponse)
        })
    //depois dispachar os dados recebidos
    return {
        type: 'FETCH_POSTS',
        // payload: dataResponse
    }

}