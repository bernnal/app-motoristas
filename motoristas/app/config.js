// GETS
export const API_GET_LOGO =                                 "https://gestao.transfergest.com/system/storage/logos/";
export const GET_ATRIBUIR =                                 "http://www.mocky.io/v2/5d3f12d32f000061006d01fe";
export const API_GOOGLE_AUTOCOMPLETE =                      "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=";
export const API_GOOGLE_CONVERT_LAT_LONG =                  "https://maps.googleapis.com/maps/api/geocode/json";
export const API_MOTORISTAS_VIATURAS =                      "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasViaturas";
export const API_TIPOS_DESPESAS =                           "http://osb2018.ddns.net/transfer_vinicius/api/ApiTipoDespesas";
export const API_OPERADORES =                               "http://osb2018.ddns.net/transfer_vinicius/api/ApiOperadores";
export const API_CATEGORIAS =                               "http://osb2018.ddns.net/transfer_vinicius/api/ApiCategorias";
export const API_CLASSES =                                  "http://osb2018.ddns.net/transfer_vinicius/api/ApiClasses";
export const API_ZONA_ORIGEM =                              "http://osb2018.ddns.net/transfer_vinicius/api/ApiZonasOrigem";
export const API_ZONA_DESTINO =                             "http://osb2018.ddns.net/transfer_vinicius/api/ApiZonasDestino";
export const API_TIPOS_COBRANCA =                           "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasTiposCobrancas";
export const API_TIPOS_SERVICOS =                           "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasTiposServico";
export const API_MOTORISTAS_SERVICOS =                      "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasServicos";
export const API_MOTORISTAS_BAGAGENS =                      "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasBagagensTransferzona";
export const API_MOTORISTAS_FINANCAS =                      "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasFinancas";
export const API_MOTORISTAS_VENCIMENTOS =                   "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasVencimentos";
export const API_MOTORISTAS_TOTAL_SERVICOS =                "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasTotalServicos";
export const API_MOTORISTAS_FORMAS_PAGAMENTOS =             "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasFormasPagamentos";
export const API_MOTORISTAS_EXTRAS_TRANSFERZONA =           "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasExtrasTransferZona";
export const API_MOTORISTAS_TIPOS_EXTRAS =                  "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasTiposExtras";

//POSTS
export const API_MOTORISTAS_LOGIN =                         "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasLogin";
export const API_INCLUI_SERVICO_TRANSFERZONA =              "http://osb2018.ddns.net/transfer_vinicius/api/ApiIncluiServicoZona";
export const API_INCLUI_DESPESA =                           "http://osb2018.ddns.net/transfer_vinicius/api/ApiIncluiDespesa";
export const API_ATUALIZA_ESTADO_SERVICO =                  "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasAtualizaEstadoServico";
export const API_ENVIA_EMAIL_CLIENTE =                      "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasEmailClientes";
export const API_MOTORISTA_FECHO_SERVICO =                  "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasFechoServico";
export const API_MOTORISTAS_INCLUI_EXTRAS_TRANSFERZONA =    "http://osb2018.ddns.net/transfer_vinicius/api/ApiMotoristasIncluiExtrasTransferzona";